<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>
    <!-- Site Title-->
    <title>发帖子</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header slider-menu-position">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px" class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand veil reveal-md-block"><a href="index.jsp" class="brand-name"><img style='margin-top: -10px;' width='164' height='29' src='images/logo-164x29.png' alt=''/></a></div>
                <div class="rd-navbar-brand veil-md reveal-tablet-md-inline-block"><a href="index.jsp" class="brand-name"><img style='margin-top: -7px;' width='128' height='24' src='images/logo-128x24.png' alt=''/></a></div>
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar-collapse-wrap" class="rd-navbar-collapse"><span></span></button>
              </div>
              <div class="rd-navbar-right-side">
                <div class="rd-navbar-nav-wrap reveal-md-inline-block">
                  <ul class="rd-navbar-nav">
                    <!-- RD Navbar Nav-->
                   	<li><a href="index.jsp" style="font-family: 微软雅黑">首页</a>
									</li>
									<li><a href="blogIndex.jsp"
										style="font-family: 微软雅黑">博客区</a></li>
									<li  ><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a>
									</li>
									<li ><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a>
									</li>
									<li class="active"><a href="#" style="font-family: 微软雅黑">发表</a>
										<ul class="rd-navbar-dropdown">
										<li><a href="postBlog.jsp">发表博客</a></li>
										<li class="active"><a href="forumPost.jsp">发表帖子</a></li>
										<li><a href="uploadFile.jsp">上传文件</a></li>
										</ul>
									</li>
              </div>
            </div>
          </nav>
        </div>
        <!-- Modern Breadcrumbs-->
        <section class="section-height-800 breadcrumb-modern rd-parallax context-dark">
          <div data-speed="0.2" data-type="media" data-url="images/backgrounds/background-02-1920x900.jpg" class="rd-parallax-layer"></div>
          <div data-speed="0" data-type="html" class="rd-parallax-layer">
            <div class="bg-overlay-chathams-blue">
              <div class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75"></div>
            </div>
          </div>
        </section>
      </header>
      <!-- Page Content-->
      <main class="page-content">
        <section class="section-75 section-md-top-60 section-md-bottom-110">
          <div class="shell">
            <div class="range range-xs-center range-lg-center">
              <div class="cell-xs-8 cell-sm-7 text-left">
                <div class="offset-top-20">
                  <h4 class="text-mine-shaft text-spacing-75 text-center text-sm-left" style="font-family: 微软雅黑">发帖子</h4>
                </div>
                <div class="offset-top-60">
                    <p class="font-accent text-bold text-mine-shaft text-spacing-30 text-center text-sm-left">标题</p>
                    <form data-form-output="form-contact-me" data-form-type="contact" method="post" action="bat/rd-mailform.php" novalidate="novalidate" class="rd-mailform offset-top-15 offset-md-top-15 text-left">
                      <div class="form-group">
                        <input id="post-title" username="username" class="form-control form-control-has-validation form-control-last-child" type="text">
                        </div>
                    </form>
                  </div>
                  <div class="offset-top-30">
                  <p class="font-accent text-bold text-mine-shaft text-spacing-30 text-center text-sm-left">标签</p>
                  <div class="text-center text-sm-right btn-group offset-top-30">
                    <button class="btn btn-xs btn-ellipse btn-primary btn-icon" onclick="changeNewTags()"><span class="icon icon-xs material-icons-ico material-icons-add" ></span>添加标签</button>
                    <button class="btn btn-xs btn-ellipse btn-primary btn-icon" onclick="createText()"><span class="icon icon-xs material-icons-ico material-icons-add"></span>创建新标签</button>
                  </div>
                  <div id="newTag" class="offset-top-15">
                    
                  </div>
                  <font size="4px" color="gray">已选标签：</font><div id="chosen" class="form-group offse-top-5">
                  
                  </div>
                  <div id="selectTag" class="offset-top-15">
                  
                  </div>
                  </div>
                  <div class="offset-top-30">
                    <p class="font-accent text-bold text-mine-shaft text-spacing-30 text-center text-sm-left">内容</p>
                    <form data-form-output="form-contact-me" data-form-type="contact" method="post" action="bat/rd-mailform.php" novalidate="novalidate" class="rd-mailform offset-top-15 offset-md-top-15 text-left">
                      <div class="form-group">
                        <script id="editor" type="text/plain" style="width:675px;height:500px;"></script><span class="form-validation"></span>
                      </div> 
                    </form>
                  </div>
                  <div id="file_error" style="color: red"></div>
                  <div class="text-center text-sm-right offset-top-60">
                    <button type="button" class="btn btn-sm btn-primary" onclick="forumPost()" >提交</button>
                  </div>
                  </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
     <footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
	<script src="js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
	<script src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="js/ueditor/ueditor.all.js"></script>
	<script type="text/javascript">
		var ue = UE.getEditor('editor',{
			autoFloatEnabled:false
		});
		function forumPost(){
			jQuery.fn.getTagList = function(){
				var tagsList = this.map( function(){
					return this.value; 
				} ).get();
				return tagsList.join(",");
			};
			var tags_str=$("#chosen").children().getTagList();
			//alert(tags_str);
			//alert(tags_str.split(","));
			var title=document.getElementById("post-title").value;
			var content=UE.getEditor('editor').getContent();
			if(title==""){
				$("#file_error").empty().append("帖子主题不能为空哦0.0");
				return;
			}
			if(tags_str==""){
				$("#file_error").empty().append("帖子标签不能为空哦0.0");
				return;
			}
			if(content==""){
				$("#file_error").empty().append("帖子内容不能为空哦0.0");
				return;
			}
			$.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/forumPostAction_forumPost",
               timeout:800000,//超时时间：8秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"post_title": title,
        		"post_content":content,
        		"post_tags":tags_str
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                
                  window.location.href="skipToMyPostAction_skip?type=userId&type_value=0";
                
              },  
              //请求出错的处理  
              error:function(){  
                       //alert("请求出错");  
              }  
           });  
			
		}
		var availableTags = [ "C#", "C++", "Java", "JavaScript", "ASP",
				"ASP.NET", "JSP", "PHP", "Python", "Ruby" ];
		// 按逗号分隔多个值
		function split(val) {
			return val.split(/,\s*/);
		}
		// 提取输入的最后一个值
		function extractLast(term) {
			return split(term).pop();
		}
		// 按Tab键时，取消为输入框设置value
		function keyDown(event) {
			if (event.keyCode === $.ui.keyCode.TAB
					&& $(this).data("autocomplete").menu.active) {
				event.preventDefault();
			}
		}
		var options = {
			// 获得焦点
			focus : function() {
				// prevent value inserted on focus
				return false;
			},
			// 从autocomplete弹出菜单选择一个值时，加到输入框最后，并以逗号分隔
			select : function(event, ui) {
				varterms = split(this.value);
				// remove the current input
				terms.pop();
				// add the selected item
				terms.push(ui.item.value);
				// add placeholder to get the comma-and-space at the end
				terms.push("");
				this.value = terms.join(", ");
				return false;
			}
		};
		// 多个值，本地数组
		$("#tag").bind("keydown", keyDown).autocomplete(
				$.extend(options, {
					minLength : 2,
					source : function(request, response) {
						// delegate back to autocomplete, but extract the last term
						response($.ui.autocomplete.filter(availableTags,
								extractLast(request.term)));
					}
				}));
		// 多个值，ajax返回json
		$("#ajax3").bind("keydown", keyDown).autocomplete($.extend(options, {
			minLength : 2,
			source : function(request, response) {
				$.getJSON("remoteJSON.ashx", {
					term : extractLast(request.term)
				}, response);
			}
		}));
		function AddElementInChoosing(id,value) {
			var TemO = document.getElementById("choosing");
			var newInput = document.createElement("button");
			newInput.id = id;
			//newInput.type = "button";
			//newInput.class="btn btn-xs btn-primary";
			newInput.value=value;
			newInput.style="font-family: 微软雅黑; font-size: 12px;padding: 4px 8px";
			 //<button class="btn btn-smalltags btn-white btn-icon btn-icon-right text-turquoise" style="font-family: 微软雅黑; font-size: 12px;padding: 4px 8px">
			 //<span class="icon icon-xs material-icons-ico material-icons-close" style="width: 15px;height: 15px;line-height: 21px;"></span>标签例子</button>
			$("#" + id).attr("class", "btn btn-smalltags btn-white btn-icon btn-icon-right text-turquoise");
			var btn="<button class='btn btn-smalltags btn-white btn-icon btn-icon-right text-turquoise' style='font-family: 微软雅黑; font-size: 12px;padding: 4px 8px' id='";
			btn+=id;
			btn+="' value='";
			btn+=value;
			btn+="'><span class='icon icon-xs material-icons-ico material-icons-close' style='width: 15px;height: 15px;line-height: 21px;'></span>";
			btn+=value;
			btn+="</button>";
			 //var btn=$("<input type='button' id='testBtn' value='Test'>");
			//TemO.appendChild(newInput);
			$("#choosing").append($(btn));
			$("#" + id).bind("click", function() {
				AddElementInChosen(id,value);
			});
		}
		function AddElementInChosen(id,value){
			var TemO = document.getElementById("chosen");
			var newInput = document.createElement("input");
			newInput.id = id;
			newInput.value=value;
			newInput.type = "button";
			newInput.class="btn btn-xs btn-primary";
			var btn="<button class='btn btn-smalltags btn-white btn-icon btn-icon-right text-turquoise' style='font-family: 微软雅黑; font-size: 12px;padding: 4px 8px' id='";
			btn+=id;
			btn+="' value='";
			btn+=value;
			btn+="'><span class='icon icon-xs material-icons-ico material-icons-close' style='width: 15px;height: 15px;line-height: 21px;'></span>";
			btn+=value;
			btn+="</button>";
			$("#chosen").append($(btn));
			//TemO.appendChild(newInput);
			$("#" + id).bind("click", function() {
				$("#" + id).remove();
			});
		}
		var changetimes=0,tags_num_in_each_page=5,num=0;
		var is_to_end=false;
		var tags=new Array();
		Array.prototype.remove=function(element){
  			for(var i=0;i<this.length;i++) { 
  				if(this[i]==element) { 
      				for(var j=i;j<this.length-1;j++){
      					this[i]=this[i+1];
      				}
      				this.pop();
      				break;
      			}
      		} 
      	} 
		function changeNewTags(){
			if(is_to_end){
				return;
			}
			$("#selectTag").empty().append("<font size=\"4px\" color=\"gray\">选择标签：</font><div id=\"choosing\" class=\"form-group offse-top-5\">");
			$.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/changeTagsAction_getNewTag",
               timeout:800000,//超时时间：8秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"change_times": changetimes,
        		"tags_num_in_each_page":tags_num_in_each_page
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                  if(data.retcode=="0001")
                  {
                  	is_to_end=true;
                  }
                  else{
                  	changetimes++;
                  	//alert("请求成功");  
                  }
                  var str=data.obj;
                  var atags=new Array();
                  atags=str.split(",");
                  //alert("请求成功");  
                  for(var i=0;i<atags.length;i++){
                  	AddElementInChoosing("tag"+num,atags[i]);
                  	num++;
                  }
                  //alert(data.yourName+"你输入的内容:"+data.yourContent);  
              },  
              //请求出错的处理  
              error:function(){  
                       //alert("请求出错");  
              }  
           });  
		
		}
		function createNewTags(){
			var new_tag=document.getElementById("tag").value;
			$.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/createTagsAction_createNewTag",
               timeout:80000,//超时时间：8秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"new_tag":new_tag
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                if(data.retcode=="0001")
                  {
                  	  AddElementInChosen("tag"+num,new_tag);
                      num++;
                  }
                  else if(data.retcode=="0000"){
                      AddElementInChosen("tag"+num,new_tag);
                      num++;
                  }else{
                  	alert("请求出错");
                  } 
              },  
              //请求出错的处理  
              error:function(){  
                        alert("请求出错");  
              }  
           }); 
		}
	function createText(){
		var btn="<button class='btn btn-xs btn-ellipse btn-primary btn-icon' id='createTag'>";
			btn+="创建标签";
			btn+="</button>";
			var text="<input id='tag' class='form-control form-control-has-validation form-control-last-child' type='text' style='height: 45px; width:70%'><br>";
			$("#newTag").empty().append($(text));
			$("#newTag").append($(btn));
			$("#createTag").bind("click", function() {
				createNewTags();
				document.getElementById("tag").value="";
			});
	}
	</script>
	<script type="text/javascript" charset="utf-8" src="js/ueditor/lang/zh-cn/zh-cn.js"></script>
</body>
</html>
