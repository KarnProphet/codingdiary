<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<!-- Site Title-->
<title>文件详情</title>
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
</head>
 <%
  	Object userid=session.getAttribute("userID");
	int loginstate;
	if(userid==null){
		loginstate=0;
	}
	else{
		loginstate=1;
		pageContext.setAttribute("userId", userid);
	}
	pageContext.setAttribute("loginstate", loginstate);
   %>
<body onload="showCurrentDoc()">
	<!-- Page-->
	<div class="page text-center">
		<!-- Page Header-->
		<header class="page-header slider-menu-position">
			<!-- RD Navbar-->
			<div class="rd-navbar-wrap">
				<nav data-md-device-layout="rd-navbar-fixed"
					data-lg-device-layout="rd-navbar-static"
					data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px"
					class="rd-navbar" data-layout="rd-navbar-fixed"
					data-sm-layout="rd-navbar-fixed"
					data-md-layout="rd-navbar-fullwidth"
					data-lg-layout="rd-navbar-static">
					<div class="rd-navbar-inner">
						<!-- RD Navbar Panel-->
						<div class="rd-navbar-panel">
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-nav-wrap"
								class="rd-navbar-toggle">
								<span></span>
							</button>
							<!-- RD Navbar Brand-->
							<div class="rd-navbar-brand veil reveal-md-block">
								<a href="index.jsp" class="brand-name"><img
									style='margin-top: -10px;' width='164' height='29'
									src='images/logo-164x29.png' alt='' /> </a>
							</div>
							<!-- <div
								class="rd-navbar-brand veil-md reveal-tablet-md-inline-block">
								<a href="index.jsp" class="brand-name"><img
									style='margin-top: -7px;' width='128' height='24'
									src='images/logo-128x24.png' alt='' /> </a>
							</div> -->
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-collapse-wrap"
								class="rd-navbar-collapse">
								<span></span>
							</button>
						</div>
						<div  id="loginState" class="rd-navbar-right-side">
							<div class="rd-navbar-nav-wrap reveal-md-inline-block">
								<ul class="rd-navbar-nav">
									<!-- RD Navbar Nav-->
									<li><a href="index.jsp" style="font-family: 微软雅黑">首页</a>
									</li>
									<li><a href="blogIndex.jsp"
										style="font-family: 微软雅黑">博客区</a></li>
									<li  ><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a>
									</li>
									<li class="active"><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a>
									</li>
									<li><a href="#" style="font-family: 微软雅黑">发表</a>
										<ul class="rd-navbar-dropdown">
										<li><a href="postBlog.jsp">发表博客</a></li>
										<li><a href="forumPost.jsp">发表帖子</a></li>
										<li><a href="uploadFile.jsp">上传文件</a></li>
										</ul>
									</li>
							</div>
						</div>
				</nav>
			</div>
			<!-- Modern Breadcrumbs-->
			<section
				class="section-height-800 breadcrumb-modern rd-parallax context-dark">
				<div data-speed="0.2" data-type="media"
					data-url="images/backgrounds/background-02-1920x900.jpg"
					class="rd-parallax-layer"></div>
				<div data-speed="0" data-type="html" class="rd-parallax-layer">
					<div class="bg-overlay-chathams-blue">
						<div
							class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75"></div>
					</div>
				</div>
			</section>
		</header>
	<!-- Single Post-->
	<section class="section-75 section-md-top-103 section-md-bottom-60">
	<div class="shell">
		<div class="range">
			<div class="cell-md-3">
				<a href="#"><img
					src="images/users/user-john-smith-271x271.jpg" width="271"
					height="271" alt="" class="img-circle img-responsive center-block">
				</a>
				<div class="offset-top-25">
					<h5 class="text-spacing-inverse-25 text-capitalize" id="username"></h5>
				</div>
			</div>
			<div class="cell-md-9 text-left">
				<div class="offset-top-25 offset-md-top-5">
					<h3 id="docName" class="text-spacing-inverse-50 text-capitalize text-center text-md-left"></h3>
				</div>
				<div class="offset-top-30 offset-md-top-54">
					<div id="uploader" class="offset-top-20 offset-md-top-14">上传人:</div>
					<div id="docTag" class="offset-top-20 offset-md-top-14">标签:</div>
					<div id="docDescription" class="offset-top-20 offset-md-top-14">文件描述:</div>
					<div id="downloadNum" class="offset-top-20 offset-md-top-14">下载次数:</div>
					<div id="collectNum" class="offset-top-20 offset-md-top-14">被收藏次数:</div>
					<br>
					<div><a href="docOperationsAction_downloadDoc?doc_id=<%=request.getParameter("docID")%>"><span>下载</span></a></div>
				</div>
				<br>
				<ul
					class="post-controls list-inline list-inline-2 list-primary text-center text-md-left">
					<li><span style="width: 30px;"
						class="text-middle icon icon-sm text-primary mdi mdi-clock text-center"></span>
						<time id="uploadTime"
							class="text-sbold text-extra-small text-middle"></time></li>
					<li style="margin-left: 14px;"><a href="#comments"
						class="text-middle"><span
							class="text-middle icon icon-sm text-turquoise mdi mdi-comment-multiple-outline text-center"> </span>
					</a><span id="commentNum"
						class="inset-left-5 text-sbold text-extra-small text-silver-chalice"></span>
					</li>
					<li><a href="#" class="text-middle" onclick="praiseClick()"><span
							id="praiseSpan" class="text-middle icon icon-sm text-turquoise mdi mdi-heart-outline text-center"></span>
					</a><span id="praiseNum"
						class="inset-left-5 text-sbold text-extra-small text-silver-chalice"></span>
					</li>
					<li><a href="#" class="text-middle"><span
							class="text-middle icon icon-sm text-turquoise mdi mdi-share text-right"> </span>
					</a><span id="forwardNum"
						class="inset-left-5 text-sbold text-extra-small text-silver-chalice"></span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section class="section-75 section-md-top-60 section-md-bottom-110">
	<div class="shell">
		<div class="range range-xs-center range-lg-right">
			<div class="cell-xs-10 cell-sm-9 text-left">
				<div class="offset-top-30">
					<div class="hr gray-lighter"></div>
				</div>
				<div class="offset-top-55">
					<p
						class="font-accent text-bold text-uppercase text-mine-shaft text-spacing-25 text-center text-sm-left">你或许会感兴趣的文档</p>
				</div>
				<div class="range range-sm-center range-lg-left offset-top-25">
					<div class="cell-sm-6 cell-lg-5">
						<div
							class="inset-left-30 inset-right-30 inset-lg-left-0 inset-lg-right-0">
							<!-- Unit-->
							<div
								class="unit unit-sm unit-sm-horizontal unit-spacing-xs text-center text-sm-left">
								<div class="unit-left">
									<img src="images/users/user-sandra-green-70x70.jpg" alt=" "
										width="70" height="70" class="img-circle">
								</div>
								<div class="unit-body">
									<div>
										<p class="text-extra-small text-bold">2015-10-25</p>
									</div>
									<div class="offset-top-5">
										<p
											class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
											<a href="#" class="text-turquoise">这是一篇文档</a>
										</p>
									</div>
								</div>
							</div>
							<div class="offset-top-30">
								<div class="hr gray-lighter"></div>
							</div>
							<div class="offset-top-30">
								<!-- Unit-->
								<div
									class="unit unit-sm unit-sm-horizontal unit-spacing-xs text-center text-sm-left">
									<div class="unit-left">
										<img src="images/users/user-john-richardson-70x70.jpg" alt=" "
											width="70" height="70" class="img-circle">
									</div>
									<div class="unit-body">
										<div>
											<p class="text-extra-small text-bold">2015-10-25</p>
										</div>
										<div class="offset-top-5">
											<p
												class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
												<a href="#" class="text-turquoise">这是一篇文档</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="offset-top-30">
								<div class="hr gray-lighter"></div>
							</div>
						</div>
					</div>
					<div class="cell-sm-6 offset-top-24 offset-sm-top-0">
						<div
							class="inset-left-30 inset-right-30 inset-lg-left-30 inset-lg-right-25">
							<!-- Unit-->
							<div
								class="unit unit-sm unit-sm-horizontal unit-spacing-xs text-center text-sm-left">
								<div class="unit-left">
									<img src="images/users/user-mike-sanders-70x70.jpg" alt=" "
										width="70" height="70" class="img-circle">
								</div>
								<div class="unit-body">
									<div>
										<p class="text-extra-small text-bold">2016-10-26</p>
									</div>
									<div class="offset-top-5">
										<p
											class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
											<a href="#" class="text-turquoise">这是一篇文档</a>
										</p>
									</div>
								</div>
							</div>
							<div class="offset-top-30">
								<div class="hr gray-lighter"></div>
							</div>
							<div class="offset-top-30">
								<!-- Unit-->
								<div
									class="unit unit-sm unit-sm-horizontal unit-spacing-xs text-center text-sm-left">
									<div class="unit-left">
										<img src="images/users/user-tom-jefferson-70x70.jpg" alt=" "
											width="70" height="70" class="img-circle">
									</div>
									<div class="unit-body">
										<div>
											<p class="text-extra-small text-bold">2016-10-27</p>
										</div>
										<div class="offset-top-5">
											<p
												class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
												<a href="#" class="text-turquoise">这是一篇文档</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="offset-top-30">
								<div class="hr gray-lighter"></div>
							</div>
						</div>
					</div>
				</div>
				<br> <br>
				<div class="offset-top-55">
					<p
						class="font-accent text-bold text-uppercase text-mine-shaft text-spacing-25">评论区</p>
				</div>
				<div id="comments" class="offset-top-30 inset-lg-right-100"></div>
				<br> <br>
				<div class="offset-top-60">
					<p
						class="font-accent text-bold text-mine-shaft text-spacing-25 text-center text-sm-left">写下你的评论吧~</p>
					<form data-form-output="form-contact-me" data-form-type="contact"
						method="post" novalidate="novalidate"
						class="rd-mailform offset-top-15 offset-md-top-30 text-left">
						<div class="form-group">
							<textarea id="myComment" style="height: 236px;"
								class="form-control"></textarea>
							<span class="form-validation"></span>
						</div>
						<div id="file_error" style="color: red"></div>
						<div class="text-center text-sm-right offset-top-30">
							<button type="submit" class="btn btn-sm btn-primary"
								onclick="submitComment()">提交</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
	<footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
	</div>
	<!-- Global Mailform Output-->
	<div id="form-output-global" class="snackbars"></div>
	<!-- PhotoSwipe Gallery-->
	<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button title="Close (Esc)"
						class="pswp__button pswp__button--close"></button>
					<button title="Share" class="pswp__button pswp__button--share"></button>
					<button title="Toggle fullscreen"
						class="pswp__button pswp__button--fs"></button>
					<button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>
				<button title="Previous (arrow left)"
					class="pswp__button pswp__button--arrow--left"></button>
				<button title="Next (arrow right)"
					class="pswp__button pswp__button--arrow--right"></button>
				<div class="pswp__caption">
					<div class="pswp__caption__cent"></div>
				</div>
			</div>
		</div>
	</div>
</body>

<!-- Java script-->
<script src="js/core.min.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">
	var commenterImage = "<div class=\"box-comment text-left box-comment-boxed\"><div class=\"media\"><div class=\"media-left\"><img src=\"images/users/user-sandra-green-70x70.jpg\" alt=\"\" width=\"70\" height=\"70\" class=\"img-circle box-comment-img\"></div><div class=\"media-body\"><header class=\"box-comment-header unit unit-vertical unit-spacing-xxs unit-md unit-md-horizontal unit-md-inverse unit-md-middle unit-md-align-right\"><div class=\"unit-left\"><ul class=\"box-comment-meta list-inline list-inline-sm text-dark\"><li><span class=\"box-comment-icon mdi mdi-clock text-middle\"></span><time datetime=\"\" class=\"text-middle\">";
	var commentTime = "";

	var commentPraise = "</time></li><li><a href=\"#like\"><span class=\"box-comment-icon mdi mdi-thumb-up-outline text-middle\"></span><span class=\"text-middle\">赞</span></a></li>";
	var commentComment = "<li><a href=\"#reply\"><span class=\"box-comment-icon mdi mdi-message-outline text-middle\"></span><span class=\"text-middle\">回复</span></a></li></ul></div>";
	var commentContent_2 = "<div class=\"unit-body\"><div class=\"box-comment-title text-extra-small text-bold text-spacing-0 text-turquoise\">";
	var commenterName_ = "";
	var commentContent_3 = "</div></div></header>";
	var commentContent_4 = "<section class=\"box-comment-body\"><p class=\"text-extra-small text-spacing-0 text-silver\">";
	var userCommentContent = "";
	var commentContent_5 = "</p></section></div></div></div>";

	var docID =<%=request.getParameter("docID")%>
	;
	function showCurrentDoc() {
	detemineloginstate();
		$.ajax({
					type : "post",//请求方式  
					url : "${pageContext.request.contextPath}/browseDocAction_loadSingleDoc",
					timeout : 800000,//超时时间：800秒  
					dataType : "json",//设置返回数据的格式  
					data : {
						"doc_id" : docID
					},
					//请求成功后的回调函数 data为json格式  
					success : function(data) {
						if (data.retcode != "0000") {
							var errorMsg = data.errorMsg;
							alert("服务器出错了=-=");
						} else {
							var jsonArray = data.obj;
							var divText = "";
							//获取文件属性信息
							var docInfo = jsonArray[jsonArray.length - 1];
							var docName = docInfo.docName;
							var docUploaderName = docInfo.docUploaderName;
							var docDescription = docInfo.docDescription;
							var praiseNum = docInfo.praiseNum;
							var commentNum = docInfo.commentNum;
							var downloadNum = docInfo.downloadNum;
							var forwardNum = docInfo.forwardNum;
							var collectNum = docInfo.collectNum;
							var docUploadTime = docInfo.docUploadTime;
							var docTags = docInfo.docTags.split(";");
							var curDocTags = "";
							var myID = docInfo.myID;
							$("#username").empty().append(docUploaderName);
							
							for ( var index = 0; index < docTags.length; index++) {
								curDocTags += docTags[index] + " ";
							}
							$("#docName").empty().append("【资料】" + docName);
							$("#uploader").empty().append(
									"<font color='gray'>上传人：" + docUploaderName+"</font>");
							$("#docTag").empty().append("<font color='gray'>文件标签：" + curDocTags+"</font>");
							$("#docDescription").empty().append(
									"<font color='gray'>文件描述：" + docDescription+"</font>");
							$("#uploadTime").empty().append(docUploadTime.split(".")[0]);
							$("#commentNum").empty().append(commentNum);
							$("#downloadNum").empty().append(
									"<font color='gray'>下载次数：" + downloadNum+"</font>");
							$("#forwardNum").empty().append(forwardNum);
							$("#praiseNum").empty().append(praiseNum);
							$("#collectNum").empty().append(
									"<font color='gray'>收藏次数：" + collectNum+"</font>");
							//展示是否已点赞
							var docPraiseUsers=docInfo.praiseUsers.split(",");
							for(var index = 0;index<docPraiseUsers.length;index++){
								if(docPraiseUsers[index]==myID){
									document.getElementById("praiseSpan").className="text-middle icon icon-sm text-turquoise mdi mdi-heart text-center";
									break;
								}
							}
							//显示评论信息 
							if (jsonArray.length == 1) {
								$("#comments")
										.empty()
										.append(
												"<div id=\"commentRegion\"><p class=\"font-accent text-bold text-uppercase text-spacing-inverse-25 text-capitalize text-mine-shaft text-spacing-30\">暂无评论，来抢第一个沙发吧:)</p></div>");
							} else {
								for ( var i = 0; i < jsonArray.length - 1; i++) {
									var commentInfo = jsonArray[i];
									var commenterID = commentInfo.commenterID;
									var commenterName = commentInfo.commenterName;
									var commentContent ="<font color='gray' size='3px'>" + commentInfo.commentContent+"</font>";
									var praiseNum = commentInfo.praiseNum;
									var docCommentTime = commentInfo.commentTime.split(".")[0];
									divText = commenterImage + docCommentTime
											+ commentPraise + commentComment
											+ commentContent_2 + commenterName
											+ commentContent_3
											+ commentContent_4 + commentContent;
									
									$("#comments").append(divText);
								}
							}
						}
					},
					//请求出错的处理  
					error : function() {
						alert("请求出错");
					}
				});
	}
	function submitComment() {
		var commentText = $("#myComment").val();
		if (commentText == "") {
			$("#file_error").empty().append("评论内容不能为空哦0.0");
			return;
		}
		$.ajax({
					type : "post",//请求方式  
					url : "${pageContext.request.contextPath}/docOperationsAction_commentDoc",
					timeout : 800000,//超时时间：800秒  
					dataType : "json",//设置返回数据的格式  
					data : {
						"doc_id" : docID,
						"comment_text" : commentText
					},
					//请求成功后的回调函数 data为json格式  
					success : function(data) {
						if (data.retcode == "0000") {
							$("#commentRegion").empty();
							var map = data.obj;
							var docCommentTime = map.docCommentTime.split(".");
							var commenterName = map.commenterName;
							var commentText = "<font color='gray' size='3px'>"+map.commentText+"</font>";
							var addComment = "";
							var commentNum = map.commentNum;
							addComment += commenterImage + docCommentTime[0]
											+ commentPraise + commentComment
											+ commentContent_2 + commenterName
											+ commentContent_3
											+ commentContent_4 + commentText;
							$("#comments").append(addComment);
							$("#commentNum").empty().append(commentNum);
						}
						else if(data.retcode=="0001"){
							alert(data.errorMsg);
						}
					},
					//请求出错的处理  
					error : function() {
						alert("请求出错");
					}
				});
	}

	function praiseClick() {
		$.ajax({
					type : "post",//请求方式  
					url : "${pageContext.request.contextPath}/docOperationsAction_praiseDoc",
					timeout : 800000,//超时时间：800秒  
					dataType : "json",//设置返回数据的格式  
					data : {
						"doc_id" : docID
					},
					//请求成功后的回调函数 data为json格式  
					success : function(data) {
						var praiseCount = "";
						if(data.retcode=="0001"){
							alert(data.errorMsg + ",点赞失败..");
						}
						else{
							var retObj = data.obj;
							if(retObj.is_have_praised=="true"){
							document.getElementById("praiseSpan").className="text-middle icon icon-sm text-turquoise mdi mdi-heart-outline text-center";
							}
							else{
								document.getElementById("praiseSpan").className="text-middle icon icon-sm text-turquoise mdi mdi-heart text-center";
							}
						}
						praiseCount = retObj.praise_num;
						$("#praiseNum").empty().append(praiseCount);
					},
					//请求出错的处理  
					error : function() {
						alert("请求出错");
					}
				});
	}
	function downloadDoc(){
	alert("请求");
		$.ajax({
					type : "post",//请求方式  
					url : "${pageContext.request.contextPath}/docOperationsAction_downloadDoc",
					timeout : 800000,//超时时间：800秒  
					dataType : "json",//设置返回数据的格式  
					data : {
						"doc_id" : docID
					},
					//请求成功后的回调函数 data为json格式  
					success : function() {
						alert("请求");
					},
					//请求出错的处理  
					error : function() {
						alert("请求出错");
					}
				});
	}
	    function detemineloginstate(){
    	var login="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
    	login+="<ul class='list-inline list-inline-0 list-primary'>";
    	login+="<li class='text-center label offset-left text-spacing-20'><a href='userLogIn.jsp' class='icon icon-xxs fa fa-user text-white'> 登录/注册</a>";
    	login+="</li></ul></div>";
    	var havelogin="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
		havelogin+="<ul class='list-inline list-inline-0 list-primary'>";
		havelogin+=" <li class='text-center'><a href='#' class='icon icon-xxs fa fa-edit text-white'></a></li>";
		havelogin+="<li class='text-center'><a href='#' class='icon icon-xxs fa fa-bell-o text-white'></a></li></ul></div> ";
		havelogin+="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
        havelogin+="<a href='skipToMyPageAction_skipToMyPage'><img src='images/users/user-sandra-green-70x70.jpg' alt='' width='40' height='40' class='img-circle box-comment-img'></a> </div> ";
    	var loginstate=${loginstate};
    	if(loginstate=="0"){
    		$("#loginState").append($(login));
    	}else if(loginstate=="1"){
    		$("#loginState").append($(havelogin));
    	}

    }
</script>
</html>