<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<title>我的资料</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body>
	 <% 
		List list=(List)request.getAttribute("documentList");
	String typevalue=(String)request.getParameter("type_value");
	typevalue=typevalue.split("and")[0];
	pageContext.setAttribute("typevalue", typevalue);
	pageContext.setAttribute("list", list);
	String typeValue=(String)session.getAttribute("typeValue");
	pageContext.setAttribute("typeValue", typeValue.split("and")[0]);
	if(typeValue.contains("and")){
		pageContext.setAttribute("isuserId", "and"+typeValue.split("and")[1]);
	}else{
		pageContext.setAttribute("isuserId", "");
	}
    int count=0; //总行数
    int page_count=0;  //开始条数
    int page_total=1;  //，总页码
    int page_current= 1;  //首页
    int page_size=5;//一页的行数
    String page_cu = request.getParameter("page_current");  
    if(page_cu==null){  
       page_cu="1";  
    }  
    pageContext.setAttribute("page_num", page_cu);
    page_current = Integer.parseInt(page_cu)-1;
    if(page_current<0){
       page_current = 1;  
    } 
    page_count=page_count+ page_current*page_size;

     %>
	<!-- Page-->
	<div class="page text-center">
		<!-- Page Header-->
	<header class="page-header slider-menu-position">
			<!-- RD Navbar-->
			<div class="rd-navbar-wrap">
				<nav data-md-device-layout="rd-navbar-fixed"
					data-lg-device-layout="rd-navbar-static"
					data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px"
					data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
					data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed"
					class="rd-navbar rd-navbar-hamburger">
					<div class="rd-navbar-inner">
						<div class="rd-navbar-inner-top">
							<!-- RD Navbar Brand-->
							<div class="rd-navbar-brand veil reveal-lg-inline-block">
								<a href="index.jsp" class="brand-name"><img
									style="margin-top: -10px;" width="164" height="29"
									src="images/logo-164x29.png" alt="">
								</a>
							</div>
						</div>
						<!-- RD Navbar Panel-->
						<div class="rd-navbar-panel">
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-nav-wrap"
								class="rd-navbar-toggle">
								<span></span>
							</button>
							<div class="rd-navbar-brand veil-lg">
								<a href="index.jsp" class="brand-name"><img
									style="margin-top: -7px;" width="128" height="24"
									src="images/logo-128x24.png" alt="">
								</a>
							</div>
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-collapse-wrap"
								class="rd-navbar-collapse veil-lg">
								<span></span>
							</button>
						</div>
						<div class="rd-navbar-right-side">
							<div class="rd-navbar-nav-wrap reveal-md-inline-block">
								<ul class="rd-navbar-nav">
									<!-- RD Navbar Nav-->
									<li><a href="index.jsp">返回首页</a>
									</li>
									<li ><a href="skipToMyPageAction_skipToMyPage">我的主页</a>
									</li>
									<li><a href="skipToMyBlogAction_skip?type=userId&type_value=-1">我的博客</a>
									</li>
									<li><a href="skipToMyPostAction_skip?type=userId&type_value=-1">我的帖子</a>
									</li>
									<li class="active"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1">我的资料</a>
									</li>
									<li><a href="followers.html">我的粉丝</a>
									</li>
									<li><a href="followings.html">关注的人</a>
									</li>
									<li><a href="my-collections.html">我的收藏</a>
										</li>
								</ul>
							</div>
							<div class="rd-navbar-collapse-wrap reveal-md-inline-block">
								
							</div>
						</div>
					</div>
				</nav>
			</div>
			<!-- Modern Breadcrumbs-->
			<section
				class="section-height-600 breadcrumb-modern rd-parallax context-dark">
				<div data-speed="0.2" data-type="media"
					data-url="images/backgrounds/background-04-1920x900.jpg"
					class="rd-parallax-layer"></div>
				<div data-speed="0" data-type="html" class="rd-parallax-layer">
					<div class="bg-overlay-chathams-blue">
						<div
							class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75">
							<div class="offset-top-30">
								<div class="cell-sm-10 cell-lg-8">
									<img src="images/users/user-blog-900x900.jpg" width="125"
										height="125" alt=""
										class="img-circle img-responsive center-block">
								</div>
							</div>
							<div class="offset-top-25">
								<ul
									class="list-inline list-marked list-marked-type-mid list-marked-type-2-dot-1 list-silver-chalice list-marked-silver-chalice"
									style="font-family: 微软雅黑; font-size: 25px; letter-spacing: 2px">
									<li class="text-spacing-50"><a href="skipToMyPageAction_skipToMyPage"
										target="_parent">主页</a>
									</li>
									<li class="text-spacing-50"><a href="skipToMyBlogAction_skip?type=userId&type_value=-1"
										target="_parent">博客</a>
									</li>
									<li class="text-spacing-50"><a href="skipToMyPostAction_skip?type=userId&type_value=-1"
										target="_parent">帖子</a>
									</li>
									<li class="text-spacing-50"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1"
										target="_parent">资料</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
		</header>
		<!-- Page Content-->
		<main
			class="page-content section-75 section-md-top-34 section-md-bottom-95">
		<!-- Blog Classic Both Sidebar--> <section>
		<div class="shell">
			<div class="range range-xs-center range-lg-left offset-top-50">
				<div class="cell-sm-10 cell-md-8 cell-md-push-1">
					<div id="loadMyDocsDiv">
						<c:forEach items="${list}" var="document" step="1" varStatus="i"
							begin="<%=page_count%>" end="<%=page_size+page_count-1%>">
							<article class="post-classic text-left">
							<div class="post-classic-body offset-top-10">
								<div class="unit unit-sm unit-sm-horizontal unit-sm-inverse">
									<div class="unit-body">
										<div class="post-body offset-top-15 offset-md-top-25">
											<div class="cell-xs-8 cell-sm-5" style='margin-top: -55px;'>
												<a href="fileDetails.jsp?docID=${document.docId }" class="post-vacancy"><span
													class="unit unit-xs unit-xs-middle unit-xs-horizontal unit-spacing-xs text-xs-left"><span
														class="unit-left"></span>
												</span><span class="unit-body"> <span
														class="post-meta text-spacing-0">${document.uploadTime}</span><span
														class="post-title  reveal-block"><span
															class="offset-top-5 font-accent text-bold text-spacing-50 text-uppercase"><span
																class="text-mine-shaft"> ${document.docName}</span>
														</span>
													</span><br>
													<span class="text-spacing-0 reveal-block zhengwen">
															${document.docDescription} <span
															class="text-extra-small text-bold text-turquoise text-uppercase">详情</span>
													</span>
												</span>
												</a>
											</div>
										</div>
									</div>
									<div class="unit-right" style='margin-top: 10px;'>
										<div class="unit unit-horizontal unit-middle unit-sm-vertical unit-spacing-xs">
											<div class="unit-left">
												<img src="images/users/user-sandra-green-70x70.jpg" alt=" "
													width="70" height="70" class="img-circle">
											</div>
											<div class="unit-body text-center offset-top-4">
												<p class="text-extra-small text-spacing-0 text-bold">
													<cite class="text-normal"><a
														href="#" class="text-turquoise">${document.userinfo.nickName}</a>
													</cite>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							</article>
							<br>
							<br>
	
						</c:forEach>
					</div>
					<div class="offset-top-30">
						<div class="hr bg-gray-lighter"></div>
					</div>
					<div class="offset-top-50">
						<!-- Classic Pagination-->
						<nav>
						<ul
							class="list-marked list-marked-type-2 list-marked-type-2-dot-1 list-marked-silver-chalice pagination-classic">
							<li class="text-regular"><a href="#"
								class="icon icon-sm icon-primary material-icons-ico material-icons-chevron_left"></a>
							</li>
							<li id="page1"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1&page_current=1">01</a>
										</li>
										<li id="page2"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1&page_current=2">02</a>
										</li>
										<li id="page3"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1&page_current=3">03</a>
										</li >
										<li id="page4"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1&page_current=4">04</a>
										</li>
										<li id="page5"><a href="skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1&page_current=5">05</a>
										</li>
										<script type="text/javascript">
											var num=${page_num};
											var div = document.getElementById("page"+num); 
											div.setAttribute("class", "active"); 
										</script>
							<li class="text-regular"><a href="#"
								class="icon icon-sm icon-primary material-icons-ico material-icons-chevron_right"></a>
							</li>
						</ul>
						</nav>
					</div>
				</div>
				<div
					class="cell-sm-10 cell-md-4 cell-lg-3 text-sm-left offset-top-10 offset-sm-top-5">
					<div class="range range-xs-center offset-top-0">
						<div class="cell-sm-3 cell-md-12">
							<div
								class="veil-sm reveal-md-block offset-top-0 offset-sm-top-0 offset-md-top-0"
								style="margin-top: -20px">
								<div class="hr bg-gray-lighter"></div>
							</div>
							<div
								class="cell-sm-10 cell-md-4 cell-lg-3 text-sm-left offset-top-64 offset-md-top-0">
								<p class="font-accent text-bold text-spacing-50 text-mine-shaft"
									style="margin-top: 30px">搜索</p>
								<div class="offset-top-5">
									<!-- RD Search Form-->
									<form action="skipToDocumentListAction_searchDocument?type=taganduserId&type_value=and-1" method="post"
										class="form-search blog-form-search rd-search">
										<div class="form-group">
											<input type="text" name="s" autocomplete="off"
												class="form-search-input form-control null">
										</div>
										<button type="submit" class="form-search-submit">
											<span
												class="icon icon-sm material-icons-ico material-icons-search"></span>
										</button>
									</form>
								</div>
								<div
									class="veil-sm reveal-md-block offset-top-30 offset-sm-top-20 offset-md-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
								<div class="offset-top-30">
									<p
										style="font-family: 微软雅黑; font-size : 18px; font-weight: bold; color :black ;">按日期分类</p>
								</div>
								<div class="offset-top-25">
									<ul
										class="list-marked text-extra-small text-bold p text-left font-accent">
										<li><a href="#" class="text-primary"
											style="font-size: 16px;">2016.6</a></li>
										<li class="offset-top-10"><a href="#"
											class="text-primary" style="font-size: 16px;">2016.7</a></li>
										<li class="offset-top-10"><a href="#"
											class="text-primary" style="font-size: 16px;">2016.8</a></li>
										<li class="offset-top-10"><a href="#"
											class="text-primary" style="font-size: 16px;">2016.9</a></li>
										<li class="offset-top-10"><a href="#"
											class="text-primary" style="font-size: 16px;">2016.10</a></li>
										<li class="offset-top-10"><a href="#"
											class="text-primary" style="font-size: 16px;">2016.11</a></li>
									</ul>
								</div>
								<div
									class="veil-sm reveal-md-block offset-top-30 offset-sm-top-0 offset-md-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
							</div>
							<div class="cell-sm-6 cell-md-12 offset-md-top-30">
								<p
									class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase">按标签分类</p>
								<div class="offset-top-20">
									<!-- List inline marked-->
									<ul
										class="list-inline list-tags font-accent text-bold text-spacing-inverse-25 text-left">
										<li><a href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=Javaand-1" class="text-turquoise">JAVA</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=Cshand-1">C#</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=CPPand-1">C++</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=PHPand-1">PHP</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=CSSand-1">CSS</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=HTMLand-1">HTML</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=IOSand-1">IOS</a></li>
										<li><a class="text-turquoise" href="skipToDocumentListAction_documentSkip?type=taganduserId&type_value=Javascriptand-1">Javascript</a></li>
									</ul>
								</div>
								<div
									class="veil-sm reveal-md-block offset-top-30 offset-sm-top-0 offset-md-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> </main>
		<!-- Page Footer-->
		<footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
	</div>
	<!-- Global Mailform Output-->
	<div id="form-output-global" class="snackbars"></div>
	<!-- PhotoSwipe Gallery-->
	<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button title="Close (Esc)"
						class="pswp__button pswp__button--close"></button>
					<button title="Share" class="pswp__button pswp__button--share"></button>
					<button title="Toggle fullscreen"
						class="pswp__button pswp__button--fs"></button>
					<button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>
				<button title="Previous (arrow left)"
					class="pswp__button pswp__button--arrow--left"></button>
				<button title="Next (arrow right)"
					class="pswp__button pswp__button--arrow--right"></button>
				<div class="pswp__caption">
					<div class="pswp__caption__cent"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Java script-->
	<script src="js/core.min.js"></script>
	<script src="js/script.js"></script>

	<script type="text/javascript">
		var innerContent_1 = "<article class=\"post-classic text-left\"><div class=\"post-classic-body offset-top-10\"><div class=\"unit unit-sm unit-sm-horizontal unit-sm-inverse\"><div class=\"unit-body\"><div class=\"post-body offset-top-15 offset-md-top-25\"><div class=\"cell-xs-8 cell-sm-5\" style=\'margin-top: -55px;\'><a href=";
		var docHref = "fileDetails.jsp?";
		var innerContent_2 = " class=\"post-vacancy\"><span class=\"unit unit-xs unit-xs-middle unit-xs-horizontal unit-spacing-xs text-xs-left\"><span class=\"unit-left\"></span></span><span class=\"unit-body\">";

		var innerContent_3 = "<span class=\"post-meta text-spacing-0\">";
		var uploadDateTime = "";
		var innerContent_4 = "</span><span class=\"post-title  reveal-block\"><span class=\"offset-top-5 font-accent text-bold text-spacing-50 text-uppercase\"><span class=\"text-mine-shaft\">";
		var documentName = "";
		var innerContent_5 = "</span></span></span><br><span class=\"text-spacing-0 reveal-block zhengwen\">";
		var documentDescription = "";
		var innerContent_6 = "<span class=\"text-extra-small text-bold text-turquoise text-uppercase\">详情</span></span></span></a></div></div></div></div></div></article><br><br>";
		function loadMyDoc() {
			$.ajax({
						type : "post",//请求方式  
						url : "${pageContext.request.contextPath}/browseDocAction_loadMyDocs",
						timeout : 800000,//超时时间：800秒  
						dataType : "json",//设置返回数据的格式  
						data : {},
						//请求成功后的回调函数 data为json格式  
						success : function(data) {
							if (data.retcode != "0000") {
								var errorMsg = data.errorMsg;
								alert("服务器出错了=-=");
							} else {
								var jsonArray = data.obj;
								var divText = "";
								for ( var i = 0; i < jsonArray.length; i++) {
									var userDoc = jsonArray[i];
									var docID = userDoc.docID;
									var curHref =  "\"" + docHref + "docID=" + docID + "\"";
									var docName = userDoc.docName;
									var docDescription = userDoc.docDescription;
									var praiseNum = userDoc.praiseNum;
									var commentNum = userDoc.commentNum;
									var downloadNum = userDoc.downloadNum;
									var forwardNum = userDoc.forwardNum;
									var collectNum = userDoc.collectNum;
									var docUploadTime = userDoc.docUploadTime;
									var docTags = userDoc.docTags.split(";");
									divText += innerContent_1 + curHref
											+ innerContent_2 + innerContent_3
											+ docUploadTime + innerContent_4
											+ docName + innerContent_5
											+ docDescription + innerContent_6;
								}
								$("#loadMyDocsDiv").empty().append(divText);
							}
						},
						//请求出错的处理  
						error : function() {
							alert("请求出错");
						}
					});
		}
	</script>
</body>
</html>