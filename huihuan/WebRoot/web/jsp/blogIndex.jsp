<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
<head>
<!-- Site Title-->
<title>博客区</title>
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
</head>
 <%
  	Object userid=session.getAttribute("userID");
	int loginstate;
	if(userid==null){
		loginstate=0;
	}
	else{
		loginstate=1;
		pageContext.setAttribute("userId", userid);
	}
	pageContext.setAttribute("loginstate", loginstate);
   %>
<body onload="detemineloginstate()">
	<!-- Page-->
	<div class="page text-center">
		<!-- Page Header-->
		<header class="page-header slider-menu-position">
            <!-- RD Navbar-->
            <div class="rd-navbar-wrap">
                <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px" class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static">
                    <div class="rd-navbar-inner">
                        <!-- RD Navbar Panel-->
                        <div class="rd-navbar-panel">
                            <!-- RD Navbar Toggle-->
                            <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                            <!-- RD Navbar Brand-->
                            <div class="rd-navbar-brand veil reveal-md-block"><a href="index.jsp" class="brand-name"><img style='margin-top: -10px;' width='164' height='29' src='images/logo-164x29.png' alt='' /></a></div>
                            <!-- <div class="rd-navbar-brand veil-md reveal-tablet-md-inline-block"><a href="index.jsp" class="brand-name"><img style='margin-top: -7px;' width='128' height='24' src='images/logo-128x24.png' alt='' /></a></div> -->
                            <!-- RD Navbar Toggle-->
                            <button data-rd-navbar-toggle=".rd-navbar-collapse-wrap" class="rd-navbar-collapse"><span></span></button>
                        </div>
                        <div id="loginState" class="rd-navbar-right-side">
                            <div class="rd-navbar-nav-wrap reveal-md-inline-block">
                                <ul class="rd-navbar-nav">
                                    <!-- RD Navbar Nav-->
                                    <li><a href="index.jsp" ">首页</a></li>
                                    <li  class="active"><a href="blogIndex.jsp" style="font-family: 微软雅黑">博客区</a></li>
                                    <li><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a></li>
                                    <li><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a></li>
<li><a href="#" style="font-family: 微软雅黑">发表</a>
										<ul class="rd-navbar-dropdown">
										<li><a href="postBlog.jsp">发表博客</a></li>
										<li><a href="forumPost.jsp">发表帖子</a></li>
										<li><a href="uploadFile.jsp">上传文件</a></li>
										</ul>
									</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
            <!-- Modern Breadcrumbs-->
        <section class="section-height-800 breadcrumb-modern rd-parallax context-dark">
          <div data-speed="0.2" data-type="media" data-url="images/backgrounds/background-11-1920x900.jpg" class="rd-parallax-layer"></div>
          <div data-speed="0" data-type="html" class="rd-parallax-layer">
            <div class="bg-overlay-chathams-blue">
              <div class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75">
              </div>
            </div>
          </div>
        </section>
        </header>
		<!-- Page Content-->
		<main
			class="page-content section-75 section-md-top-0 section-md-bottom-110">

		<!-- Yachts-->
		<section>
			<div class="shell">
				<div class="range range-xs-center offset-top-50">
					<div class="cell-sm-10 cell-lg-6 cell-lg-push-2">
						<!-- Post Classic-->


						<div class="offset-top-35">
							<!-- Post Classic-->
							<article class="post-classic text-left">
								<div class="post-classic-img-wrap">
									<a href="#"><img
										src="images/blog/post-blog-570x381.jpg" width="570"
										height="381" alt="" class="img-responsive center-block">
									</a>
								</div>

							</article>

							<div class="offset-top-35">
								<!-- Post Classic-->

								<div class="offset-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
							</div>
						</div>

					</div>
					<div
						class="cell-sm-10 cell-lg-3 cell-lg-push-1 text-sm-left offset-top-64 offset-lg-top-0">
						<p
							class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase wryh">搜索博客</p>
						<div class="offset-top-0">
							<!-- RD Search Form-->
							<form action="searchAction_searchBlog" method="post"
								class="form-search blog-form-search rd-search">
								<div class="form-group">
									<input id="searchtext" type="text" name="searchtext" autocomplete="off"
										class="form-search-input form-control null">
								</div>
								<button type="submit" class="form-search-submit" onclick="search()">
									<span
										class="icon icon-sm material-icons-ico material-icons-search"></span>
								</button>
							</form>
						</div>
						<div class="cell-sm-6 cell-md-4 cell-lg-12 offset-lg-top-55">
							<p
								class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase">标签分类</p>
							<div class="offset-top-20">
								<!-- List inline marked-->
								<ul
									class="list-inline list-tags font-accent text-bold text-spacing-inverse-25 text-left">
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=CPP"
										class="text-turquoise"> C++</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=SQL"
										class="text-turquoise">数据库</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=Java"
										class="text-turquoise">Java</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=Android"
										class="text-turquoise">Android</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=HTML"
										class="text-turquoise">CSS/HTML</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=Python"
										class="text-turquoise">Python</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=Csh"
										class="text-turquoise">C#</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=web"
										class="text-turquoise">web开发</a>
									</li>
									<li><a
										href="skipToBlogListAction_skip?type=tag&type_value=Linux"
										class="text-turquoise">Linux</a>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<div
						class="cell-sm-10 cell-lg-3 cell-lg-push-3 text-sm-left offset-top-44 offset-lg-top-0">
						<p
							class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase">最热博客</p>
						<div class="offset-top-25">
							<!-- Unit-->
							<div
								class="unit unit-middle unit-horizontal unit-spacing-xs text-left">
								<div class="unit-left">
									<img src="images/users/user-sandra-green-70x70.jpg" alt=" "
										width="70" height="70" class="img-circle">
								</div>
								<div class="unit-body">
									<div>
										<p class="text-extra-small text-bold">July 25, 2016</p>
									</div>
									<div class="offset-top-5">
										<p
											class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold wryh">
											<a href="#" class="text-turquoise">C++指针的用法</a>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="offset-top-25">
							<div class="hr bg-gray-lighter"></div>
						</div>
						<div class="offset-top-30">
							<!-- Unit-->
							<div
								class="unit unit-middle unit-horizontal unit-spacing-xs text-left">
								<div class="unit-left">
									<img src="images/users/user-tom-jefferson-70x70.jpg" alt=" "
										width="70" height="70" class="img-circle">
								</div>
								<div class="unit-body">
									<div>
										<p class="text-extra-small text-bold">July 20, 2016</p>
									</div>
									<div class="offset-top-5">
										<p
											class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
											<a href="#" class="text-turquoise">C#之装箱</a>
										</p>
									</div>
								</div>
							</div>
							<div class="offset-top-25">
								<div class="hr bg-gray-lighter"></div>
							</div>
							<div class="offset-top-30">
								<!-- Unit-->
								<div
									class="unit unit-middle unit-horizontal unit-spacing-xs text-left wryh">
									<div class="unit-left">
										<img src="images/users/user-mike-sanders-70x70.jpg" alt=" "
											width="70" height="70" class="img-circle">
									</div>
									<div class="unit-body">
										<div>
											<p class="text-extra-small text-bold">July 10, 2016</p>
										</div>
										<div class="offset-top-5">
											<p
												class="font-accent text-extra-small text-spacing-25 text-uppercase text-bold">
												<a href="#" class="text-turquoise">Java的String,StringBuffer与StringBuilder区别</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="offset-top-25">
								<div class="hr bg-gray-lighter"></div>
							</div>
						</div>

						<div class="range range-xs-center offset-top-55"></div>
					</div>
				</div>
			</div>
			<div class="shell">
				<div class="offset-top-0">
					<svg width="135" height="4" viewBox="0 0 135 4">
						<path style="fill:#efefef"
							d="M 2.3227821,2.0449621 -0.03800192,0.07659815 3.1569295,1.2434136 c 2.5401262,0.9276753 3.7227371,0.8843425 5.7702062,-0.2114297 1.9784883,-1.05885455 2.8486773,-1.10484315 3.7550683,-0.19845145 1.484362,1.48436175 5.031902,1.52768545 6.480446,0.079141 0.80087,-0.80087 2.091954,-0.7421935 4.740217,0.21543085 2.979173,1.0772827 4.014876,1.0816979 5.707987,0.024332 1.636053,-1.02173165 2.672814,-1.04140395 4.959784,-0.09411 2.271014,0.9406851 3.546951,0.926086 5.946995,-0.068045 2.294075,-0.95023691 3.340846,-0.9804738 4.2,-0.1213203 1.486272,1.48627181 5.021657,1.46707331 6.524159,-0.035429 0.906391,-0.9063917 1.77658,-0.8604031 3.755068,0.19845151 1.998288,1.0694512 3.235083,1.1291955 5.520207,0.2666579 2.160373,-0.81544991 3.729489,-0.81544991 5.889862,0 2.285124,0.8625376 3.521919,0.8027933 5.520207,-0.2666579 1.978488,-1.05885461 2.848677,-1.10484321 3.755068,-0.19845151 1.484362,1.48436181 5.031902,1.52768551 6.480446,0.079141 0.80087,-0.80087 2.071309,-0.7496588 4.664419,0.18802211 2.948695,1.0662618 4.004871,1.0526028 6.119554,-0.079141 1.963517,-1.05084213 2.82919,-1.09441383 3.735581,-0.18802223 1.484362,1.48436183 5.031906,1.52768553 6.480446,0.079141 0.80087,-0.80087 2.07131,-0.7496587 4.66442,0.18802203 2.90135,1.0491408 4.00935,1.0502085 5.96095,0.00574 1.95708,-1.04739773 2.78238,-1.04236583 4.49536,0.027409 1.72387,1.0765766 2.7384,1.078836 5.68684,0.012665 2.7988,-1.01205733 3.90688,-1.03357323 5.03447,-0.097755 0.7952,0.6599557 2.61547,0.976326 4.04505,0.703045 2.37877,-0.4547301 2.464,-0.3616463 1.00491,1.097441 -1.45908,1.4590874 -1.84642,1.4453923 -4.56661,-0.1614625 -2.74627,-1.6222625 -3.15865,-1.6336716 -5.42297,-0.1500332 -2.18024,1.4285487 -2.76388,1.4437811 -5.28891,0.1380357 -2.47017,-1.2773717 -3.19253,-1.2820786 -5.57019,-0.036295 -2.37022,1.241885 -3.09846,1.241885 -5.5,0 -2.40452,-1.2434263 -3.13028,-1.2416208 -5.52624,0.013748 C 99.763844,3.9190467 99.15734,3.9133435 97.292676,2.607281 95.404183,1.2845297 94.833305,1.2870756 92.236706,2.6298291 89.597447,3.9946429 89.064352,3.982798 86.815628,2.5093781 84.52735,1.0100401 84.120628,1.0069808 81.884292,2.4722843 79.735334,3.8803355 79.122431,3.8992836 76.704532,2.6324181 74.291896,1.3683104 73.641159,1.3851356 71.292806,2.7723415 68.845881,4.2177761 68.408458,4.2082649 66.042738,2.6581862 63.671614,1.1045661 63.264662,1.0965199 61.032448,2.5591227 58.880523,3.969118 58.285419,3.9846813 55.824584,2.69532 53.371101,1.4098101 52.768844,1.4213922 50.673394,2.7943834 48.512048,4.2105516 48.042674,4.1968405 45.408131,2.6405778 42.740652,1.0648591 42.325185,1.0568632 40.065069,2.5377482 37.884828,3.9662969 37.30119,3.9815293 34.776157,2.6757839 32.280231,1.3850906 31.599207,1.3855448 29.12951,2.6795501 26.631592,3.9883412 26.052244,3.9748646 23.889307,2.5576544 21.678633,1.1091646 21.182621,1.1064374 18.434276,2.5276615 15.749926,3.9157928 15.172991,3.9243054 13.287423,2.6036024 11.426177,1.2999346 10.817722,1.295134 8.3625636,2.5647457 4.8669338,4.3724061 5.142697,4.3961383 2.3227821,2.0449621 Z" /></svg>
				</div>
				<div class="offset-top-40">
					<div
						class="range range-xs-center offset-top-44 offset-md-top-53 text-sm-left">
						<div class="cell-sm-6 cell-md-4" style='margin-top: -40px;'>
							<div class="post-inspiration post-inspiration-img-auto-height">
								<div class="post-inspiration-img-wrap">
									<img src="images/blog/c++.jpg" width="962" height="491" alt="">
								</div>
								<div class="post-inspiration-body">
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="veil reveal-sm-inline-block">
										<span
											class="icon icon-lg thin-ico thin-icon-anchor text-white"></span>
									</div>
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="offset-top-20">
										<h3>C++</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a href="skipToBlogListAction_skip?type=tag&type_value=CPP"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>
						<div class="cell-sm-6 cell-md-4" style='margin-top: -40px;'>
							<div class="post-inspiration post-inspiration-img-auto-height">
								<div class="post-inspiration-img-wrap">
									<img src="images/blog/android.jpg" width="962" height="491"
										alt="">
								</div>
								<div class="post-inspiration-body">
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="veil reveal-sm-inline-block">
										<span
											class="icon icon-lg thin-ico thin-icon-anchor text-white"></span>
									</div>
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="offset-top-20">
										<h3>Android</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a
											href="skipToBlogListAction_skip?type=tag&type_value=Android"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>
						<div class="cell-sm-6 cell-md-4"  style='margin-top: -40px;'>
                              <div class="post-inspiration post-inspiration-img-auto-height">
                                <div class="post-inspiration-img-wrap"><img src="images/blog/SQL.jpg" width="962" height="491" alt=""></div>
                                <div class="post-inspiration-body">
                                  <div style="margin-bottom: 20px;" class="veil reveal-sm-inline-block text-bottom"><img src="images/divider-01-67x4.png" width="67" height="4" alt="" class="img-responsive center-block"></div>
                                  <div class="veil reveal-sm-inline-block"><span class="icon icon-lg thin-ico thin-icon-anchor text-white"></span></div>
                                  <div style="margin-bottom: 20px;" class="veil reveal-sm-inline-block text-bottom"><img src="images/divider-01-67x4.png" width="67" height="4" alt="" class="img-responsive center-block"></div>
                                  <div class="offset-top-20">
										<h3>数据库</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a href="skipToBlogListAction_skip?type=tag&type_value=SQL"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>
						<div class="cell-sm-6 cell-md-4 offset-top-60"
							style='margin-top: 30px;'>
							<div class="post-inspiration post-inspiration-img-auto-height">
								<div class="post-inspiration-img-wrap">
									<img src="images/blog/css.jpg" width="962" height="491" alt="">
								</div>
								<div class="post-inspiration-body">
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="veil reveal-sm-inline-block">
										<span
											class="icon icon-lg thin-ico thin-icon-anchor text-white"></span>
									</div>
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="offset-top-20">
										<h3>CSS/HTML</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a href="skipToBlogListAction_skip?type=tag&type_value=HTML"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>

						<div class="cell-sm-6 cell-md-4 offset-top-60"
							style='margin-top: 30px;'>
							<div class="post-inspiration post-inspiration-img-auto-height">
								<div class="post-inspiration-img-wrap">
									<img src="images/blog/Java.jpg" width="962" height="491" alt="">
								</div>
								<div class="post-inspiration-body">
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="veil reveal-sm-inline-block">
										<span
											class="icon icon-lg thin-ico thin-icon-anchor text-white"></span>
									</div>
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="offset-top-20">
										<h3>Java</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a href="skipToBlogListAction_skip?type=tag&type_value=Java"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>
						<div class="cell-sm-6 cell-md-4 offset-top-60"
							style='margin-top: 30px;'>
							<div class="post-inspiration post-inspiration-img-auto-height">
								<div class="post-inspiration-img-wrap">
									<img src="images/blog/Csh.jpg" width="962" height="491" alt="">
								</div>
								<div class="post-inspiration-body">
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="veil reveal-sm-inline-block">
										<span
											class="icon icon-lg thin-ico thin-icon-anchor text-white"></span>
									</div>
									<div style="margin-bottom: 20px;"
										class="veil reveal-sm-inline-block text-bottom">
										<img src="images/divider-01-67x4.png" width="67" height="4"
											alt="" class="img-responsive center-block">
									</div>
									<div class="offset-top-20">
										<h3>C#</h3>
									</div>
									<div style="max-width:200px;"
										class="btn-both-lines offset-top-30 center-block">
										<a href="skipToBlogListAction_skip?type=tag&type_value=Csh"
											style="min-width: 100px; margin-top: 10px"
											class="btn btn-primary">进入 </a>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<fdiv class="offset-top-44">
				<svg width="135" height="4" viewBox="0 0 135 4">
					<path style="fill:#efefef"
						d="M 2.3227821,2.0449621 -0.03800192,0.07659815 3.1569295,1.2434136 c 2.5401262,0.9276753 3.7227371,0.8843425 5.7702062,-0.2114297 1.9784883,-1.05885455 2.8486773,-1.10484315 3.7550683,-0.19845145 1.484362,1.48436175 5.031902,1.52768545 6.480446,0.079141 0.80087,-0.80087 2.091954,-0.7421935 4.740217,0.21543085 2.979173,1.0772827 4.014876,1.0816979 5.707987,0.024332 1.636053,-1.02173165 2.672814,-1.04140395 4.959784,-0.09411 2.271014,0.9406851 3.546951,0.926086 5.946995,-0.068045 2.294075,-0.95023691 3.340846,-0.9804738 4.2,-0.1213203 1.486272,1.48627181 5.021657,1.46707331 6.524159,-0.035429 0.906391,-0.9063917 1.77658,-0.8604031 3.755068,0.19845151 1.998288,1.0694512 3.235083,1.1291955 5.520207,0.2666579 2.160373,-0.81544991 3.729489,-0.81544991 5.889862,0 2.285124,0.8625376 3.521919,0.8027933 5.520207,-0.2666579 1.978488,-1.05885461 2.848677,-1.10484321 3.755068,-0.19845151 1.484362,1.48436181 5.031902,1.52768551 6.480446,0.079141 0.80087,-0.80087 2.071309,-0.7496588 4.664419,0.18802211 2.948695,1.0662618 4.004871,1.0526028 6.119554,-0.079141 1.963517,-1.05084213 2.82919,-1.09441383 3.735581,-0.18802223 1.484362,1.48436183 5.031906,1.52768553 6.480446,0.079141 0.80087,-0.80087 2.07131,-0.7496587 4.66442,0.18802203 2.90135,1.0491408 4.00935,1.0502085 5.96095,0.00574 1.95708,-1.04739773 2.78238,-1.04236583 4.49536,0.027409 1.72387,1.0765766 2.7384,1.078836 5.68684,0.012665 2.7988,-1.01205733 3.90688,-1.03357323 5.03447,-0.097755 0.7952,0.6599557 2.61547,0.976326 4.04505,0.703045 2.37877,-0.4547301 2.464,-0.3616463 1.00491,1.097441 -1.45908,1.4590874 -1.84642,1.4453923 -4.56661,-0.1614625 -2.74627,-1.6222625 -3.15865,-1.6336716 -5.42297,-0.1500332 -2.18024,1.4285487 -2.76388,1.4437811 -5.28891,0.1380357 -2.47017,-1.2773717 -3.19253,-1.2820786 -5.57019,-0.036295 -2.37022,1.241885 -3.09846,1.241885 -5.5,0 -2.40452,-1.2434263 -3.13028,-1.2416208 -5.52624,0.013748 C 99.763844,3.9190467 99.15734,3.9133435 97.292676,2.607281 95.404183,1.2845297 94.833305,1.2870756 92.236706,2.6298291 89.597447,3.9946429 89.064352,3.982798 86.815628,2.5093781 84.52735,1.0100401 84.120628,1.0069808 81.884292,2.4722843 79.735334,3.8803355 79.122431,3.8992836 76.704532,2.6324181 74.291896,1.3683104 73.641159,1.3851356 71.292806,2.7723415 68.845881,4.2177761 68.408458,4.2082649 66.042738,2.6581862 63.671614,1.1045661 63.264662,1.0965199 61.032448,2.5591227 58.880523,3.969118 58.285419,3.9846813 55.824584,2.69532 53.371101,1.4098101 52.768844,1.4213922 50.673394,2.7943834 48.512048,4.2105516 48.042674,4.1968405 45.408131,2.6405778 42.740652,1.0648591 42.325185,1.0568632 40.065069,2.5377482 37.884828,3.9662969 37.30119,3.9815293 34.776157,2.6757839 32.280231,1.3850906 31.599207,1.3855448 29.12951,2.6795501 26.631592,3.9883412 26.052244,3.9748646 23.889307,2.5576544 21.678633,1.1091646 21.182621,1.1064374 18.434276,2.5276615 15.749926,3.9157928 15.172991,3.9243054 13.287423,2.6036024 11.426177,1.2999346 10.817722,1.295134 8.3625636,2.5647457 4.8669338,4.3724061 5.142697,4.3961383 2.3227821,2.0449621 Z" /></svg>
			</div>
	</div>
	</section>
	</main>
	<!-- Page Footer-->
	<footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
	</div>
	<!-- Global Mailform Output-->
	<div id="form-output-global" class="snackbars"></div>
	<!-- PhotoSwipe Gallery-->
	<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button title="Close (Esc)"
						class="pswp__button pswp__button--close"></button>
					<button title="Share" class="pswp__button pswp__button--share"></button>
					<button title="Toggle fullscreen"
						class="pswp__button pswp__button--fs"></button>
					<button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>
				<button title="Previous (arrow left)"
					class="pswp__button pswp__button--arrow--left"></button>
				<button title="Next (arrow right)"
					class="pswp__button pswp__button--arrow--right"></button>
				<div class="pswp__caption">
					<div class="pswp__caption__cent"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Java script-->
	<script src="js/core.min.js"></script>
	<script src="js/script.js"></script>
	    <script type="text/javascript">
    function detemineloginstate(){
    	var login="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
    	login+="<ul class='list-inline list-inline-0 list-primary'>";
    	login+="<li class='text-center label offset-left text-spacing-20'><a href='userLogIn.jsp' class='icon icon-xxs fa fa-user text-white'> 登录/注册</a>";
    	login+="</li></ul></div>";
    	var havelogin="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
		havelogin+="<ul class='list-inline list-inline-0 list-primary'>";
		havelogin+=" <li class='text-center'><a href='#' class='icon icon-xxs fa fa-edit text-white'></a></li>";
		havelogin+="<li class='text-center'><a href='#' class='icon icon-xxs fa fa-bell-o text-white'></a></li></ul></div> ";
		havelogin+="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
        havelogin+="<a href='skipToMyPageAction_skipToMyPage'><img src='images/users/user-sandra-green-70x70.jpg' alt='' width='40' height='40' class='img-circle box-comment-img'></a> </div> ";
    	var loginstate=${loginstate};
    	if(loginstate=="0"){
    		$("#loginState").append($(login));
    	}else if(loginstate=="1"){
    		$("#loginState").append($(havelogin));
    	}

    }
    function search(){
    	var text=document.document.getElementById("searchtext").value;
    	$.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/searchBlogAction_searchBlog",
               timeout:80000,//超时时间：8秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"typeValue":text
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                  if(data.retcode=="0001")
                  {
                  	  alert("已经存在");
                  }
                  else if(data.retcode=="0000"){
                      AddElementInChoosing("tag"+num,new_tag);
                      num++;
                  }else{
                  	alert("请求出错");
                  }
                  //alert(data.yourName+"你输入的内容:"+data.yourContent);  
              },  
              //请求出错的处理  
              error:function(){  
                        alert("请求出错");  
              }  
           }); 
    }
    </script>
</body>
</html>
