<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
<head>
<title>上传资料</title>
<meta name="format-detection" content="telephone=no">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->

<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
<script src="js/jquery.js"></script>
<script src="js/ajaxfileupload.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<!--<script type="text/javascript" src="js/core.min.js">-->
<!-- <ajaxfileupload与core.min.js冲突> -->
<script type="text/javascript">
	function fileupload() {
		/* if($("#uploadFile").val()==""){  
		        alert("上传文件不能为空!");  
		        return false;  
		    }   */
		var doc_name = $("#upload-doc-name").val();
		var doc_tag = $("#doc-tag-name").val();
		var doc_description = $("#doc-description").val();
		$.ajaxFileUpload({
			url : "${pageContext.request.contextPath}/uploadFileAction_upload", //用于文件上传的服务器端请求地址
			secureuri : false, //是否需要安全协议
			fileElementId : 'uploadFile', //文件上传域的ID
			data : {
				"doc_name" : doc_name,
				"doc_tag" : doc_tag,
				"doc_description" : doc_description
			},
			dataType : 'json', //返回值类型 
			success : function(data, status) //服务器成功响应处理函数
			{
				if (data.retcode == "0000") {
					alert("上传成功！");
					window.location.href = "skipToMyDocumentsAction_documentSkip?type=userId&type_value=-1";
				} else
					alert("文件已存在0.0");
			},
			error : function(data, status, e)//服务器响应失败处理函数
			{
				alert("数据库操作出错，上传失败了...");
			}
		});
		return false;
	}
</script>
</head>
<body>
	<!-- Page-->
	<div class="page text-center">
		<!-- Page Header-->
		<header class="page-header slider-menu-position"> <!-- RD Navbar-->
		<div class="rd-navbar-wrap">
			<nav data-md-device-layout="rd-navbar-fixed"
				data-lg-device-layout="rd-navbar-static"
				data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px"
				class="rd-navbar" data-layout="rd-navbar-fixed"
				data-sm-layout="rd-navbar-fixed"
				data-md-layout="rd-navbar-fullwidth"
				data-lg-layout="rd-navbar-static">
			<div class="rd-navbar-inner">
				<!-- RD Navbar Panel-->
				<div class="rd-navbar-panel">
					<!-- RD Navbar Toggle-->
					<button data-rd-navbar-toggle=".rd-navbar-nav-wrap"
						class="rd-navbar-toggle">
						<span></span>
					</button>
					<!-- RD Navbar Brand-->
					<div class="rd-navbar-brand veil reveal-md-block">
						<a href="index.jsp" class="brand-name"><img
							style='margin-top: -10px;' width='164' height='29'
							src='images/logo-164x29.png' alt='' />
						</a>
					</div>
					<div class="rd-navbar-brand veil-md reveal-tablet-md-inline-block">
						<a href="index.jsp" class="brand-name"><img
							style='margin-top: -7px;' width='128' height='24'
							src='images/logo-128x24.png' alt='' />
						</a>
					</div>
					<!-- RD Navbar Toggle-->
					<button data-rd-navbar-toggle=".rd-navbar-collapse-wrap"
						class="rd-navbar-collapse">
						<span></span>
					</button>
				</div>
				<div class="rd-navbar-right-side">
					<div class="rd-navbar-nav-wrap reveal-md-inline-block">
						<ul class="rd-navbar-nav">
							<!-- RD Navbar Nav-->
							<li class="active"><a href="index.jsp"">首页</a></li>
                    <li><a href="blogIndex.jsp" style="font-family: 微软雅黑">博客区</a></li>
                    <li><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a></li>
                    <li><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a></li>
                   <li><a href="#" style="font-family: 微软雅黑">发表</a>
										<ul class="rd-navbar-dropdown">
										<li><a href="postBlog.jsp">发表博客</a></li>
										<li class="active"><a href="forumPost.jsp">发表帖子</a></li>
										<li><a href="uploadFile.jsp">上传文件</a></li>
										
										</ul>
									</li>
						</ul>
					</div>
				</div>
			</div>
			</nav>
		</div>
		<!-- Modern Breadcrumbs--> <section
			class="section-height-800 breadcrumb-modern rd-parallax context-dark">
		<div data-speed="0.2" data-type="media"
			data-url="images/backgrounds/background-01-1920x900.jpg"
			class="rd-parallax-layer"></div>
		<div data-speed="0" data-type="html" class="rd-parallax-layer">
			<div class="bg-overlay-chathams-blue">
				<div
					class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75">
					<div class="veil reveal-md-block">
						<div
							class="text-extra-big font-accent font-passage text-bold text-spacing-inverse-50">上传资料</div>
					</div>

				</div>
			</div>
		</div>
		</section> </header>
		<!-- Page Content-->
		<main
			class="page-content section-75 section-md-top-90 section-md-bottom-110">
		<section>
		<div class="shell">
			<div class="range range-xs-center offset-top-0">
				<div class="cell-sm-6 cell-lg-4">
					<table align="center">
						<tr>
							<td>
								<div class="offset-top-35">
									<input type="file" id="uploadFile" class="" name="uploadFile"
										onchange="get_file_name(this)" 
										accept=".pdf,.doc,.docx,.ppt,.pptx,.zip,.rar">
								</div>
								<div id="file_upload_div">
									请选择pdf/doc/docx/ppt/pptx/.zip/.rar格式的文件，大小不超过200MB，文件数量1个。</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="offset-top-35">
									<table>
										<tr>
											<td>
												<div class="cell-sm-6">
													<label>资料名称：</label>
													<div class="form-group">
														<!-- <label for="upload-doc-name" class="form-label">给资料取个醒目的名称吧~</label> -->
														<input id="upload-doc-name" type="text"
															name="upload-doc-name" required class="form-control">
													</div>
												</div></td>
										</tr>
										<tr>
											<td>
												<div class="cell-sm-6">
													<label>资料标签/关键词(请以";"为标签分隔符):</label>
													<div class="form-group">
														<!-- <label for="doc-tag-name" class="form-label">资料相关的标签和关键词</label> -->
														<input id="doc-tag-name" type="text" name="doc-tag-name"
															required class="form-control">
													</div>
												</div></td>
										</tr>
										<tr>
											<td>
												<div class="cell-sm-6">
													<label>资料描述：</label>
													<div class="form-group">
														<!-- <label for="doc-description" class="form-label">描述一下你上传的宝贝资料吧~</label> -->
														<textarea id="doc-description" rows="5"
															name="doc-description" required class="form-control"></textarea>
													</div>
												</div></td>
										</tr>
									</table>
								</div>
								<div id="file_error" style="color: red"></div>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div class="offset-top-35">
									<button type="submit" class="btn btn-primary"
										onclick="confirmUpload()">确认上传</button>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		</section> </main>
		<!-- Page Footer-->
		<footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
	</div>
	<!-- Global Mailform Output-->
	<div id="form-output-global" class="snackbars"></div>
	<!-- PhotoSwipe Gallery-->
	<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button title="Close (Esc)"
						class="pswp__button pswp__button--close"></button>
					<button title="Share" class="pswp__button pswp__button--share"></button>
					<button title="Toggle fullscreen"
						class="pswp__button pswp__button--fs"></button>
					<button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>
				<button title="Previous (arrow left)"
					class="pswp__button pswp__button--arrow--left"></button>
				<button title="Next (arrow right)"
					class="pswp__button pswp__button--arrow--right"></button>
				<div class="pswp__caption">
					<div class="pswp__caption__cent"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Java script-->
	<script type="text/javascript">
		var isIE = /msie/i.test(navigator.userAgent) && !window.opera;
		var is_valid = false;
		var filePath = "";
		function get_file_name(target) {
			var file = $("#uploadFile").val();
			/*  var strFileName = file.replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi,
			     "$1"); //正则表达式获取文件名，不带	后缀 */
			var strFileName = file.substring(0,file.lastIndexOf("."));
			strFileName = strFileName.substring(strFileName.lastIndexOf("\\")+1);
			var FileExt = file.replace(/.+\./, ""); //正则表达式获取后缀
			document.getElementById("upload-doc-name").value = strFileName
					+ "." + FileExt;
			var addHtml = "";
			is_valid = true;
			if (FileExt != "pdf" && FileExt != "doc" && FileExt != "docx"
					&& FileExt != "ppt" && FileExt != "pptx"
					&& FileExt != "zip" && FileExt != "rar") {
				addHtml = "文件类型错误=-=";
				is_valid = false;
			} else {
				is_valid = true;
				var fileSize = 0;
				if (isIE && !target.files) { // IE浏览器
					filePath = target.value; // 获得上传文件的绝对路径
					var fileSystem = new ActiveXObject(
							"Scripting.FileSystemObject");
					// GetFile(path) 方法从磁盘获取一个文件并返回。
					var file = fileSystem.GetFile(filePath);
					fileSize = file.Size; // 文件大小，单位：b
				} else { // 非IE浏览器
					fileSize = target.files[0].size;
				}
				var size = fileSize / 1024 / 1024;
				if (size > 200) {
					is_pdf_correct = false;
					addHtml = "文件大小超过200M，请重新上传0.0";
				}
			}
			$("#file_error").empty().append(addHtml);
		}
		function check() {
			//document.getElementById("file_upload_div").style.color = "red";
			setTimeout(
					"document.getElementById(\"file_upload_div\").style.color=\"\"",
					200);
			return is_valid;
		}
		//form的onsubmit是在button的onclick函数执行完之后再执行的
		function confirmUpload() {
			var doc_name = $("#upload-doc-name").val();
			var doc_tag = $("#doc-tag-name").val();
			var doc_description = $("#doc-description").val();
			if (doc_name == "") {
				$("#file_error").empty().append("请填写资料名称");
				is_valid = false;
				return;
			}
			if (doc_tag == "") {
				$("#file_error").empty().append("请填写资料标签(请以\";\"为分隔符)");
				is_valid = false;
				return;
			}
			if (doc_description == "") {
				$("#file_error").empty().append("请填写资料描述");
				is_valid = false;
				return;
			}
			var validInfo = check();
			if (validInfo == false) {
				alert("请上传文件!");
				return;
			}
			fileupload();
		}
	</script>
</body>
</html>