<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String url = request.getScheme()+"://"+ request.getServerName()+request.getRequestURI()+"?"+request.getQueryString();
%>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<!-- Site Title-->
<title>blog list</title>
<meta name="format-detection" content="telephone=no">
<meta name="viewport"
	content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Stylesheets-->
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
<link rel="stylesheet" href="css/style.css">
<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
</head>
<%
  	Object userid=session.getAttribute("userID");
	int loginstate;
	if(userid==null){
		loginstate=0;
	}
	else{
		loginstate=1;
		pageContext.setAttribute("userId", userid);
	}
	pageContext.setAttribute("loginstate", loginstate);
   %>
<body onload="detemineloginstate()">
	<% 
	List list=(List)request.getAttribute("blogList");
	String typevalue=(String)request.getParameter("type_value");
	typevalue=typevalue.split("and")[0];
	pageContext.setAttribute("typevalue", typevalue);
	pageContext.setAttribute("list", list);
	String typeValue=(String)session.getAttribute("typeValue");
	pageContext.setAttribute("typeValue", typeValue.split("and")[0]);
	if(typeValue.contains("and")){
		pageContext.setAttribute("isuserId", "and"+typeValue.split("and")[1]);
	}else{
		pageContext.setAttribute("isuserId", "");
	}
    int count=0; //总行数
    int page_count=0;  //开始条数
    int page_total=1;  //，总页码
    int page_current= 1;  //首页
    int page_size=5;//一页的行数
    String page_cu = request.getParameter("page_current");  
    if(page_cu==null){  
       page_cu="1";  
    }  
    pageContext.setAttribute("page_num", page_cu);
    page_current = Integer.parseInt(page_cu)-1;
    if(page_current<0){
       page_current = 1;  
    } 
    page_count=page_count+ page_current*page_size;
     %>
	<!-- Page-->
	<div class="page text-center">
		<!-- Page Header-->
		<header class="page-header slider-menu-position">
			<!-- RD Navbar-->
			<div class="rd-navbar-wrap">
				<nav data-md-device-layout="rd-navbar-fixed"
					data-lg-device-layout="rd-navbar-static"
					data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px"
					class="rd-navbar" data-layout="rd-navbar-fixed"
					data-sm-layout="rd-navbar-fixed"
					data-md-layout="rd-navbar-fullwidth"
					data-lg-layout="rd-navbar-static">
					<div class="rd-navbar-inner">
						<!-- RD Navbar Panel-->
						<div class="rd-navbar-panel">
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-nav-wrap"
								class="rd-navbar-toggle">
								<span></span>
							</button>
							<!-- RD Navbar Brand-->
							<div class="rd-navbar-brand veil reveal-md-block">
								<a href="index.jsp" class="brand-name"><img
									style='margin-top: -10px;' width='164' height='29'
									src='images/logo-164x29.png' alt='' />
								</a>
							</div>
							<!-- <div
								class="rd-navbar-brand veil-md reveal-tablet-md-inline-block">
								<a href="index.jsp" class="brand-name"><img
									style='margin-top: -7px;' width='128' height='24'
									src='images/logo-128x24.png' alt='' />
								</a>
							</div> -->
							<!-- RD Navbar Toggle-->
							<button data-rd-navbar-toggle=".rd-navbar-collapse-wrap"
								class="rd-navbar-collapse">
								<span></span>
							</button>
						</div>
						<div  id="loginState" class="rd-navbar-right-side">
							<div class="rd-navbar-nav-wrap reveal-md-inline-block">
								<ul class="rd-navbar-nav">
									<!-- RD Navbar Nav-->
									<li><a href="index.jsp" style="font-family: 微软雅黑">首页</a>
									</li>
									<li class="active"><a href="blogIndex.jsp"
										style="font-family: 微软雅黑">博客区</a>
									</li>
									<li><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a>
									</li>
									<li><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a>
									</li>
									<li><a href="#" style="font-family: 微软雅黑">发表</a>
										<ul class="rd-navbar-dropdown">
										<li><a href="postBlog.jsp">发表博客</a></li>
										<li><a href="forumPost.jsp">发表帖子</a></li>
										<li><a href="uploadFile.jsp">上传文件</a></li>
										</ul>
									</li>
								</ul>
							</div>
							
						</div>
					</div>
				</nav>
			</div>
			<!-- Modern Breadcrumbs-->
			<section
				class="section-height-800 breadcrumb-modern rd-parallax context-dark">
				<div data-speed="0.2" data-type="media"
					data-url="images/backgrounds/background-04-1920x900.jpg"
					class="rd-parallax-layer"></div>
				<div data-speed="0" data-type="html" class="rd-parallax-layer">
					<div class="bg-overlay-chathams-blue">
						<div
							class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75">
							<div class="veil reveal-md-block">
								<div
									class="text-extra-big font-accent text-bold text-spacing-inverse-50">${typeValue}</div>
							</div>

						</div>
					</div>
				</div>
			</section>
		</header>
		<!-- Page Content-->
		<main
			class="page-content section-75 section-md-top-0 section-md-bottom-95">
		<!-- Blog Classic Both Sidebar-->
		<section>
			<div class="shell">
				<div class="range range-xs-center range-lg-left offset-top-50">

					<div class="cell-sm-10 cell-md-8 cell-md-push-1">

						<!-- Post Classic-->
						<c:forEach items="${list}"  var="blog"  step="1" varStatus="i"  begin="<%=page_count%>"  end="<%=page_size+page_count-1%>">
						<article class="post-classic text-left">

							<div class="post-classic-body offset-top-30">
								<div class="unit unit-sm unit-sm-horizontal unit-sm-inverse">
									<div class="unit-body">
										<!-- Post Body-->
										<div class="post-body offset-top-15 offset-md-top-25">
											<div class="cell-xs-8 cell-sm-5" style='margin-top: -55px;'>
												<!-- Post Vacancy-->
												<a href="skipToSingleBlogAction_skip?blogID=${blog.blogId }" class="post-vacancy"> <!-- Unit-->
													<span
													class="unit unit-xs unit-xs-middle unit-xs-horizontal unit-spacing-xs text-xs-left"><span
														class="unit-left"></span><span class="unit-body"> <span
															class="post-meta"> <span class="text-spacing-0">${blog.publishTime}</span>
														</span> <span class="post-title  reveal-block"> <span
																class="offset-top-5 font-accent text-bold text-spacing-50 text-uppercase"><span
																	class="text-mine-shaft">${blog.blogTitle}</span> </span> </span>
															<br> <span
															class="text-spacing-0 reveal-block zhengwen">${blog.blogContent}
																<span
																class="text-extra-small text-bold text-turquoise text-uppercase">更多</span>
														</span> </span>
												</span>
												</a>
											</div>

										</div>

									</div>
									<div class="unit-right" style='margin-top: 10px;'>
										<div
											class="unit unit-horizontal unit-middle unit-sm-vertical unit-spacing-xs">
											<div class="unit-left">
												<img src="images/users/user-sandra-green-70x70.jpg" alt=" "
													width="70" height="70" class="img-circle">
											</div>
											<div class="unit-body text-center offset-top-4">
												<p class="text-extra-small text-spacing-0 text-bold">
													<cite class="text-normal"><a
														href="#" class="text-turquoise">${blog.userinfo.nickName}</a>
													</cite>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</article>
						<br>
						</c:forEach> 

						<div class="offset-top-35">
							<!-- Post Classic-->
							<div class="offset-top-35">
								<!-- Post Classic-->
								<div class="offset-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
							</div>
							<div class="offset-top-50">
								<!-- Classic Pagination-->
								<nav>
									<ul
										class="list-marked list-marked-type-2 list-marked-type-2-dot-1 list-marked-silver-chalice pagination-classic">
										<li class="text-regular"><a href="#"
											class="icon icon-sm icon-primary material-icons-ico material-icons-chevron_left"></a>
										</li>
										<li id="page1"><a href="skipToBlogListAction_skip?type=${type }&type_value=${typevalue}${isuserId}&page_current=1">01</a>
										</li>
										<li id="page2"><a href="skipToBlogListAction_skip?type=${type }&type_value=${typevalue}${isuserId}&page_current=2">02</a>
										</li>
										<li id="page3"><a href="skipToBlogListAction_skip?type=${type }&type_value=${typevalue}${isuserId}&page_current=3">03</a>
										</li >
										<li id="page4"><a href="skipToBlogListAction_skip?type=${type }&type_value=${typevalue}${isuserId}&page_current=4">04</a>
										</li>
										<li id="page5"><a href="skipToBlogListAction_skip?type=${type }&type_value=${typevalue}${isuserId}&page_current=5">05</a>
										</li>
										<script type="text/javascript">
											var num=${page_num};
											var div = document.getElementById("page"+num); 
											div.setAttribute("class", "active"); 
										</script>
										<li class="text-regular"><a href="#"
											class="icon icon-sm icon-primary material-icons-ico material-icons-chevron_right"></a>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
					<div
						class="cell-sm-10 cell-md-4 cell-lg-3 text-sm-left offset-top-64 offset-md-top-0">
						<p
							class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase"
							style="margin-top: 30px">搜索文章</p>
						<div class="offset-top-10">
							<!-- RD Search Form-->
							<form action="skipToBlogListAction_searchBlog?type=${type }&type_value=CPP${isuserId}" method="post"
								class="form-search blog-form-search rd-search">
								<div class="form-group">
									<input id="searchtext" type="text" name="searchtext" autocomplete="off"
										class="form-search-input form-control null">
								</div>
								<button type="submit" class="form-search-submit" onclick="search()">
									<span
										class="icon icon-sm material-icons-ico material-icons-search"></span>
								</button>
							</form>
						</div>
						<div class="offset-top-35">
							<div class="hr bg-gray-lighter"></div>
						</div>
						<br>
						<div class="range range-xs-center offset-top-55">
							<div class="cell-sm-3 cell-md-12" style="margin-top: -120px">
								<p class="font-accent text-bold text-spacing-50 text-mine-shaft text-uppercase offset-top-55">博文分类</p>
								<div class="offset-top-10">
									<ul class="list-inline list-tags font-accent text-bold text-spacing-inverse-25 text-left">
										<li><a href="skipToBlogListAction_skip?type=${type }&type_value=CPP${isuserId}" class="text-primary">C++</a>
										</li>
										<li class="offset-top-10"><a href="skipToBlogListAction_skip?type=${type }&type_value=Java${isuserId}"
											class="text-primary">Java</a>
										</li>
										<li class="offset-top-10"><a href="skipToBlogListAction_skip?type=${type }&type_value=PHP${isuserId}"
											class="text-primary">PHP</a>
										</li>
										<li class="offset-top-10"><a href="skipToBlogListAction_skip?type=${type }&type_value=SQL${isuserId}"
											class="text-primary">数据库</a>
										</li>
										<li class="offset-top-10"><a href="skipToBlogListAction_skip?type=${type }&type_value=IOS${isuserId}"
											class="text-primary">IOS</a>
										</li>
										<li class="offset-top-10"><a href="#"
											class="text-primary">其他</a>
										</li>
									</ul>
								</div>
								<div
									class="veil-sm reveal-md-block offset-top-30 offset-sm-top-0 offset-md-top-30">
									<div class="hr bg-gray-lighter"></div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</section>
		</main>
		<!-- Page Footer-->
		<footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
	</div>
	<!-- Global Mailform Output-->
	<div id="form-output-global" class="snackbars"></div>
	<!-- PhotoSwipe Gallery-->
	<div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">
				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>
					<button title="Close (Esc)"
						class="pswp__button pswp__button--close"></button>
					<button title="Share" class="pswp__button pswp__button--share"></button>
					<button title="Toggle fullscreen"
						class="pswp__button pswp__button--fs"></button>
					<button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>
				<div
					class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
					<div class="pswp__share-tooltip"></div>
				</div>
				<button title="Previous (arrow left)"
					class="pswp__button pswp__button--arrow--left"></button>
				<button title="Next (arrow right)"
					class="pswp__button pswp__button--arrow--right"></button>
				<div class="pswp__caption">
					<div class="pswp__caption__cent"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Java script-->
	<script src="js/core.min.js"></script>
	<script src="js/script.js"></script>
		    <script type="text/javascript">
    function detemineloginstate(){
    	var login="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
    	login+="<ul class='list-inline list-inline-0 list-primary'>";
    	login+="<li class='text-center label offset-left text-spacing-20'><a href='userLogIn.jsp' class='icon icon-xxs fa fa-user text-white'> 登录/注册</a>";
    	login+="</li></ul></div>";
    	var havelogin="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
		havelogin+="<ul class='list-inline list-inline-0 list-primary'>";
		havelogin+=" <li class='text-center'><a href='#' class='icon icon-xxs fa fa-edit text-white'></a></li>";
		havelogin+="<li class='text-center'><a href='#' class='icon icon-xxs fa fa-bell-o text-white'></a></li></ul></div> ";
		havelogin+="<div class='rd-navbar-collapse-wrap reveal-md-inline-block'>";
        havelogin+="<a href='skipToMyPageAction_skipToMyPage'><img src='images/users/user-sandra-green-70x70.jpg' alt='' width='40' height='40' class='img-circle box-comment-img'></a> </div> ";
    	var loginstate=${loginstate};
    	if(loginstate=="0"){
    		$("#loginState").append($(login));
    	}else if(loginstate=="1"){
    		$("#loginState").append($(havelogin));
    	}

    }
    function search(){
    	var text=document.document.getElementById("searchtext").value;
    	$.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/searchBlogAction_searchBlog",
               timeout:80000,//超时时间：8秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"typeValue":text
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                  if(data.retcode=="0001")
                  {
                  	  alert("已经存在");
                  }
                  else if(data.retcode=="0000"){
                      AddElementInChoosing("tag"+num,new_tag);
                      num++;
                  }else{
                  	alert("请求出错");
                  }
                  //alert(data.yourName+"你输入的内容:"+data.yourContent);  
              },  
              //请求出错的处理  
              error:function(){  
                        alert("请求出错");  
              }  
           }); 
    }
    </script>
</body>
</html>
