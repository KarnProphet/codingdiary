<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en" class="wide wow-animation smoothscroll scrollTo">
  <head>
    <title>登录</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400%7CQuicksand:400,700">
    <link rel="stylesheet" href="css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-header slider-menu-position" >
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="50px" data-lg-stick-up-offset="1px" class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand veil reveal-md-block"><a href="index.jsp" class="brand-name"><img style='margin-top: -10px;' width='164' height='29' src='images/logo-164x29.png' alt=''/></a></div>
              
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar-collapse-wrap" class="rd-navbar-collapse"><span></span></button>
              </div>
				<div id="loginState" class="rd-navbar-right-side">
					<div class="rd-navbar-nav-wrap reveal-md-inline-block">
						<ul class="rd-navbar-nav">
							<!-- RD Navbar Nav-->
							<li class="active"><a href="index.jsp"">首页</a>
							</li>
							<li><a href="blogIndex.jsp" style="font-family: 微软雅黑">博客区</a>
							</li>
							<li><a href="bbs-index.jsp" style="font-family: 微软雅黑">论坛讨论区</a>
							</li>
							<li><a href="skipToDataIndexAction_documentSkip?type=userId&type_value=0" style="font-family: 微软雅黑">学习资料区</a>
							</li>

						</ul>
					</div>
					<div class="rd-navbar-collapse-wrap reveal-md-inline-block">
						<ul class="list-inline list-inline-0 list-primary">
							<li class="text-center label offset-left text-spacing-20"><a
								href="#" class="icon icon-xxs fa fa-user text-white"> 登录/注册</a>
							</li>

						</ul>
					</div>
				</div>
			</div>
          </nav>
        </div>
        <!-- Modern Breadcrumbs--> <section
		class="section-height-800 breadcrumb-modern rd-parallax context-dark">
	<div data-speed="0.2" data-type="media"
		data-url="images/backgrounds/background-02-1920x900.jpg"
		class="rd-parallax-layer"></div>
	<div data-speed="0" data-type="html" class="rd-parallax-layer">
		<div class="bg-overlay-chathams-blue">
			<div
				class="shell section-top-34 section-bottom-34 section-md-top-175 section-md-bottom-75 section-lg-top-158 section-lg-bottom-125 section-md-tablet-75"></div>
		</div>
	</div>
	</section>
      </header>
      <!-- Page Content-->
      <main class="page-content section-75 section-md-top-90 section-md-bottom-110">
        <!-- Log In-->
        <section>
          <div class="shell">
            <div>
              <h1>登  录</h1>
            </div>
            <div class="range range-xs-center offset-top-44">
              <div class="cell-md-10 cell-lg-8">
                <!-- RD Mailform-->
                <form class="rd-mailform">
                  <div class="range range-xs-center">
                    <div class="cell-sm-6">
                      <div class="form-group">
                        <label for="login-username" class="form-label">用户名/账号</label>
                        <input id="login-username" type="text" name="username"  class="form-control">
                      </div>
                    </div>
                    <div class="cell-sm-6 offset-top-15 offset-sm-top-0">
                      <div class="form-group">
                        <label for="login-password" class="form-label">密码</label>
                        <input id="login-password" type="password" name="password"  class="form-control">
                      </div>
                    </div>
                  </div>
                  <div id="file_error" style="color:red"></div>
                  <div class="offset-top-35">
                    <button type="submit" class="btn btn-primary" onclick="logInFunc()">登录</button>
                    <a href="register.jsp" class="btn btn-primary">注册</a>
                  </div>
                </form>
                </div>
            </div>
          </div>
        </section>
      </main>
      <!-- Page Footer-->
      <footer
		class="page-footer bg-gray-lighter section-75 section-md-top-103 section-md-bottom-55 text-md-left">
		<div class="shell">
			<div class="range range-xs-center offset-top-20">
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<a href="index.jsp"><img width="164" height="29"
						src="images/logo-dark-164x29.png" alt="">
					</a>
				</div>

				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-bold font-accent text-spacing-50 text-mine-shaft">关于我们</p>
					</div>
					<div class="offset-top-10 offset-md-top-20">
						<p class="text-gray-light" style="line-height: 35px">“CodingDiary”,是一个主要面向高校计算机方向学生的，集分享知识、学习互动、交流心得为一体的综合平台。构建以校园为单位的“校园博客圈”。</p>
					</div>
				</div>
				<div class="cell-sm-8 cell-md-4 offset-top-44 offset-md-top-0">
					<div>
						<p class="text-uppercase text-bold font-accent text-spacing-50 text-mine-shaft">联系我们</p>
					</div>
					<div class="reveal-inline-block offset-top-10 offset-md-top-20">
						<ul class="text-left">
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="icon icon-sm icon-primary material-icons-ico material-icons-home"></span>
								上海市 普陀区 华东师范大学 第五宿舍420室</li>
							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-call"></span>
								15221532065</li>

							<li class=" text-gray-light"
								style="line-height:35px; font-size: 16px"><span
								class="offset-top-10 icon icon-sm icon-primary material-icons-ico material-icons-mail"></span>
								1906817459@qq.com</li>
						</ul>
					</div>
				</div>

				<div class="offset-top-60">
					<div class="hr bg-mercury"></div>
				</div>
				<div class="cell-md-push-6 offset-top-50">
					<p class="text-extra-small text-gray-light text-center">
						CodingDiary &#169; <span id="copyright-year"></span> <a
							href="#" class="text-gray-light">All rights
							reserved.</a>
					</p>
				</div>
			</div>
	</footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>
    
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="js/layer-v2.0/layer/layer.js"></script>
    <script type="text/javascript" src="js/WhwUtil.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript">  
           function logInFunc(){  
             var login_account=$("#login-username").val();  
             var login_password=$("#login-password").val();
             if(login_account==""){
             	$("#file_error").empty().append("账号不能为空哦0.0");
             	return;
             }
             if(login_password==""){
             	$("#file_error").empty().append("密码不能为空哦0.0");
             	return;
             }
             $.ajax({  
               type:"post",//请求方式  
               url: "${pageContext.request.contextPath}/userLogInAction_logIn",
               timeout:800000,//超时时间：800秒  
               dataType:"json",//设置返回数据的格式  
               data: {
        		"login_account": login_account,
            	"login_password": login_password
        		},
        	   //请求成功后的回调函数 data为json格式  
               success:function(data){  
                  if(data.retcode!="0000"){
                  	alert("账号或密码错误");
                  }
                  else
                  {
                    alert("登录成功！");
                    window.location.href="index.jsp"; 
                   // alert("登录成功！");
                   // alert("1");
                   //	response.sendRedirect("/web/jsp/index.jsp");

                  }
                  //alert(data.yourName+"你输入的内容:"+data.yourContent);  
              },  
              //请求出错的处理  
              error:function(){  
                        alert("请求出错");  
              }  
           });  
          }  
    </script>
  </body>
</html>