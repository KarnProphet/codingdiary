package com.huihuan.gmp.actions.userPage;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.userPage.UserPageService;

public class UserPageAction extends BaseAction{
	@Resource
	private UserPageService userPageService;
	public Userinfo userinfo;
	
	public Userinfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public String skipToMyPage() throws ServiceException{
		HttpSession session = getHttpRequest().getSession(); 
		try{
			int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
			userinfo=userPageService.getUserinfo(userId);
			session.setAttribute("userinfo", userinfo);
		}catch (Exception e) {
			// TODO: handle exception
			return "nologin";
		}
		return "success";
	}
	
	
}
