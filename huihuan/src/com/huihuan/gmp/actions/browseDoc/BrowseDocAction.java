package com.huihuan.gmp.actions.browseDoc;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.apache.struts2.json.JSONUtil;

import com.huihuan.common.util.JsonUtil;
import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.loadDocument.LoadDocumentService;
import com.jcraft.jsch.Session;

public class BrowseDocAction extends BaseAction {
	@Resource
	private LoadDocumentService loadDocumentService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	/**
	 * 加载所有用户的所有文档
	 * 
	 * @return
	 * @throws Exception
	 */
	public String loadAllDocs() throws ServiceException {
		List<List<Map<String, String>>> docsList = loadDocumentService
				.getAllDocsInfo();
		queryJson.setObj(docsList);
		return "jsonResult";
	}

	/**
	 * 点击某个学习资料,接受从前台传来的docID
	 * 
	 * @return
	 * @throws Exception
	 */
	public String loadSingleDoc() throws ServiceException {
		int doc_id = Integer.parseInt(getHttpRequest().getParameter("doc_id"));
		// 拿到文件的属性信息
		Map<String, String> map = loadDocumentService.getDocInfoByDocID(doc_id);
		try {
			map.put("myID",
					getHttpRequest().getSession()
							.getAttribute(Constants.USER_Id).toString());
		}
		catch(NullPointerException e){
			map.put("myID", "null");
		}
		// 文件评论信息
		List<Map<String, String>> mapList = loadDocumentService
				.getDocCommentByDocID(doc_id);
		// map列表，最后一个map存放属性信息，前面的存放评论信息
		mapList.add(map);
		queryJson.setObj(mapList);
		return "jsonResult";
	}

	/**
	 * 加载该用户的所有学习资料
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public String loadMyDocs() throws ServiceException {
		HttpSession session = getHttpRequest().getSession();
		int user_id = Integer.parseInt(session.getAttribute(Constants.USER_Id)
				.toString());
		List<Map<String, String>> list = loadDocumentService
				.getDocsInfoByUserID(user_id);
		// JSONArray jsonArray = JSONArray.fromObject(list);
		// System.out.println(jsonArray.toString());
		queryJson.setObj(list);
		return "jsonResult";
	}
}
