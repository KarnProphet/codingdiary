package com.huihuan.gmp.actions.uploadFile;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.security.auth.callback.ConfirmationCallback;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.tools.ant.types.resources.selectors.Date;

import ch.epfl.lamp.compiler.msil.util.Table.Constant;

import com.huihuan.gmp.cst.Constants;
import com.huihuan.common.util.JsonUtil;
import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.uploadFile.UploadFileService;
import com.jcraft.jsch.Session;
import com.opensymphony.xwork2.ActionContext;

public class UploadFileAction extends BaseAction {
	@Resource
	private UploadFileService uploadFileService;
	private BaseJson queryJson = new BaseJson();

	private File uploadFile;
	private String uploadFileFileName;
	private String uploadFileContentType;

	/**
	 * uploadFile为jsp中name="uploadFile"即<input type="file" name="uploadFile">
	 * 
	 * @return file
	 */
	public File getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(File uploadFile) {
		this.uploadFile = uploadFile;
	}

	public String getUploadFileFileName() {
		return uploadFileFileName;
	}

	public void setUploadFileFileName(String uploadFileFileName) {
		this.uploadFileFileName = uploadFileFileName;
	}

	public String getUploadFileContentType() {
		return uploadFileContentType;
	}

	public void setUploadFileContentType(String uploadFileContentType) {
		this.uploadFileContentType = uploadFileContentType;
	}

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public void uploadFile() throws ServiceException {
		queryJson = new BaseJson();
		try {

			String docNameString = getHttpRequest().getParameter("doc_name");
			String docTagString = getHttpRequest().getParameter("doc_tag");
			String docDescriptionString = getHttpRequest().getParameter(
					"doc_description");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			HttpSession session = getHttpRequest().getSession();
			String user_id = session.getAttribute(Constants.USER_Id).toString();
			//相对地址
			String filePath = "/fileResources/" + user_id + "/" + uploadFileFileName;
			int result = uploadFileService.saveFileInfo(
					Integer.parseInt(user_id), docNameString, docTagString,
					docDescriptionString, filePath, timestamp);
			if (result == 0) {
				queryJson.setRetcode("0001");
				queryJson.setErrorMsg("文档已存在0.0");
			} else {
				queryJson.setRetcode("0000");
			}
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常=-=");
			processException(e, queryJson);
		} catch (Exception e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常=-=");
			e.printStackTrace();
		}
	}

	/**
	 * 复制文件后，调用uploadFile函数把路径传到数据库中
	 * @return
	 * @throws IOException
	 */
	public String upload() throws IOException {
		getHttpRequest().setCharacterEncoding("UTF-8");
		getHttpResponse().setCharacterEncoding("UTF-8");
		HttpSession session = getHttpRequest().getSession();
		String user_id = session.getAttribute(Constants.USER_Id).toString();
		String realpath = ServletActionContext.getServletContext().getRealPath(
				"/fileResources/" + user_id);
		
		System.out.println("Real Path : " + realpath);
		if (uploadFile != null) {
			File savedir = new File(realpath);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
			// 将文件复制到我的项目文件夹中 D:\MyEclipse Workspaces\MyEclipse
			// 10\.metadata\.me_tcat\webapps\huihuan\fileResources\1,数据库中存相对地址
			File savefile = new File(realpath, uploadFileFileName);
			FileUtils.copyFile(uploadFile, savefile);
		}
		try {
			uploadFile();
		} catch (Exception e) {
			queryJson.setErrorMsg("0003");
			queryJson.setErrorMsg("数据库连接出错了0.0");
			e.printStackTrace();
		}
		return "jsonResult";
	}
}