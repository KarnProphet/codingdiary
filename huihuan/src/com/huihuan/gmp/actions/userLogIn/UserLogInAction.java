package com.huihuan.gmp.actions.userLogIn;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.Login.ILoginService;
import com.huihuan.gmp.services.userLogIn.UserLogInService;

public class UserLogInAction extends BaseAction {
	@Resource
	private UserLogInService userLogInService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String returnLoginID() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    int id=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    queryJson.setObj(id);
	    return "jsonResult";
	}
	
	public String returnLoginName() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    String idStr=session.getAttribute(Constants.USER_Id).toString();
	    String loginName=userLogInService.getUsername(Integer.parseInt(idStr));
	    queryJson.setObj(loginName);
	    return "jsonResult";
	}
	
	public String logIn() throws ServiceException {
		queryJson = new BaseJson();
		try {	
			String account=getHttpRequest().getParameter("login_account");
			String pwd=getHttpRequest().getParameter("login_password");
			int result=userLogInService.login(account,pwd);
			if(result==-1){
			    queryJson.setRetcode("0001");
			    queryJson.setErrorMsg("该用户名不存在");
			}
			else if(result==0){
				queryJson.setRetcode("0002");
				queryJson.setErrorMsg("密码错误");
			}
			else{
				queryJson.setRetcode("0000");
				writeSession(Constants.USER_Id, result);
			}
		} catch (ServiceException e) {
			// TODO: handle exception
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}

	public String logout(){
		getHttpRequest().getSession().invalidate();
		return "logout";
	}
}