package com.huihuan.gmp.actions.postBlog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.postBlog.PostBlogService;

public class PostBlogAction extends BaseAction{
	@Resource
	private PostBlogService postBlogService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String postBlog() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    String blogContent=getBlogContent();
	    String blogTitle=getBlogTitle();
	    Set<String> tagsList=getTagsList();
	    int userId=0;
	    try{
	    	userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    }catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		queryJson.setRetcode("0000");
		try {
			postBlogService.postBlog(userId, blogContent, blogTitle, tagsList);
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}

	public String getBlogTitle() throws ServiceException {
	    String blogTitle= getHttpRequest().getParameter("blog_title");
	    return blogTitle;
	}
	
	public String getBlogContent() throws ServiceException {
		String blogContent = getHttpRequest().getParameter("blog_content");
	    return blogContent;
	}
	
	public Set<String> getTagsList() throws ServiceException {
		String tags = getHttpRequest().getParameter("blog_tags");
		String[] tagsArray=tags.split(",");
		Set<String> tagsList=new HashSet<String>();
		for(String tag:tagsArray){
			tagsList.add(tag);
		}
	    return tagsList;
	}
}
