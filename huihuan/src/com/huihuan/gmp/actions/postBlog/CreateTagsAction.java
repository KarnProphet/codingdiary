package com.huihuan.gmp.actions.postBlog;

import javax.annotation.Resource;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.postBlog.CreateTagsService;

public class CreateTagsAction extends BaseAction {
	@Resource
	private CreateTagsService createTagsService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String createNewTag() throws ServiceException {
		queryJson = new BaseJson();
		String tagName = getTagName();
		queryJson.setRetcode("0000");
		try {
			int result=createTagsService.createNewTag(tagName);
			if(result==-1){
				queryJson.setRetcode("0001");
			}
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}

	private String getTagName() {
		// TODO Auto-generated method stub
		String tagName = getHttpRequest().getParameter("new_tag");
		return tagName;
	}

}
