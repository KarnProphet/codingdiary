package com.huihuan.gmp.actions.postBlog;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.postBlog.ChangeTagsService;

public class ChangeTagsAction extends BaseAction {
	@Resource
	private ChangeTagsService changeTagsService;
	private BaseJson queryJson = new BaseJson();
	
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}
	
	public int getChangeTimes(){
		return Integer.parseInt(getHttpRequest().getParameter("change_times"));
	}
	
	public String getNewTag(){
		queryJson = new BaseJson();
		queryJson.setRetcode("0000");
		List<Tag> taglist=changeTagsService.getNewTags(getChangeTimes(),getTagsNumInEachPage());
		if(taglist.size()!=getTagsNumInEachPage()){
			queryJson.setRetcode("0001");
		}
		StringBuffer result=new StringBuffer();
		for(Tag tag : taglist){
			result.append(tag.getTagName());
			result.append(",");
		}
		queryJson.setObj(result.substring(0, result.length()-1).toString());
		return "jsonResult";
	}

	public int getTagsNumInEachPage() {
		// TODO Auto-generated method stub
		return Integer.parseInt(getHttpRequest().getParameter("tags_num_in_each_page"));
	}

}
