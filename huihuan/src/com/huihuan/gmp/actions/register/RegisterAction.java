package com.huihuan.gmp.actions.register;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.common.util.JsonUtil;
import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.Login.ILoginService;
import com.huihuan.gmp.services.register.RegisterService;

public class RegisterAction extends BaseAction{
	@Resource
	private RegisterService registerService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}
	public String setInfo() throws ServiceException{
		queryJson = new BaseJson();
		String account = getRegisterAccount();
		String userName= getUserName();
		String password = getUserPassword();
		String email = getUserEmail();
		
		queryJson.setObj(password);
		queryJson.setRetcode("0000");
		try{
			registerService.setUserAccount(account, userName, password,email);
		}
		catch(ServiceException e){
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}
	
	public String getRegisterAccount() throws ServiceException {
	    String account= getHttpRequest().getParameter("register_account");
	    return account;
	}
	
	public String getUserName() throws ServiceException {
		String userName = getHttpRequest().getParameter("register_userName");
	    return userName;
	}
	
	public String getUserPassword() throws ServiceException {
		String passwd = getHttpRequest().getParameter("register_password");
	    return passwd;
	}
	public String getUserEmail() throws ServiceException {
		String email = getHttpRequest().getParameter("register_email");
	    return email;
	}
}