package com.huihuan.gmp.actions.singlePost;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.singlePost.CommentPostService;

public class CommentPostAction extends BaseAction{
	@Resource
	private CommentPostService commentPostService;
	private BaseJson queryJson = new BaseJson();
	
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}
	
	public String commentPost() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    String commentContent=getCommentContent();
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int postID=getPostId();
		queryJson.setRetcode("0000");
		try {
			int commentID=commentPostService.comment(userId, commentContent);
			commentPostService.postComment(commentID, postID);
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}
	
	public String getCommentContent() throws ServiceException{
		String commentContent= getHttpRequest().getParameter("content");
	    return commentContent;
	}
	
	public int getPostId() throws ServiceException{
		int postId= Integer.parseInt(getHttpRequest().getParameter("postId"));
	    return postId;
	}
}
