package com.huihuan.gmp.actions.singlePost;

import java.util.List;

import javax.annotation.Resource;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.singleBlog.SkipToSingleBlogService;
import com.huihuan.gmp.services.singlePost.SkipToSinglePostService;


public class SkipToSinglePostAction extends BaseAction{
	@Resource
	private SkipToSinglePostService skipToSinglePostService;
	public List<Usercomment> commentList;
	public Forumpost post;
	
	public List<Usercomment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<Usercomment> commentList) {
		this.commentList = commentList;
	}
	public Forumpost getPost() {
		return post;
	}
	public void setPost(Forumpost post) {
		this.post = post;
	}
	
	public String skip() throws ServiceException{
		int postID=getPostID();
		post=skipToSinglePostService.getPost(postID);
		commentList=skipToSinglePostService.getCommentList(postID, "");
		getHttpRequest().getSession().setAttribute("commentList", commentList);
		getHttpRequest().getSession().setAttribute("post", post);
		return "success";
		
	}

	public int getPostID(){
		int postID= Integer.parseInt(getHttpRequest().getParameter("postID"));
	    return postID;
	}
	
}
