package com.huihuan.gmp.actions.singlePost;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.singlePost.PraisePostService;


public class PraisePostAction extends BaseAction{
	@Resource
	private PraisePostService praisePostService;
	private BaseJson queryJson = new BaseJson();
	
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String praisePost() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int postId=getPostId();
		queryJson.setRetcode("0000");
		try {
			int result=praisePostService.praisePost(userId, postId);
			if(result==-1){
				queryJson.setRetcode("0001");
			}
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}
	
	public String praiseComment() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int commentId=getCommentId();
		queryJson.setRetcode("0000");
		try {
			int result=praisePostService.praiseComment(userId, commentId);
			if(result==-1){
				queryJson.setRetcode("0001");
			}
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		
		return "jsonResult";
	}
	
	public int getPostId() throws ServiceException{
		int postId= Integer.parseInt(getHttpRequest().getParameter("postId"));
	    return postId;
	}
	public int getCommentId() throws ServiceException{
		int commentId= Integer.parseInt(getHttpRequest().getParameter("commentId"));
	    return commentId;
	}
}
