package com.huihuan.gmp.actions.docOperations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import org.apache.commons.collections.map.HashedMap;
import org.apache.struts2.ServletActionContext;

import scala.tools.nsc.settings.MutableSettings.OutputSetting;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.actions.userLogIn.UserLogInAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.docOperations.DocOperationsService;
import com.huihuan.gmp.services.loadDocument.LoadDocumentService;
import com.huihuan.gmp.services.userLogIn.UserLogInService;
import java.io.OutputStream;

public class DocOperationsAction extends BaseAction {
	@Resource
	private DocOperationsService docOperationsService;
	@Resource
	private UserLogInService userLogInService;
	@Resource
	private LoadDocumentService loadDocumentService;
	private BaseJson queryJson = new BaseJson();

	private InputStream fileInput;  
    private String fileName;  
    
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	/**
	 * 评论文档，接受前台传来的文档id
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public String commentDoc() throws ServiceException {
		int doc_id = Integer.parseInt(getHttpRequest().getParameter("doc_id"));
		HttpSession session = getHttpRequest().getSession();
		int user_id = Integer.parseInt(session.getAttribute(Constants.USER_Id)
				.toString());
		String commentDocText = getHttpRequest().getParameter("comment_text");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		int result = docOperationsService.commentDoc(user_id, doc_id,
				commentDocText, timestamp);
		if (result == -1) {
			queryJson.setRetcode("0001");
			queryJson.setErrorMsg("数据库连接出错了0.0");
		} else {
			Map<String, String> map = new HashedMap();
			map.put("docCommentTime", timestamp.toString());
			map.put("commenterName", userLogInService.getUsername(user_id));
			map.put("commentText", commentDocText);
			map.put("commentNum", String.valueOf(result));
			queryJson.setObj(map);
			queryJson.setRetcode("0000");
		}
		return "jsonResult";
	}

	/**
	 * 点赞文档，接受前台传来的文档id
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public String praiseDoc() throws ServiceException {
		int doc_id = Integer.parseInt(getHttpRequest().getParameter("doc_id"));
		HttpSession session = getHttpRequest().getSession();
		int user_id = Integer.parseInt(session.getAttribute(Constants.USER_Id)
				.toString());
		Map<String, String> result = docOperationsService.praiseDoc(user_id,
				doc_id);
		queryJson.setObj(result);
		if (result.containsKey("error")) {
			queryJson.setErrorMsg("连接出错了");
			queryJson.setRetcode("0001");
		} else {
			queryJson.setRetcode("0000");
		}
		return "jsonResult";
	}
	
	public InputStream getFileInput() {  
        return fileInput;  
    }  
  
    public void setFileInput(InputStream fileInput) {  
        this.fileInput = fileInput;  
    }  
    
    public String getFileName()  {  
        return fileName;  
    }  
  
    public void setFileName(String fileName) {  
        this.fileName = fileName;  
    }  
    
	/**
	 * 下载文档，接受前台传来的文档id
	 * 
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 */
	public String execute() throws ServiceException {
		int doc_id = Integer.parseInt(getHttpRequest().getParameter("doc_id"));
		HttpSession session = getHttpRequest().getSession();
		int user_id = Integer.parseInt(session.getAttribute(Constants.USER_Id)
				.toString());
		Map<String, String> result = loadDocumentService
				.getDocInfoByDocID(doc_id);
		fileName = result.get("docPath");
		
		fileInput=ServletActionContext.getServletContext().getResourceAsStream(fileName);
		fileName=fileName.substring(fileName.lastIndexOf("/")+1);
		try {
			fileName=URLEncoder.encode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "success";
	}
}