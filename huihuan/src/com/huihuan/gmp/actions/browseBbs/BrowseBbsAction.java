package com.huihuan.gmp.actions.browseBbs;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.loadDocument.LoadDocumentService;
import com.huihuan.gmp.services.loadPost.LoadPostService;

public class BrowseBbsAction extends BaseAction {
	@Resource
	private LoadPostService loadPostService;
	private BaseJson queryJson = new BaseJson();
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}
	/**
	 * 加载所有用户的所有文档
	 * @return
	 * @throws Exception
	 */
	public String loadAllDocs() throws ServiceException{
		List<List<Map<String, String>>> postList = loadPostService.getAllPostInfo();
		queryJson.setObj(postList);
		return "jsonResult";
	}
	
	/**
	 * 点击某个学习资料,接受从前台传来的docID
	 * @return
	 * @throws Exception
	 */
	public String loadSinglePost() throws ServiceException{
		
		return "jsonResult";
	}
	/**
	 * 加载该用户的所有学习资料
	 * @return
	 * @throws ServiceException
	 */
	public String loadBbsPost() throws ServiceException{
		HttpSession session = getHttpRequest().getSession();
		int user_id = 1;
		try {
			user_id =1;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<Map<String, String>> list = loadPostService.getPostInfoByUserID(user_id);
//		JSONArray jsonArray = JSONArray.fromObject(list);
//		System.out.println(jsonArray.toString());
		queryJson.setObj(list);
		return "jsonResult";
	}
}
