package com.huihuan.gmp.actions.postIndex;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.postIndex.SkipToPostListService;


public class SkipToPostListAction  extends BaseAction{
	@Resource
	private SkipToPostListService skipToPostListService;
	private BaseJson queryJson = new BaseJson();
	public List<Forumpost> postList;
	
	public List<Forumpost> getPostList() {
		return postList;
	}

	public void setPostList(List<Forumpost> postList) {
		this.postList = postList;
	}

	public String skip() throws ServiceException{
		String type=getType();
		String typeValue=getTypeValue();
		postList=skipToPostListService.getPostList(type,typeValue);
		getHttpRequest().getSession().setAttribute("postList", postList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue.split("and")[0]);
		getHttpRequest().getSession().setAttribute("type", type);
		return "success";
	}
	
	public String searchPost() throws ServiceException{
		String typeValue=getHttpRequest().getParameter("searchtext");
		if(getType().contains("and")){
			String temp=getTypeValue().split("and")[1];
			typeValue=typeValue+"and"+temp;
		}
		postList=skipToPostListService.getPostListBySearch(typeValue);
		getHttpRequest().getSession().setAttribute("postList", postList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue.split("and")[0]);
		getHttpRequest().getSession().setAttribute("type", getType());
		return "success";
	}
	
	public String getType(){
		String type= getHttpRequest().getParameter("type");
	    return type;
	}
	
	public String getTypeValue(){
		String typeValue= getHttpRequest().getParameter("type_value");
	    return replaceTypeValue(typeValue);
	}
	
	public String replaceTypeValue(String typeValue){
		Map<String, String> replaceTable=new HashMap<String, String>();
		replaceTable.put("CPP", "C++");
		replaceTable.put("Csh", "C#");
		replaceTable.put("SQL", "数据库");
		if(typeValue.contains("-1")){
			typeValue=typeValue.replace("-1", getHttpRequest().getSession().getAttribute(Constants.USER_Id).toString());
		}
		replaceTable.put("0", "1");
		if(typeValue.contains("and")){
			String temp[]=typeValue.split("and");
			if(replaceTable.containsKey(temp[0])){
				return replaceTable.get(temp[0])+"and"+ temp[1];
			}else{
				return typeValue;
			}
		}
		if(replaceTable.containsKey(typeValue)){
			return replaceTable.get(typeValue);
		}else{
			return typeValue;
		}
		
	}
}
