package com.huihuan.gmp.actions.singleBlog;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.singleBlog.CollectService;
import com.huihuan.gmp.services.singleBlog.PraiseService;

public class CollectAction extends BaseAction{
	@Resource
	private CollectService collectService;
	private BaseJson queryJson = new BaseJson();
	
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String collectBlog() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int blogId=getBlogId();
		queryJson.setRetcode("0000");
		try {
			int result=collectService.collectBlog(userId, blogId);
			if(result==-1){
				queryJson.setRetcode("0001");
			}
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}
	
	public int getBlogId() throws ServiceException{
		int blogId= Integer.parseInt(getHttpRequest().getParameter("blogId"));
	    return blogId;
	}
	
}
