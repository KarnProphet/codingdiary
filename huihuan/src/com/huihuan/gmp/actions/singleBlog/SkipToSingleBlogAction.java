package com.huihuan.gmp.actions.singleBlog;

import java.util.List;

import javax.annotation.Resource;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.singleBlog.SkipToSingleBlogService;

public class SkipToSingleBlogAction extends BaseAction{
	@Resource
	private SkipToSingleBlogService skipToSingleBlogService;
	public List<Usercomment> commentList;
	public Blog blog;
	
	public List<Usercomment> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<Usercomment> commentList) {
		this.commentList = commentList;
	}
	public Blog getBlog() {
		return blog;
	}
	public void setBlog(Blog blog) {
		this.blog = blog;
	}
	
	public String skip() throws ServiceException{
		int blogID=getBlogID();
		blog=skipToSingleBlogService.getBlog(blogID);
		commentList=skipToSingleBlogService.getCommentList(blogID, "");
		getHttpRequest().getSession().setAttribute("commentList", commentList);
		getHttpRequest().getSession().setAttribute("blog", blog);
		return "success";
		
	}

	public int getBlogID(){
		int blogID= Integer.parseInt(getHttpRequest().getParameter("blogID"));
	    return blogID;
	}
	
}
