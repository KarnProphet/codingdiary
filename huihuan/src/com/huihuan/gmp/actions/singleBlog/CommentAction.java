package com.huihuan.gmp.actions.singleBlog;

import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.postBlog.PostBlogService;
import com.huihuan.gmp.services.singleBlog.CommentService;

public class CommentAction extends BaseAction{
	@Resource
	private CommentService commentService;
	private BaseJson queryJson = new BaseJson();
	
	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}
	
	public String commentBlog() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    String commentContent=getCommentContent();
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    int blogID=getBlogId();
		queryJson.setRetcode("0000");
		try {
			int commentID=commentService.comment(userId, commentContent);
			commentService.blogComment(commentID, blogID);
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}
	
	public String getCommentContent() throws ServiceException{
		String commentContent= getHttpRequest().getParameter("content");
	    return commentContent;
	}
	
	public int getBlogId() throws ServiceException{
		int blogId= Integer.parseInt(getHttpRequest().getParameter("blogId"));
	    return blogId;
	}
}
