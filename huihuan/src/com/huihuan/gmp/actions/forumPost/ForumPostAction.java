package com.huihuan.gmp.actions.forumPost;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.forumPost.ForumPostService;

public class ForumPostAction extends BaseAction{
	@Resource
	private ForumPostService forumPostService;
	private BaseJson queryJson = new BaseJson();

	public BaseJson getQueryJson() {
		return queryJson;
	}

	public void setQueryJson(BaseJson queryJson) {
		this.queryJson = queryJson;
	}

	public String forumPost() throws ServiceException {
		queryJson = new BaseJson();
		HttpSession session = getHttpRequest().getSession(); 
	    //int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
	    String postContent=getPostContent();
	    String postTitle=getPostTitle();
	    Set<String> tagsList=getTagsList();
	    int userId=Integer.parseInt(session.getAttribute(Constants.USER_Id).toString());
		queryJson.setRetcode("0000");
		try {
			 forumPostService. forumPost(userId, postContent, postTitle, tagsList);
		} catch (ServiceException e) {
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			processException(e, queryJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			queryJson.setRetcode("0003");
			queryJson.setErrorMsg("服务器连接异常");
			e.printStackTrace();
		}
		return "jsonResult";
	}

	public String getPostTitle() throws ServiceException {
	    String postTitle= getHttpRequest().getParameter("post_title");
	    return postTitle;
	}
	
	public String getPostContent() throws ServiceException {
		String postContent = getHttpRequest().getParameter("post_content");
	    return postContent;
	}
	
	public Set<String> getTagsList() throws ServiceException {
		String tags = getHttpRequest().getParameter("post_tags");
		String[] tagsArray=tags.split(",");
		Set<String> tagsList=new HashSet<String>();
		for(String tag:tagsArray){
			tagsList.add(tag);
		}
	    return tagsList;
	}
}
