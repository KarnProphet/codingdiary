package com.huihuan.gmp.actions.blogIndex;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.base.BaseAction;
import com.huihuan.gmp.cst.Constants;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.json.BaseJson;
import com.huihuan.gmp.services.blogIndex.SkipToBlogListService;

public class SkipToBlogListAction  extends BaseAction{
	@Resource
	private SkipToBlogListService skipToBlogListService;
	private BaseJson queryJson = new BaseJson();
	public List<Blog> blogList;
	public List<Document> documentList;
	public List<Blog> getBlogList() {
		return blogList;
	}

	public List<Document> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Document> documentList) {
		this.documentList = documentList;
	}

	public void setBlogList(List<Blog> blogList) {
		this.blogList = blogList;
	}

	public String skip() throws ServiceException{
		String type=getType();
		String typeValue=getTypeValue();
		blogList=skipToBlogListService.getBlogList(type,typeValue);
		getHttpRequest().getSession().setAttribute("blogList", blogList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue);
		getHttpRequest().getSession().setAttribute("type", type);
		return "success";
	}
	
	public String documentSkip() throws ServiceException{
		String type=getType();
		String typeValue=getTypeValue();
		documentList=skipToBlogListService.getDocumentList(type,typeValue);
		getHttpRequest().getSession().setAttribute("documentList", documentList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue);
		getHttpRequest().getSession().setAttribute("type", type);
		return "success";
	}
	
	public String searchBlog() throws ServiceException{
		String typeValue=getHttpRequest().getParameter("searchtext");
		if(getType().contains("and")){
			String temp=getTypeValue().split("and")[1];
			typeValue=typeValue+"and"+temp;
		}
		blogList=skipToBlogListService.getBlogListBySearch(typeValue);
		getHttpRequest().getSession().setAttribute("blogList", blogList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue);
		getHttpRequest().getSession().setAttribute("type", getType());
		return "success";
	}
	
	public String searchDocument() throws ServiceException{
		String typeValue=getHttpRequest().getParameter("searchtext");
		if(getType().contains("and")){
			String temp=getTypeValue().split("and")[1];
			typeValue=typeValue+"and"+temp;
		}
		documentList=skipToBlogListService.getDocumentListBySearch(typeValue);
		getHttpRequest().getSession().setAttribute("documentList", documentList);
		getHttpRequest().getSession().setAttribute("typeValue", typeValue);
		getHttpRequest().getSession().setAttribute("type", getType());
		return "success";
	}
	
	public String getType(){
		String type= getHttpRequest().getParameter("type");
	    return type;
	}
	
	public String getTypeValue(){
		String typeValue= getHttpRequest().getParameter("type_value");
	    return replaceTypeValue(typeValue);
	}
	
	public String replaceTypeValue(String typeValue){
		Map<String, String> replaceTable=new HashMap<String, String>();
		replaceTable.put("CPP", "C++");
		replaceTable.put("Csh", "C#");
		replaceTable.put("SQL", "数据库");
		if(typeValue.contains("-1")){
			typeValue=typeValue.replace("-1", getHttpRequest().getSession().getAttribute(Constants.USER_Id).toString());
		}
		replaceTable.put("0", "1");
		if(typeValue.contains("and")){
			String temp[]=typeValue.split("and");
			if(replaceTable.containsKey(temp[0])){
				return replaceTable.get(temp[0])+"and"+  temp[1];
			}else{
				return typeValue;
			}
		}
		if(replaceTable.containsKey(typeValue)){
			return replaceTable.get(typeValue);
		}else{
			return typeValue;
		}
		
	}
}
