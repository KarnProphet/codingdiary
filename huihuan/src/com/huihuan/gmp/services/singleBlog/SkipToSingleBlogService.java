package com.huihuan.gmp.services.singleBlog;

import java.util.List;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.common.ICommService;

public interface SkipToSingleBlogService extends ICommService{
	public Blog getBlog(int blogID) throws ServiceException;
	public List<Usercomment> getCommentList(int blogID,String order) throws ServiceException;
}
