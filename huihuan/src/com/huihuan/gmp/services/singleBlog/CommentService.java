package com.huihuan.gmp.services.singleBlog;

import java.util.Set;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface CommentService extends ICommService{
	public int comment(int userId,String commentContent) throws ServiceException;
	public int blogComment(int commentID,int blogID) throws ServiceException;
}
