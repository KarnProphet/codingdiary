package com.huihuan.gmp.services.singleBlog;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface CollectService extends ICommService {
	public int collectBlog(int userId,int blogId) throws ServiceException;
}
