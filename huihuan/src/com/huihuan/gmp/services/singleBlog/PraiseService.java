package com.huihuan.gmp.services.singleBlog;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface PraiseService extends ICommService {
	public int praiseBlog(int userId,int blogId) throws ServiceException;
	public int praiseComment(int userId,int commentId) throws ServiceException;
}
