package com.huihuan.gmp.services.singleBlog.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singleBlog.CollectService;

@Service(value = "collectService")
public class CollectServiceImpl extends CommServiceImpl implements CollectService {

	@Override
	public int collectBlog(int userId, int blogId) throws ServiceException {
		// TODO Auto-generated method stub
		List list = baseDAO.findBySQL("select * from blog_collect where collectorID =? and blogID=?", new Object[]{userId,blogId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into blog_collect values(?,?)", new Object[]{blogId,userId});
		Blog blog=baseDAO.findById(blogId, Blog.class);
		int collectNum=blog.getCollectNum();
		collectNum++;
		blog.setCollectNum(collectNum);
		baseDAO.executeSQL("update blog set collectNum=? where blogID=?", new Object[]{collectNum,blogId});
		return 0;
	}

	

}
