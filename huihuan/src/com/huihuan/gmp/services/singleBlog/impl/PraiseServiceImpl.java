package com.huihuan.gmp.services.singleBlog.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singleBlog.PraiseService;

@Service(value = "praiseService")
public class PraiseServiceImpl extends CommServiceImpl implements PraiseService {

	@Override
	public int praiseBlog(int userId, int blogId) throws ServiceException {
		// TODO Auto-generated method stub
		//Userinfo userinfo=baseDAO.findById(userId, Userinfo.class);
		
		List<Integer> list = baseDAO.findBySQL("select * from praise_blog where praiseFromUserID =? and praiseBlogID=?", new Object[]{userId,blogId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into praise_blog values(?,?)", new Object[]{userId,blogId});
		Blog blog=baseDAO.findById(blogId, Blog.class);
		int praiseNum=blog.getPraiseNum();
		praiseNum++;
		blog.setPraiseNum(praiseNum);
		baseDAO.executeSQL("update blog set praiseNum=? where blogID=?", new Object[]{praiseNum,blogId});
		return 0;
	}

	@Override
	public int praiseComment(int userId, int commentId) throws ServiceException {
		// TODO Auto-generated method stub
		List<Integer> list = baseDAO.findBySQL("select * from praise_comment where praiseFromUserID =? and praiseCommentID=?", new Object[]{userId,commentId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into praise_comment values(?,?)", new Object[]{userId,commentId});
		Usercomment usercomment=baseDAO.findById(commentId, Usercomment.class);
		int praiseNum=usercomment.getPraiseNum();
		praiseNum++;
		usercomment.setPraiseNum(praiseNum);
		baseDAO.executeSQL("update usercomment set praiseNum=? where commentID=?", new Object[]{praiseNum,commentId});
		return 0;
	}



}
