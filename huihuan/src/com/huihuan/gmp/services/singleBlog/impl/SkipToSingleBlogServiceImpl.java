package com.huihuan.gmp.services.singleBlog.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.blogIndex.SkipToBlogListService;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singleBlog.SkipToSingleBlogService;

@Service(value = "skipToSingleBlogService")
public class SkipToSingleBlogServiceImpl extends CommServiceImpl implements SkipToSingleBlogService {

	@Override
	public Blog getBlog(int blogID) throws ServiceException {
		// TODO Auto-generated method stub
		Blog blog=baseDAO.findById(blogID, Blog.class);
		int browseNum=blog.getBrowseNum();
		browseNum++;
		blog.setBrowseNum(browseNum);
		baseDAO.executeSQL("update blog set browseNum=? where blogID=?", new Object[]{browseNum,blogID});
		return blog;
	}

	@Override
	public List<Usercomment> getCommentList(int blogID, String order)
			throws ServiceException {
		// TODO Auto-generated method stub
		List<Usercomment> userCommentList=new ArrayList<Usercomment>();
		List<Integer> userCommentIDList = baseDAO.findBySQL("select commentID from blog_comment where blogID =?", new Object[]{blogID});
		Integer[] list=(Integer[])userCommentIDList.toArray(new Integer[userCommentIDList.size()]);
		for(int temp:list){
			Usercomment usercomment=baseDAO.findById(temp, Usercomment.class);
			userCommentList.add(usercomment);
		}
		return userCommentList;
	}



}
