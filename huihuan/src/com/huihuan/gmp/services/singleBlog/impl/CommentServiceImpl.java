package com.huihuan.gmp.services.singleBlog.impl;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Service;


import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singleBlog.CommentService;

@Service(value = "commentService")
public class CommentServiceImpl  extends CommServiceImpl implements CommentService {

	@Override
	public int comment(int userId, String commentContent)
			throws ServiceException {
		// TODO Auto-generated method stub
		Userinfo userinfo=baseDAO.findById(userId, Userinfo.class);
		Date date = new Date();  
		Timestamp commentTime = new Timestamp(date.getTime());
		Usercomment comment=new Usercomment(userinfo, commentContent, commentTime);
		baseDAO.save(comment);
		int commentId=comment.getCommentId();
		return commentId;
	}

	@Override
	public int blogComment(int commentID, int blogID) throws ServiceException {
		// TODO Auto-generated method stub
		baseDAO.executeSQL("insert into blog_comment values(?,?)", new Object[]{blogID,commentID});
		Blog blog=baseDAO.findById(blogID, Blog.class);
		int commentNum=blog.getCommentNum();
		commentNum++;
		blog.setCommentNum(commentNum);
		baseDAO.executeSQL("update blog set commentNum=? where blogID=?", new Object[]{commentNum,blogID});
		return 0;
	}


}
