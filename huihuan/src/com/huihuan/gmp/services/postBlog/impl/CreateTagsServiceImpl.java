package com.huihuan.gmp.services.postBlog.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.postBlog.CreateTagsService;

@Service(value = "createTagsService")
public class CreateTagsServiceImpl extends CommServiceImpl implements CreateTagsService {
	public int createNewTag(String tagName) throws ServiceException {
		List list = baseDAO.findBySQL("select * from tag where tagName=?", new Object[]{tagName});
		if(list.size()>0){
			return -1;
		}
		Tag tag =new Tag(tagName);
		baseDAO.save(tag);
		return 0;
	}
}
