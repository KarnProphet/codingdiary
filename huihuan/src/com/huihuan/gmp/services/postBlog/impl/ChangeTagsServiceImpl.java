package com.huihuan.gmp.services.postBlog.impl;


import java.util.List;

import javax.xml.registry.infomodel.User;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.daos.BaseDAO;
import com.huihuan.gmp.entity.Products;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.postBlog.ChangeTagsService;

@Service(value = "changeTagsService")
public class ChangeTagsServiceImpl extends CommServiceImpl implements ChangeTagsService {

	@Override
	public List getNewTags(int times,int tagsNumInEachPage) {
		// TODO Auto-generated method stub
		//int total=baseDAO.findByProperty("accountNumber", account, Tag.class);
		List<Tag> tags= baseDAO.findAll(Tag.class);
		int startIndex=times*tagsNumInEachPage;
		int endIndex=(times+1)*tagsNumInEachPage;
		if(endIndex>tags.size()){
			endIndex=tags.size();
		}
		return tags.subList(startIndex, endIndex);
	}
}
