package com.huihuan.gmp.services.postBlog.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.postBlog.PostBlogService;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.entity.Tag;

@Service(value = "postBlogService")
public class PostBlogServiceImpl extends CommServiceImpl implements PostBlogService {


	@Override
	public int postBlog(int userId, String blogContent, String blogTitle,
			Set<String> tagsList) throws ServiceException {
		// TODO Auto-generated method stub
		Blog blog=new Blog(blogTitle,blogContent);
		Userinfo userinfo = baseDAO.findById(userId, Userinfo.class);
		blog.setUserinfo(userinfo);
		Set tagsIdList=new HashSet();
		String[] tempTagsList=(String[]) tagsList.toArray(new String[tagsList.size()]);
		for(String tagStr : tempTagsList){
			List<Tag> tagList=baseDAO.findByProperty("tagName", tagStr , Tag.class);
			tagsIdList.add(tagList.get(0).getTagId());
		}
		Date date = new Date();  
		Timestamp ts = new Timestamp(date.getTime());
		blog.setPublishTime(ts);
		baseDAO.save(blog);
		//baseDAO.executeSQL("insert into document_tag values(?,?)", new Object[]{documentID,tagID});
		int blogId=blog.getBlogId();
		for(String tagStr : tempTagsList){
			List<Tag> tagList=baseDAO.findByProperty("tagName", tagStr , Tag.class);
			baseDAO.executeSQL("insert into blog_tag values(?,?)", new Object[]{blogId,tagList.get(0).getTagId()});
		}
		return 0;
	}

}
