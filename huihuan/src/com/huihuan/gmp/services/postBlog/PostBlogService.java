package com.huihuan.gmp.services.postBlog;

import java.util.Set;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface PostBlogService {
	public int postBlog(int userId,String blogContent,String blogTitle,Set<String> tagsList) throws ServiceException;

}
