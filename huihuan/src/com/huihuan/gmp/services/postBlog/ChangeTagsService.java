package com.huihuan.gmp.services.postBlog;

import java.util.List; 

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface ChangeTagsService extends ICommService {
	public List getNewTags(int times,int tagsNumInEachPage);
	
}
