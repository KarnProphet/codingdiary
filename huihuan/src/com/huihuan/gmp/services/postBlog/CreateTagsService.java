package com.huihuan.gmp.services.postBlog;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface CreateTagsService  extends ICommService{
	public int createNewTag(String tagName) throws ServiceException;
}
