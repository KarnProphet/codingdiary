package com.huihuan.gmp.services.userPage;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.ICommService;

public interface UserPageService extends ICommService{
	public Userinfo getUserinfo(int userID) throws ServiceException;
}
