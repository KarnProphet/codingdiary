package com.huihuan.gmp.services.userPage.impl;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.userPage.UserPageService;

@Service(value = "userPageService")
public class UserPageServiceImpl extends CommServiceImpl implements UserPageService{

	@Override
	public Userinfo getUserinfo(int userID) throws ServiceException {
		// TODO Auto-generated method stub
		Userinfo userinfo=baseDAO.findById(userID, Userinfo.class);
		return userinfo;
	}

}
