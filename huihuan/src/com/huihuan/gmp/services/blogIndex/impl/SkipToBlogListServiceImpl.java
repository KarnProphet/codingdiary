package com.huihuan.gmp.services.blogIndex.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.BlogTag;
import com.huihuan.gmp.entity.BlogTagId;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.blogIndex.SkipToBlogListService;
import com.huihuan.gmp.services.common.ICommService;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.sun.org.apache.bcel.internal.generic.Select;

@Service(value = "skipToBlogListService")
public class SkipToBlogListServiceImpl extends CommServiceImpl implements SkipToBlogListService {

	@Override
	public List<Blog> getBlogList(String type, String typeValue)
			throws ServiceException {
		// TODO Auto-generated method stub
		List<Blog> blogList=new ArrayList<Blog>();
		if(type.equals("tag")){
			
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValue , Tag.class);
			List<Integer> blogIDList = baseDAO.findBySQL("select blogID from blog_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])blogIDList.toArray(new Integer[blogIDList.size()]);
			for(int temp:list){
				Blog blog=baseDAO.findById(temp, Blog.class);
				String content=blog.getBlogContent();
				content=content.substring(0, content.indexOf("</p>")+4);
				blog.setBlogContent(content);
				blogList.add(blog);
			}
		}
		else if(type.equals("userId")){
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValue), Userinfo.class);
			blogList=baseDAO.findByProperty("userinfo", userinfo , Blog.class);
			Blog[] list=(Blog[])blogList.toArray(new Blog[blogList.size()]);
			for(Blog blog :list){
				String content=blog.getBlogContent();
				content=content.substring(0, content.indexOf("</p>")+4);
				blog.setBlogContent(content);
			}
		}
		else if(type.equals("taganduserId")){
			String[] typeValues=typeValue.split("and");
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValues[1]), Userinfo.class);
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValues[0] , Tag.class);
			List<Integer> blogIDList = baseDAO.findBySQL("select blogId from blog_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])blogIDList.toArray(new Integer[blogIDList.size()]);
			for(int temp:list){
				Blog blog=baseDAO.findById(temp, Blog.class);
				if(blog.getUserinfo().equals(userinfo)){
					String content=blog.getBlogContent();
					content=content.substring(0, content.indexOf("</p>")+4);
					blog.setBlogContent(content);
					blogList.add(blog);
				}
			}
		}
		return blogList;
	}

	@Override
	public List<Document> getDocumentList(String type, String typeValue)
			throws ServiceException {
		List<Document> documentList=new ArrayList<Document>();
		if(type.equals("tag")){
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValue , Tag.class);
			List<Integer> documentIDList = baseDAO.findBySQL("select docId from document_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])documentIDList.toArray(new Integer[documentIDList.size()]);
			for(int temp:list){
				Document document=baseDAO.findById(temp, Document.class);
				documentList.add(document);
			}
		}
		else if(type.equals("userId")){
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValue), Userinfo.class);
			documentList = baseDAO.findByProperty("userinfo", userinfo,Document.class);
		}
		else if(type.equals("taganduserId")){
			String[] typeValues=typeValue.split("and");
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValues[1]), Userinfo.class);
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValues[0] , Tag.class);
			List<Integer> documentIDList = baseDAO.findBySQL("select docId from document_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])documentIDList.toArray(new Integer[documentIDList.size()]);
			for(int temp:list){
				Document document=baseDAO.findById(temp, Document.class);
				if(document.getUserinfo().equals(userinfo)){
					documentList.add(document);
				}
			}
		}
		return documentList;
	}

	@Override
	public List<Blog> getBlogListBySearch(String typeValue)
			throws ServiceException {
		// TODO Auto-generated method stub
		List<Blog> blogList = new ArrayList<Blog>();
		List<Integer> blogIDList = baseDAO.findBySQL(
				"select blogID from blog where blogContent like ?",
				new Object[] { "%" + typeValue.split("and")[0] + "%" });
		Integer[] list = (Integer[]) blogIDList.toArray(new Integer[blogIDList
				.size()]);
		if(typeValue.contains("and")){
			for (int temp : list) {
				Blog blog = baseDAO.findById(temp, Blog.class);
				if(blog.getUserinfo().getUserId().toString().equals(typeValue.split("and")[1])){
					String content = blog.getBlogContent();
					content = content.substring(0, content.indexOf("</p>") + 4);
					blog.setBlogContent(content);
					blogList.add(blog);
				}
			}
		}
		else{
			for (int temp : list) {
				Blog blog = baseDAO.findById(temp, Blog.class);
				String content = blog.getBlogContent();
				content = content.substring(0, content.indexOf("</p>") + 4);
				blog.setBlogContent(content);
				blogList.add(blog);
			}
		}
		return blogList;
	}

	@Override
	public List<Document> getDocumentListBySearch(String typeValue)
			throws ServiceException {
		List<Document> documentList = new ArrayList<Document>();
		List<Integer> documentIDList = baseDAO.findBySQL(
				"select docId from document where docDescription like ?",
				new Object[] { "%" + typeValue.split("and")[0] + "%" });
		Integer[] list = (Integer[]) documentIDList.toArray(new Integer[documentIDList
				.size()]);
		if(typeValue.contains("and")){
			for (int temp : list) {
				Document document = baseDAO.findById(temp, Document.class);
				if(document.getUserinfo().getUserId().toString().equals(typeValue.split("and")[1])){
					documentList.add(document);
				}
			}
		}else{
			for (int temp : list) {
				Document document = baseDAO.findById(temp, Document.class);
				documentList.add(document);
			}
		}
		return documentList;
	}


}
