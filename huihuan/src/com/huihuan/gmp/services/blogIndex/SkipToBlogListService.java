package com.huihuan.gmp.services.blogIndex;

import java.util.List;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.services.common.ICommService;

public interface SkipToBlogListService extends ICommService{
	public List<Blog> getBlogList(String type,String typeValue) throws ServiceException;
	public List<Blog> getBlogListBySearch(String typeValue) throws ServiceException;
	public List<Document> getDocumentList(String type,String typeValue) throws ServiceException;
	public List<Document> getDocumentListBySearch(String typeValue) throws ServiceException;
}
