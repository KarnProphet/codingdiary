package com.huihuan.gmp.services.loadPost.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.loadPost.LoadPostService;

@Service(value = "loadPostService")
public class LoadPostSeiviceImpl extends CommServiceImpl implements
LoadPostService{

	/**
	 * 资料区首页展示所有资料
	 */
	@Override
	public List<List<Map<String, String>>> getAllPostInfo()
			throws ServiceException {
		List userIDs = baseDAO.findBySQL("select userID from userinfo", null);
		List<List<Map<String, String>>> retList = new ArrayList<List<Map<String, String>>>();
		for (int i = 0; i < userIDs.size(); i++) {
			int curUserID = Integer.parseInt(userIDs.get(i).toString());
			List<Map<String, String>> mapList = getPostInfoByUserID(curUserID);
			retList.add(mapList);
		}
		return retList;
	}

	/**
	 * 拿到个人资料区展示该人所有学习资料的信息（不包括评论内容）
	 */
	@Override
	public List<Map<String, String>> getPostInfoByUserID(int user_id)
			throws ServiceException {
		Userinfo userinfo = baseDAO.findById(user_id, Userinfo.class);
		List<Forumpost> posts = baseDAO.findByProperty("userinfo",userinfo, Forumpost.class);

		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		for (int i = 0; i < posts.size(); i++) {
			Forumpost post = posts.get(i);
			Map<String, String> map = new HashedMap();
			int postId = post.getPostId();
			map.put("postID", String.valueOf(postId));
			map.put("postTitle", post.getPostTitle());
			List postTagsList = baseDAO
					.findBySQL(
							"select tag.tagName from tag natural join forumpost_tag where forumpost_tag.postID=?",
							new Object[] { postId });
			StringBuilder sBuilder = new StringBuilder("");
			//tag 按分号隔开
			for (int j = 0; j < postTagsList.size(); j++) {
				sBuilder.append(postTagsList.get(j)).append(";");
			}
			sBuilder.deleteCharAt(sBuilder.length() - 1);

			map.put("postTags", sBuilder.toString());
			map.put("postUser", post.getUserinfo().getNickName());
			map.put("postTime", post.getPublishTime().toString());
			mapList.add(map);
		}
		
		return mapList;
	}

	/**
	 * 根据文件ID拿到该文件的所有评论
	 */
	@Override
	public List<Map<String, String>> getPostCommentByPostID(int postID) throws ServiceException{
		List list = baseDAO.findBySQL(
				"select commentID from post_comment where postID=?",
				new Object[] { postID });
		List<Map<String, String>> commentInfoList = new ArrayList<Map<String, String>>();
		for (int i = 0; i < list.size(); i++) {
			int commentID = Integer.parseInt(String.valueOf(list.get(i)));
			Usercomment commentInfo = baseDAO.findById(commentID, Usercomment.class);
			Map<String,String> map = new HashedMap();
			map.put("commenterID", String.valueOf(commentInfo.getUserinfo().getUserId()));
			map.put("commentContent", commentInfo.getCommentContent());
			map.put("praiseNum", String.valueOf(commentInfo.getPraiseNum()));
			map.put("commentTime",commentInfo.getCommentTime().toString());
			commentInfoList.add(map);
		}
		return commentInfoList;
	}
}
