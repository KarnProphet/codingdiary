package com.huihuan.gmp.services.loadPost;

import java.util.List;
import java.util.Map;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.services.common.ICommService;

public interface LoadPostService extends ICommService {
	
	/**
	 * 资料区首页展示所有资料
	 */
	public List<List<Map<String, String>>> getAllPostInfo() throws ServiceException;
	
	/**
	 * 拿到个人资料区展示该人所有资料的信息（不包括评论内容）
	 */
	public List<Map<String, String>> getPostInfoByUserID(int user_id) throws ServiceException;
	
	/**
	 * 根据文件ID拿到该文件的评论
	 */
	public List<Map<String, String>> getPostCommentByPostID(int postID) throws ServiceException;
}
