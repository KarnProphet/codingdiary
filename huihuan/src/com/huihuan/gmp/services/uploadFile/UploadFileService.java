package com.huihuan.gmp.services.uploadFile;

import java.sql.Timestamp;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;


public interface UploadFileService extends ICommService {
	/**
	 * 将文件相关信息存到数据库中
	 * @return  0   文档已存在;
	 *          1   文档信息完整,且成功存到数据库
	 */
	public int saveFileInfo(int UserID, String docName,String docTags, String docDescription,String filePath/*文件保存的地址*/,Timestamp timestamp) throws Exception;
}
