package com.huihuan.gmp.services.uploadFile.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import scala.inline;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.DocumentTag;
import com.huihuan.gmp.entity.DocumentTagId;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.uploadFile.UploadFileService;
import com.mysql.jdbc.log.Log;
import java.sql.Timestamp;

@Service(value = "uploadFileService")
public class UploadFileServiceImpl extends CommServiceImpl implements
		UploadFileService {

	/**
	 * 将文件相关信息存到数据库中
	 * @return 
	 * 			0   文档已存在;
	 *          1   文档信息完整
	 */
	@Override
	public int saveFileInfo(int UserID, String docName, String docTags,
			String docDescription, String filePath,Timestamp timestamp) throws Exception {

		Userinfo userinfo = baseDAO.findById(UserID, Userinfo.class);
		Document document = new Document(userinfo, docName, docDescription,
				filePath,timestamp);
		//检查文件是否已存在
		List<Document> docs = baseDAO.findBySQL("select * from document where uploaderID=? and docName=?", new Object[]{UserID,docName});
		if(docs.size()>0)
			return 0;
		baseDAO.save(document);
		String[] tagsStrings = docTags.split(";");
		for (int i = 0; i < tagsStrings.length; i++) {
			int tagID = -1;
			Tag newTag = new Tag(tagsStrings[i]);
			List<Tag> tags = baseDAO.findByProperty("tagName", tagsStrings[i], Tag.class);
			if (tags.isEmpty()) {
				baseDAO.save(newTag);
				tagID = newTag.getTagId();
			}
			else {
				tagID = tags.get(0).getTagId();
			}
			/*
			 *这样做插入不了数据到中间表
			DocumentTagId documentTagId = new DocumentTagId(document, newTag);
			DocumentTag docTag = new DocumentTag(documentTagId);
			baseDAO.save(docTag);
			System.out.println("DOCUMENT ID:"+ document.getDocId());
			System.out.println("TagID:"+newTag.getTagId());*/
			int documentID = document.getDocId();
			baseDAO.executeSQL("insert into document_tag values(?,?)", new Object[]{documentID,tagID});
		}
		return 1;
	}
}