package com.huihuan.gmp.services.register;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface RegisterService extends ICommService {
		public int setUserAccount(String account,String userName,String pwd,String email) throws ServiceException;
}
