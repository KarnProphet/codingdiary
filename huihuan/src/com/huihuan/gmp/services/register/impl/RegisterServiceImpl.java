package com.huihuan.gmp.services.register.impl;

import java.sql.Timestamp;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.register.RegisterService;
import com.huihuan.gmp.entity.Userinfo;

@Service(value = "registerService")
public class RegisterServiceImpl extends CommServiceImpl implements RegisterService{

	@Override
	public int setUserAccount(String account, String userName, String pwd,String email)
			throws ServiceException {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Userinfo User_Info = new Userinfo(account,userName,pwd,email,timestamp);
		baseDAO.save(User_Info);
		return 0;
	}
}