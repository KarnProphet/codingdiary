package com.huihuan.gmp.services.postIndex;

import java.util.List;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.services.common.ICommService;


public interface SkipToPostListService extends ICommService{
	public List<Forumpost> getPostList(String type,String typeValue) throws ServiceException;
	public List<Forumpost> getPostListBySearch(String typeValue) throws ServiceException;
}
