package com.huihuan.gmp.services.postIndex.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Tag;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.postIndex.SkipToPostListService;

@Service(value = "skipToPostListService")
public class SkipToPostListServiceImpl extends CommServiceImpl implements SkipToPostListService {

	@Override
	public List<Forumpost> getPostList(String type, String typeValue)
			throws ServiceException {
		List<Forumpost> postList=new ArrayList<Forumpost>();
		if(type.equals("tag")){
			
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValue , Tag.class);
			List<Integer> postIDList = baseDAO.findBySQL("select postID from forumpost_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])postIDList.toArray(new Integer[postIDList.size()]);
			for(int temp:list){
				Forumpost post=baseDAO.findById(temp, Forumpost.class);
				String content=post.getPostContent();
				content=content.substring(0, content.indexOf("</p>")+4);
				post.setPostContent(content);
				postList.add(post);
			}
		}
		else if(type.equals("userId")){
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValue), Userinfo.class);
			postList=baseDAO.findByProperty("userinfo", userinfo , Forumpost.class);
			Forumpost[] list=(Forumpost[])postList.toArray(new Forumpost[postList.size()]);
			for(Forumpost post :list){
				String content=post.getPostContent();
				content=content.substring(0, content.indexOf("</p>")+4);
				post.setPostContent(content);
			}
		}
		else if(type.equals("taganduserId")){
			String[] typeValues=typeValue.split("and");
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(typeValues[1]), Userinfo.class);
			List<Tag> tagList=baseDAO.findByProperty("tagName", typeValues[0] , Tag.class);
			List<Integer> postIDList = baseDAO.findBySQL("select postID from forumpost_tag where tagID =?", new Object[]{tagList.get(0).getTagId()});
			Integer[] list=(Integer[])postIDList.toArray(new Integer[postIDList.size()]);
			for(int temp:list){
				Forumpost post=baseDAO.findById(temp, Forumpost.class);
				if(post.getUserinfo().equals(userinfo)){
					String content=post.getPostContent();
					content=content.substring(0, content.indexOf("</p>")+4);
					post.setPostContent(content);
					postList.add(post);
				}
			}
		}
		return postList;
	}

	@Override
	public List<Forumpost> getPostListBySearch(String typeValue)
			throws ServiceException {
		List<Forumpost> postList=new ArrayList<Forumpost>();
		List<Integer> postIDList = baseDAO.findBySQL(
				"select postID from forumpost where postContent like ?",
				new Object[] { "%" + typeValue.split("and")[0] + "%" });
		Integer[] list = (Integer[])postIDList.toArray(new Integer[postIDList
				.size()]);
		if(typeValue.contains("and")){
			for (int temp : list) {
				Forumpost forumpost = baseDAO.findById(temp, Forumpost.class);
				if(forumpost.getUserinfo().getUserId().toString().equals(typeValue.split("and")[1])){
				String content = forumpost.getPostContent();
				content = content.substring(0, content.indexOf("</p>") + 4);
				forumpost.setPostContent(content);
				postList.add(forumpost);
				}
			}
		}else{
			for (int temp : list) {
				Forumpost forumpost = baseDAO.findById(temp, Forumpost.class);
				String content = forumpost.getPostContent();
				content = content.substring(0, content.indexOf("</p>") + 4);
				forumpost.setPostContent(content);
				postList.add(forumpost);
			}
		}
		return postList;
	}

}
