package com.huihuan.gmp.services.userLogIn.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.registry.infomodel.User;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.daos.BaseDAO;
import com.huihuan.gmp.entity.Products;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.userLogIn.UserLogInService;

@Service(value = "userLogInService")
public class UserLogInServiceImpl extends CommServiceImpl implements UserLogInService{

	@Override
	public String getUsername(int id) throws ServiceException {
		Userinfo user = baseDAO.findById(id, Userinfo.class);
		String userName = null;
		userName = user.getNickName();
		return userName;
	}

	@Override
	public String getLoginAccountNum(int id) throws ServiceException {
		Userinfo user = baseDAO.findById(id, Userinfo.class);
		String userAccount = null;
		userAccount = user.getAccountNumber();
		return userAccount;
	}

	@Override
	public String getUserIDByAccountNum(String account) throws ServiceException {
		String UserID = "";
		List<Userinfo> userinfos = baseDAO.findByProperty("accountNumber", account, Userinfo.class);
		List<Userinfo> userinfos_1 = baseDAO.findByProperty("nickName", account , Userinfo.class);
		List<Userinfo> userinfos_2 = baseDAO.findByProperty("email", account , Userinfo.class);
		for(Userinfo user : userinfos){
			UserID += user.getUserId().toString()+",";
		}
		for(Userinfo user : userinfos_1){
			UserID += user.getUserId().toString()+",";
		}
		for(Userinfo user : userinfos_2){
			UserID += user.getUserId().toString()+",";
		}
		UserID = UserID.substring(0,UserID.length()-1);
		return UserID;
	}

	@Override
	public int login(String name, String pwd) throws ServiceException {
		String UserID = getUserIDByAccountNum(name);
		if(UserID.equals(null))
			return -1;
		String[] usersStrings = UserID.split(",");
		for(String ID : usersStrings)
		{
			Userinfo userinfo = baseDAO.findById(Integer.parseInt(ID), Userinfo.class);
			if(userinfo.getUserPassword().equals(pwd))
			{
				return Integer.parseInt(ID);
			}
		}
		return 0;
	}
}