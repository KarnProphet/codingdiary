package com.huihuan.gmp.services.userLogIn;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface UserLogInService extends ICommService{
	public String getUsername(int id) throws ServiceException;
	public String getLoginAccountNum(int id) throws ServiceException;
	public String getUserIDByAccountNum(String account) throws ServiceException;
	public int login(String name,String pwd) throws ServiceException;
}
