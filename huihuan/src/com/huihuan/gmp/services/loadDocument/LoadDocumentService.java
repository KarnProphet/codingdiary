package com.huihuan.gmp.services.loadDocument;

import java.util.List;
import java.util.Map;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.services.common.ICommService;

public interface LoadDocumentService extends ICommService {

	/**
	 * 资料区首页展示所有资料
	 */
	public List<List<Map<String, String>>> getAllDocsInfo()
			throws ServiceException;

	/**
	 * 拿到个人资料区展示该人所有资料的信息（不包括评论内容）
	 */
	public List<Map<String, String>> getDocsInfoByUserID(int user_id)
			throws ServiceException;

	/**
	 * 根据文件ID拿到该文件的评论
	 */
	public List<Map<String, String>> getDocCommentByDocID(int docID)
			throws ServiceException;
	/**
	 * 根据文件ID拿到该文件的属性信息
	 * @param docID
	 * @return
	 * @throws ServiceException
	 */
	public Map<String, String> getDocInfoByDocID(int docID) throws ServiceException;
}
