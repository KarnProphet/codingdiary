package com.huihuan.gmp.services.loadDocument.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor.STRING;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.actions.tool.UserChoiceAction;
import com.huihuan.gmp.daos.BaseDAO;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.loadDocument.LoadDocumentService;
import com.sun.org.apache.bcel.internal.generic.IXOR;

@Service(value = "loadDocumentService")
public class LoadDocumentServiceImpl extends CommServiceImpl implements
		LoadDocumentService {

	/**
	 * 资料区首页展示所有资料
	 */
	@Override
	public List<List<Map<String, String>>> getAllDocsInfo()
			throws ServiceException {
		List userIDs = baseDAO.findBySQL("select userID from userinfo", null);
		List<List<Map<String, String>>> retList = new ArrayList<List<Map<String, String>>>();
		for (int i = 0; i < userIDs.size(); i++) {
			int curUserID = Integer.parseInt(userIDs.get(i).toString());
			List<Map<String, String>> mapList = getDocsInfoByUserID(curUserID);
			retList.add(mapList);
		}
		return retList;
	}

	/**
	 * 拿到个人资料区展示该人所有学习资料的信息（不包括评论内容）
	 */
	@Override
	public List<Map<String, String>> getDocsInfoByUserID(int user_id)
			throws ServiceException {
		Userinfo userinfo = baseDAO.findById(user_id, Userinfo.class);
		/*List<Document> docs = baseDAO.findByProperty("userinfo", userinfo,
				Document.class);*/
		List<Document> docs = baseDAO.findBySQL("select * from document order by uploadTime asc",null);
		List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
		for (int i = 0; i < docs.size(); i++) {
			Document document = docs.get(i);
			Map<String, String> map = new HashedMap();
			int docId = document.getDocId();
			map = getDocInfoByDocID(docId);
			mapList.add(map);
		}
		return mapList;
	}

	/**
	 * 根据文件ID拿到该文件的所有评论
	 */
	@Override
	public List<Map<String, String>> getDocCommentByDocID(int docID)
			throws ServiceException {
		List list = baseDAO.findBySQL(
				"select commentID from document_comment where docID=?",
				new Object[] { docID });
		List<Map<String, String>> commentInfoList = new ArrayList<Map<String, String>>();

		for (int i = 0; i < list.size(); i++) {
			int commentID = Integer.parseInt(String.valueOf(list.get(i)));
			Usercomment commentInfo = baseDAO.findById(commentID,
					Usercomment.class);
			Map<String, String> map = new HashedMap();
			map.put("commenterID",
					String.valueOf(commentInfo.getUserinfo().getUserId()));
			map.put("commenterName", commentInfo.getUserinfo().getNickName());
			map.put("commentContent", commentInfo.getCommentContent());
			map.put("praiseNum", String.valueOf(commentInfo.getPraiseNum()));
			map.put("commentTime", commentInfo.getCommentTime().toString());
			commentInfoList.add(map);
		}
		return commentInfoList;
	}

	/**
	 * 根据文件ID拿到该文件的属性信息
	 * 
	 * @param docID
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public Map<String, String> getDocInfoByDocID(int docID)
			throws ServiceException {
		Document document = baseDAO.findById(docID, Document.class);
		Map<String, String> map = new HashedMap();

		List docTagsList = baseDAO
				.findBySQL(
						"select tag.tagName from tag natural join document_tag where document_tag.docID=?",
						new Object[] { docID });
		StringBuilder sBuilder = new StringBuilder("");
		// tag 按分号隔开
		for (int j = 0; j < docTagsList.size(); j++) {
			sBuilder.append(docTagsList.get(j)).append(";");
		}
		List<Integer> praiseDocUsers = baseDAO
				.findBySQL(
						"select praiseFromUserID from praise_document where praiseDocID = ?",
						new Object[] { docID });
		StringBuilder usersString = new StringBuilder("");
		for (Integer string : praiseDocUsers) {
			usersString.append(String.valueOf(string)).append(",");
		}
		if (praiseDocUsers.size() > 0) {
			usersString.deleteCharAt(usersString.length() - 1);
		}
		map.put("praiseUsers", usersString.toString());
		System.out.println(usersString.toString());
		map.put("docID", String.valueOf(docID));
		map.put("docName", document.getDocName());
		map.put("docUploaderName", document.getUserinfo().getNickName());
		map.put("docTags", sBuilder.toString());
		map.put("docDescription", document.getDocDescription());
		map.put("docPath", document.getDocPath());
		map.put("praiseNum", String.valueOf(document.getPraiseNum()));
		map.put("commentNum", String.valueOf(document.getCommentNum()));
		map.put("downloadNum", String.valueOf(document.getDownloadCount()));
		map.put("forwardNum", String.valueOf(document.getForwardNum()));
		map.put("collectNum", String.valueOf(document.getCollectNum()));
		map.put("docUploadTime", document.getUploadTime().toString());
		return map;
	}
}