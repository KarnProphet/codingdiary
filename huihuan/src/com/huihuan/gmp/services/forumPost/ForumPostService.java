package com.huihuan.gmp.services.forumPost;

import java.util.Set;

import com.huihuan.framework.exceptions.ServiceException;


public interface ForumPostService {
	public int forumPost(int userId,String postContent,String postTitle,Set<String> tagsList) throws ServiceException;

}
