package com.huihuan.gmp.services.forumPost.impl;


import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.forumPost.ForumPostService;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.entity.Tag;

@Service(value = "forumPostService")
public class ForumPostServiceImpl extends CommServiceImpl implements ForumPostService {


	@Override
	public int forumPost(int userId,String postContent,String postTitle,Set<String> tagsList)  throws ServiceException {
		// TODO Auto-generated method stub
		Forumpost post=new Forumpost(postTitle,postContent);
		//Blog blog=new Blog(blogTitle,blogContent);
		Userinfo userinfo = baseDAO.findById(userId, Userinfo.class);
		post.setUserinfo(userinfo);
		Set tagsIdList=new HashSet();
		String[] tempTagsList=(String[]) tagsList.toArray(new String[tagsList.size()]);
		for(String tagStr : tempTagsList){
			List<Tag> tagList=baseDAO.findByProperty("tagName", tagStr , Tag.class);
			tagsIdList.add(tagList.get(0).getTagId());
		}
		Date date = new Date();  
		Timestamp ts = new Timestamp(date.getTime());
		post.setPublishTime(ts);
		baseDAO.save(post);
		//baseDAO.executeSQL("insert into document_tag values(?,?)", new Object[]{documentID,tagID});
		int postId=post.getPostId();
		for(String tagStr : tempTagsList){
			List<Tag> tagList=baseDAO.findByProperty("tagName", tagStr , Tag.class);
			baseDAO.executeSQL("insert into forumpost_tag values(?,?)", new Object[]{postId,tagList.get(0).getTagId()});
		}
		//tempTagsList=(String[]) tagsList.toArray(new String[tagsList.size()]);
		//Set s=post.getTags();
		//System.out.print(s);
		return 0;
	}

}
