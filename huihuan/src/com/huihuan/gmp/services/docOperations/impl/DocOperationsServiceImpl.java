package com.huihuan.gmp.services.docOperations.impl;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Document;
import com.huihuan.gmp.entity.PraiseDocument;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.docOperations.DocOperationsService;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Service;

@Service(value = "docOperationsService")
public class DocOperationsServiceImpl extends CommServiceImpl implements
		DocOperationsService {
	@Override
	public int commentDoc(int user_id, int doc_id, String commentContent,
			Timestamp timestamp) throws ServiceException {
		try {
			Userinfo userinfo = baseDAO.findById(user_id, Userinfo.class);
			Usercomment usercomment = new Usercomment(userinfo, commentContent,
					timestamp);
			baseDAO.save(usercomment);
			int comment_id = usercomment.getCommentId();
			baseDAO.executeSQL("insert into document_comment values(?,?)",
					new Object[] { doc_id, comment_id });
			baseDAO.executeSQL(
					"update document set commentNum=commentNum+1 where docID=?",
					new Object[] { doc_id });
			List commentNum = baseDAO.findBySQL("select commentNum from document where docID=?", new Object[]{doc_id});
			if(commentNum.size()>0)
			{
				return Integer.parseInt(commentNum.get(0).toString());
			}
			return 0;
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public Map<String, String> praiseDoc(int user_id, int doc_id) throws ServiceException {
		
		Map<String, String> map = new HashedMap();
		try {
			List list = baseDAO
					.findBySQL(
							"select * from praise_document where praiseFromUserID=? and praiseDocID=?",
							new Object[] { user_id, doc_id });
			//未点过赞
			if (list.size() == 0) {
				baseDAO.executeSQL("insert into praise_document values(?,?)",
						new Object[] { user_id, doc_id });
				baseDAO.executeSQL(
						"update document set praiseNum=praiseNum+1 where docID=?",
						new Object[] { doc_id });
				map.put("is_have_praised", "false");
				
			} 
			//已点过赞，则取消点赞
			else {
				baseDAO.executeSQL(
						"delete from praise_document where praiseFromUserID=? and praiseDocID=?",
						new Object[] { user_id, doc_id });
				baseDAO.executeSQL(
						"update document set praiseNum=praiseNum-1 where docID=?",
						new Object[] { doc_id });
				map.put("is_have_praised", "true");
			}

		} catch (Exception e) {
			map.put("error", "error");
		}
		Document document = baseDAO.findById(doc_id,Document.class);
		String praiseNum = String.valueOf(document.getPraiseNum());
		map.put("praise_num",praiseNum);
		return map;
	}
	
	@Override
	public int forwardDoc(int user_id, int doc_id) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}
}
