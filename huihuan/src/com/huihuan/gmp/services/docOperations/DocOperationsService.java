package com.huihuan.gmp.services.docOperations;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;
import java.sql.Timestamp;
import java.util.Map;

public interface DocOperationsService extends ICommService {
	/**
	 * 评论文件
	 * @param user_id 评论人id
	 * @param doc_id  文件id
	 * @param commentContent 评论内容
	 * @param timestamp 评论时间
	 * @return 返回该文档的评论数 ； -1 代表评论失败
	 * @throws ServiceException
	 */
	public int commentDoc(int user_id, int doc_id, String commentContent,
			Timestamp timestamp) throws ServiceException;
	/**
	 * 点赞文件
	 * @param user_id 点赞人id
	 * @param doc_id 文件id
	 * @return 
	 * @throws ServiceException
	 */
	public Map<String,String> praiseDoc(int user_id,int doc_id) throws ServiceException;
	
	public int forwardDoc(int user_id,int doc_id) throws ServiceException;
}
