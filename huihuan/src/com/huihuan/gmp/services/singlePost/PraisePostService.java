package com.huihuan.gmp.services.singlePost;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface PraisePostService extends ICommService {
	public int praisePost(int userId,int postId) throws ServiceException;
	public int praiseComment(int userId,int commentId) throws ServiceException;
}
