package com.huihuan.gmp.services.singlePost;

import java.util.List;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.common.ICommService;


public interface SkipToSinglePostService extends ICommService{
	public Forumpost getPost(int postID) throws ServiceException;
	public List<Usercomment> getCommentList(int postID,String order) throws ServiceException;
}
