package com.huihuan.gmp.services.singlePost.impl;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Blog;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.entity.Userinfo;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singlePost.CommentPostService;

@Service(value = "commentPostService")
public class CommentPostServiceImpl  extends CommServiceImpl implements CommentPostService {

	@Override
	public int comment(int userId, String commentContent)
			throws ServiceException {
		// TODO Auto-generated method stub
		Userinfo userinfo=baseDAO.findById(userId, Userinfo.class);
		Date date = new Date();  
		Timestamp commentTime = new Timestamp(date.getTime());
		Usercomment comment=new Usercomment(userinfo, commentContent, commentTime);
		baseDAO.save(comment);
		int commentId=comment.getCommentId();
		return commentId;
	}

	@Override
	public int postComment(int commentID, int postID) throws ServiceException {
		// TODO Auto-generated method stub
		baseDAO.executeSQL("insert into forumpost_comment values(?,?)", new Object[]{postID,commentID});
		Forumpost post=baseDAO.findById(postID, Forumpost.class);
		int commentNum=post.getCommentNum();
		commentNum++;
		post.setCommentNum(commentNum);
		baseDAO.executeSQL("update forumpost set commentNum=? where postID=?", new Object[]{commentNum,postID});
		return 0;
	}


}
