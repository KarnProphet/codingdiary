package com.huihuan.gmp.services.singlePost.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singlePost.CollectPostService;

@Service(value = "collectPostService")
public class CollectPostServiceImpl extends CommServiceImpl implements CollectPostService {

	@Override
	public int collectPost(int userId, int postId) throws ServiceException {
		// TODO Auto-generated method stub
		List list = baseDAO.findBySQL("select * from forumpost_collect where collectorID =? and postID=?", new Object[]{userId,postId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into forumpost_collect values(?,?)", new Object[]{postId,userId});
		Forumpost post=baseDAO.findById(postId, Forumpost.class);
		int collectNum=post.getCollectNum();
		collectNum++;
		post.setCollectNum(collectNum);
		baseDAO.executeSQL("update forumpost set collectNum=? where postID=?", new Object[]{collectNum,postId});
		return 0;
	}

	

}
