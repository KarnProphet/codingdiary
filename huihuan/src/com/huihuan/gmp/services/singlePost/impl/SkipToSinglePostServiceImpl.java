package com.huihuan.gmp.services.singlePost.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singlePost.SkipToSinglePostService;


@Service(value = "skipToSinglePostService")
public class SkipToSinglePostServiceImpl extends CommServiceImpl implements SkipToSinglePostService {

	@Override
	public Forumpost getPost(int postID) throws ServiceException {
		// TODO Auto-generated method stub
		Forumpost post=baseDAO.findById(postID, Forumpost.class);
		int browseNum=post.getBrowseNum();
		browseNum++;
		post.setBrowseNum(browseNum);
		baseDAO.executeSQL("update forumpost set browseNum=? where postID=?", new Object[]{browseNum,postID});
		return post;
	}

	@Override
	public List<Usercomment> getCommentList(int postID, String order)
			throws ServiceException {
		// TODO Auto-generated method stub
		List<Usercomment> userCommentList=new ArrayList<Usercomment>();
		List<Integer> userCommentIDList = baseDAO.findBySQL("select commentID from forumpost_comment where postID =?", new Object[]{postID});
		Integer[] list=(Integer[])userCommentIDList.toArray(new Integer[userCommentIDList.size()]);
		for(int temp:list){
			Usercomment usercomment=baseDAO.findById(temp, Usercomment.class);
			userCommentList.add(usercomment);
		}
		return userCommentList;
	}



}
