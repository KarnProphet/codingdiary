package com.huihuan.gmp.services.singlePost.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.entity.Forumpost;
import com.huihuan.gmp.entity.Usercomment;
import com.huihuan.gmp.services.common.impl.CommServiceImpl;
import com.huihuan.gmp.services.singlePost.PraisePostService;

@Service(value = "praisePostService")
public class PraisePostServiceImpl extends CommServiceImpl implements PraisePostService {

	@Override
	public int praisePost(int userId, int postId) throws ServiceException {
		// TODO Auto-generated method stub
		//Userinfo userinfo=baseDAO.findById(userId, Userinfo.class);
		
		List<Integer> list = baseDAO.findBySQL("select * from praise_forumpost where praiseFromUserID =? and praiseForumPostID=?", new Object[]{userId,postId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into praise_forumpost values(?,?)", new Object[]{userId,postId});
		Forumpost post=baseDAO.findById(postId, Forumpost.class);
		int praiseNum=post.getPraiseNum();
		praiseNum++;
		post.setPraiseNum(praiseNum);
		baseDAO.executeSQL("update forumpost set praiseNum=? where postID=?", new Object[]{praiseNum,postId});
		return 0;
	}

	@Override
	public int praiseComment(int userId, int commentId) throws ServiceException {
		// TODO Auto-generated method stub
		List<Integer> list = baseDAO.findBySQL("select * from praise_comment where praiseFromUserID =? and praiseCommentID=?", new Object[]{userId,commentId});
		if(list.size()>0){
			return -1;
		}
		baseDAO.executeSQL("insert into praise_comment values(?,?)", new Object[]{userId,commentId});
		Usercomment usercomment=baseDAO.findById(commentId, Usercomment.class);
		int praiseNum=usercomment.getPraiseNum();
		praiseNum++;
		usercomment.setPraiseNum(praiseNum);
		baseDAO.executeSQL("update usercomment set praiseNum=? where commentID=?", new Object[]{praiseNum,commentId});
		return 0;
	}



}
