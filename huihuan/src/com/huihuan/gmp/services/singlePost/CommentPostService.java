package com.huihuan.gmp.services.singlePost;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface CommentPostService extends ICommService{
	public int comment(int userId,String commentContent) throws ServiceException;
	public int postComment(int commentID,int postID) throws ServiceException;
}
