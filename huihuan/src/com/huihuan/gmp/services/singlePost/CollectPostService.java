package com.huihuan.gmp.services.singlePost;

import com.huihuan.framework.exceptions.ServiceException;
import com.huihuan.gmp.services.common.ICommService;

public interface CollectPostService extends ICommService{
	public int collectPost(int userId,int postId) throws ServiceException;
}
