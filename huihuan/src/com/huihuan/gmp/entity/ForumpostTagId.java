package com.huihuan.gmp.entity;

/**
 * ForumpostTagId entity. @author MyEclipse Persistence Tools
 */

public class ForumpostTagId implements java.io.Serializable {

	// Fields

	private Forumpost forumpost;
	private Tag tag;

	// Constructors

	/** default constructor */
	public ForumpostTagId() {
	}

	/** full constructor */
	public ForumpostTagId(Forumpost forumpost, Tag tag) {
		this.forumpost = forumpost;
		this.tag = tag;
	}

	// Property accessors

	public Forumpost getForumpost() {
		return this.forumpost;
	}

	public void setForumpost(Forumpost forumpost) {
		this.forumpost = forumpost;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ForumpostTagId))
			return false;
		ForumpostTagId castOther = (ForumpostTagId) other;

		return ((this.getForumpost() == castOther.getForumpost()) || (this
				.getForumpost() != null && castOther.getForumpost() != null && this
				.getForumpost().equals(castOther.getForumpost())))
				&& ((this.getTag() == castOther.getTag()) || (this.getTag() != null
						&& castOther.getTag() != null && this.getTag().equals(
						castOther.getTag())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getForumpost() == null ? 0 : this.getForumpost().hashCode());
		result = 37 * result
				+ (getTag() == null ? 0 : this.getTag().hashCode());
		return result;
	}

}