package com.huihuan.gmp.entity;

/**
 * BlogCollect entity. @author MyEclipse Persistence Tools
 */

public class BlogCollect implements java.io.Serializable {

	// Fields

	private BlogCollectId id;

	// Constructors

	/** default constructor */
	public BlogCollect() {
	}

	/** full constructor */
	public BlogCollect(BlogCollectId id) {
		this.id = id;
	}

	// Property accessors

	public BlogCollectId getId() {
		return this.id;
	}

	public void setId(BlogCollectId id) {
		this.id = id;
	}

}