package com.huihuan.gmp.entity;

/**
 * BlogTag entity. @author MyEclipse Persistence Tools
 */

public class BlogTag implements java.io.Serializable {

	// Fields

	private BlogTagId id;

	// Constructors

	/** default constructor */
	public BlogTag() {
	}

	/** full constructor */
	public BlogTag(BlogTagId id) {
		this.id = id;
	}

	// Property accessors

	public BlogTagId getId() {
		return this.id;
	}

	public void setId(BlogTagId id) {
		this.id = id;
	}

}