package com.huihuan.gmp.entity;

/**
 * ForumpostCommentId entity. @author MyEclipse Persistence Tools
 */

public class ForumpostCommentId implements java.io.Serializable {

	// Fields

	private Forumpost forumpost;
	private Usercomment usercomment;

	// Constructors

	/** default constructor */
	public ForumpostCommentId() {
	}

	/** full constructor */
	public ForumpostCommentId(Forumpost forumpost, Usercomment usercomment) {
		this.forumpost = forumpost;
		this.usercomment = usercomment;
	}

	// Property accessors

	public Forumpost getForumpost() {
		return this.forumpost;
	}

	public void setForumpost(Forumpost forumpost) {
		this.forumpost = forumpost;
	}

	public Usercomment getUsercomment() {
		return this.usercomment;
	}

	public void setUsercomment(Usercomment usercomment) {
		this.usercomment = usercomment;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ForumpostCommentId))
			return false;
		ForumpostCommentId castOther = (ForumpostCommentId) other;

		return ((this.getForumpost() == castOther.getForumpost()) || (this
				.getForumpost() != null && castOther.getForumpost() != null && this
				.getForumpost().equals(castOther.getForumpost())))
				&& ((this.getUsercomment() == castOther.getUsercomment()) || (this
						.getUsercomment() != null
						&& castOther.getUsercomment() != null && this
						.getUsercomment().equals(castOther.getUsercomment())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getForumpost() == null ? 0 : this.getForumpost().hashCode());
		result = 37
				* result
				+ (getUsercomment() == null ? 0 : this.getUsercomment()
						.hashCode());
		return result;
	}

}