package com.huihuan.gmp.entity;

/**
 * DocumentComment entity. @author MyEclipse Persistence Tools
 */

public class DocumentComment implements java.io.Serializable {

	// Fields

	private DocumentCommentId id;

	// Constructors

	/** default constructor */
	public DocumentComment() {
	}

	/** full constructor */
	public DocumentComment(DocumentCommentId id) {
		this.id = id;
	}

	// Property accessors

	public DocumentCommentId getId() {
		return this.id;
	}

	public void setId(DocumentCommentId id) {
		this.id = id;
	}

}