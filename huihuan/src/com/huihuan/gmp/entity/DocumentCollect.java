package com.huihuan.gmp.entity;

/**
 * DocumentCollect entity. @author MyEclipse Persistence Tools
 */

public class DocumentCollect implements java.io.Serializable {

	// Fields

	private DocumentCollectId id;

	// Constructors

	/** default constructor */
	public DocumentCollect() {
	}

	/** full constructor */
	public DocumentCollect(DocumentCollectId id) {
		this.id = id;
	}

	// Property accessors

	public DocumentCollectId getId() {
		return this.id;
	}

	public void setId(DocumentCollectId id) {
		this.id = id;
	}

}