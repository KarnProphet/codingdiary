 package com.huihuan.gmp.entity;

/**
 * ForumpostTag entity. @author MyEclipse Persistence Tools
 */

public class ForumpostTag implements java.io.Serializable {

	// Fields

	private ForumpostTagId id;

	// Constructors

	/** default constructor */
	public ForumpostTag() {
	}

	/** full constructor */
	public ForumpostTag(ForumpostTagId id) {
		this.id = id;
	}

	// Property accessors

	public ForumpostTagId getId() {
		return this.id;
	}

	public void setId(ForumpostTagId id) {
		this.id = id;
	}

}