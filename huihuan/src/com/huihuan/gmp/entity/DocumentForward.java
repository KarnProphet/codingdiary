package com.huihuan.gmp.entity;

/**
 * DocumentForward entity. @author MyEclipse Persistence Tools
 */

public class DocumentForward implements java.io.Serializable {

	// Fields

	private DocumentForwardId id;

	// Constructors

	/** default constructor */
	public DocumentForward() {
	}

	/** full constructor */
	public DocumentForward(DocumentForwardId id) {
		this.id = id;
	}

	// Property accessors

	public DocumentForwardId getId() {
		return this.id;
	}

	public void setId(DocumentForwardId id) {
		this.id = id;
	}

}