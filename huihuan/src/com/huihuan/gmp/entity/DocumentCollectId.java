package com.huihuan.gmp.entity;

/**
 * DocumentCollectId entity. @author MyEclipse Persistence Tools
 */

public class DocumentCollectId implements java.io.Serializable {

	// Fields

	private Document document;
	private Userinfo userinfo;

	// Constructors

	/** default constructor */
	public DocumentCollectId() {
	}

	/** full constructor */
	public DocumentCollectId(Document document, Userinfo userinfo) {
		this.document = document;
		this.userinfo = userinfo;
	}

	// Property accessors

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DocumentCollectId))
			return false;
		DocumentCollectId castOther = (DocumentCollectId) other;

		return ((this.getDocument() == castOther.getDocument()) || (this
				.getDocument() != null && castOther.getDocument() != null && this
				.getDocument().equals(castOther.getDocument())))
				&& ((this.getUserinfo() == castOther.getUserinfo()) || (this
						.getUserinfo() != null
						&& castOther.getUserinfo() != null && this
						.getUserinfo().equals(castOther.getUserinfo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getDocument() == null ? 0 : this.getDocument().hashCode());
		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		return result;
	}

}