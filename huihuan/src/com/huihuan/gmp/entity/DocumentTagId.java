package com.huihuan.gmp.entity;

/**
 * DocumentTagId entity. @author MyEclipse Persistence Tools
 */

public class DocumentTagId implements java.io.Serializable {

	// Fields

	private Document document;
	private Tag tag;

	// Constructors

	/** default constructor */
	public DocumentTagId() {
	}

	/** full constructor */
	public DocumentTagId(Document document, Tag tag) {
		this.document = document;
		this.tag = tag;
	}

	// Property accessors

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DocumentTagId))
			return false;
		DocumentTagId castOther = (DocumentTagId) other;

		return ((this.getDocument() == castOther.getDocument()) || (this
				.getDocument() != null && castOther.getDocument() != null && this
				.getDocument().equals(castOther.getDocument())))
				&& ((this.getTag() == castOther.getTag()) || (this.getTag() != null
						&& castOther.getTag() != null && this.getTag().equals(
						castOther.getTag())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getDocument() == null ? 0 : this.getDocument().hashCode());
		result = 37 * result
				+ (getTag() == null ? 0 : this.getTag().hashCode());
		return result;
	}

}