package com.huihuan.gmp.entity;

/**
 * BlogForward entity. @author MyEclipse Persistence Tools
 */

public class BlogForward implements java.io.Serializable {

	// Fields

	private BlogForwardId id;

	// Constructors

	/** default constructor */
	public BlogForward() {
	}

	/** full constructor */
	public BlogForward(BlogForwardId id) {
		this.id = id;
	}

	// Property accessors

	public BlogForwardId getId() {
		return this.id;
	}

	public void setId(BlogForwardId id) {
		this.id = id;
	}

}