package com.huihuan.gmp.entity;

/**
 * ForumpostCollect entity. @author MyEclipse Persistence Tools
 */

public class ForumpostCollect implements java.io.Serializable {

	// Fields

	private ForumpostCollectId id;

	// Constructors

	/** default constructor */
	public ForumpostCollect() {
	}

	/** full constructor */
	public ForumpostCollect(ForumpostCollectId id) {
		this.id = id;
	}

	// Property accessors

	public ForumpostCollectId getId() {
		return this.id;
	}

	public void setId(ForumpostCollectId id) {
		this.id = id;
	}

}