package com.huihuan.gmp.entity;

/**
 * PraiseBlog entity. @author MyEclipse Persistence Tools
 */

public class PraiseBlog implements java.io.Serializable {

	// Fields

	private PraiseBlogId id;

	// Constructors

	/** default constructor */
	public PraiseBlog() {
	}

	/** full constructor */
	public PraiseBlog(PraiseBlogId id) {
		this.id = id;
	}

	// Property accessors

	public PraiseBlogId getId() {
		return this.id;
	}

	public void setId(PraiseBlogId id) {
		this.id = id;
	}

}