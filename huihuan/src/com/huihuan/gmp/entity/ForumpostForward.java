package com.huihuan.gmp.entity;

/**
 * ForumpostForward entity. @author MyEclipse Persistence Tools
 */

public class ForumpostForward implements java.io.Serializable {

	// Fields

	private ForumpostForwardId id;

	// Constructors

	/** default constructor */
	public ForumpostForward() {
	}

	/** full constructor */
	public ForumpostForward(ForumpostForwardId id) {
		this.id = id;
	}

	// Property accessors

	public ForumpostForwardId getId() {
		return this.id;
	}

	public void setId(ForumpostForwardId id) {
		this.id = id;
	}

}