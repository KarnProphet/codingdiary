package com.huihuan.gmp.entity;

/**
 * DocumentTag entity. @author MyEclipse Persistence Tools
 */

public class DocumentTag implements java.io.Serializable {

	// Fields

	private DocumentTagId id;

	// Constructors

	/** default constructor */
	public DocumentTag() {
	}

	/** full constructor */
	public DocumentTag(DocumentTagId id) {
		this.id = id;
	}

	// Property accessors

	public DocumentTagId getId() {
		return this.id;
	}

	public void setId(DocumentTagId id) {
		this.id = id;
	}

}