package com.huihuan.gmp.entity;

/**
 * BlogTagId entity. @author MyEclipse Persistence Tools
 */

public class BlogTagId implements java.io.Serializable {

	// Fields

	private Blog blog;
	private Tag tag;

	// Constructors

	/** default constructor */
	public BlogTagId() {
	}

	/** full constructor */
	public BlogTagId(Blog blog, Tag tag) {
		this.blog = blog;
		this.tag = tag;
	}

	// Property accessors

	public Blog getBlog() {
		return this.blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BlogTagId))
			return false;
		BlogTagId castOther = (BlogTagId) other;

		return ((this.getBlog() == castOther.getBlog()) || (this.getBlog() != null
				&& castOther.getBlog() != null && this.getBlog().equals(
				castOther.getBlog())))
				&& ((this.getTag() == castOther.getTag()) || (this.getTag() != null
						&& castOther.getTag() != null && this.getTag().equals(
						castOther.getTag())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getBlog() == null ? 0 : this.getBlog().hashCode());
		result = 37 * result
				+ (getTag() == null ? 0 : this.getTag().hashCode());
		return result;
	}

}