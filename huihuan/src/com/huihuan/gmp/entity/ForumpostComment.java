package com.huihuan.gmp.entity;

/**
 * ForumpostComment entity. @author MyEclipse Persistence Tools
 */

public class ForumpostComment implements java.io.Serializable {

	// Fields

	private ForumpostCommentId id;

	// Constructors

	/** default constructor */
	public ForumpostComment() {
	}

	/** full constructor */
	public ForumpostComment(ForumpostCommentId id) {
		this.id = id;
	}

	// Property accessors

	public ForumpostCommentId getId() {
		return this.id;
	}

	public void setId(ForumpostCommentId id) {
		this.id = id;
	}

}