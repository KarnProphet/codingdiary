package com.huihuan.gmp.entity;

/**
 * PraiseDocumentId entity. @author MyEclipse Persistence Tools
 */

public class PraiseDocumentId implements java.io.Serializable {

	// Fields

	private Userinfo userinfo;
	private Document document;

	// Constructors

	/** default constructor */
	public PraiseDocumentId() {
	}

	/** full constructor */
	public PraiseDocumentId(Userinfo userinfo, Document document) {
		this.userinfo = userinfo;
		this.document = document;
	}

	// Property accessors

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PraiseDocumentId))
			return false;
		PraiseDocumentId castOther = (PraiseDocumentId) other;

		return ((this.getUserinfo() == castOther.getUserinfo()) || (this
				.getUserinfo() != null && castOther.getUserinfo() != null && this
				.getUserinfo().equals(castOther.getUserinfo())))
				&& ((this.getDocument() == castOther.getDocument()) || (this
						.getDocument() != null
						&& castOther.getDocument() != null && this
						.getDocument().equals(castOther.getDocument())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		result = 37 * result
				+ (getDocument() == null ? 0 : this.getDocument().hashCode());
		return result;
	}

}