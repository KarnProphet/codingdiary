package com.huihuan.gmp.entity;

/**
 * PraiseForumpostId entity. @author MyEclipse Persistence Tools
 */

public class PraiseForumpostId implements java.io.Serializable {

	// Fields

	private Userinfo userinfo;
	private Forumpost forumpost;

	// Constructors

	/** default constructor */
	public PraiseForumpostId() {
	}

	/** full constructor */
	public PraiseForumpostId(Userinfo userinfo, Forumpost forumpost) {
		this.userinfo = userinfo;
		this.forumpost = forumpost;
	}

	// Property accessors

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public Forumpost getForumpost() {
		return this.forumpost;
	}

	public void setForumpost(Forumpost forumpost) {
		this.forumpost = forumpost;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PraiseForumpostId))
			return false;
		PraiseForumpostId castOther = (PraiseForumpostId) other;

		return ((this.getUserinfo() == castOther.getUserinfo()) || (this
				.getUserinfo() != null && castOther.getUserinfo() != null && this
				.getUserinfo().equals(castOther.getUserinfo())))
				&& ((this.getForumpost() == castOther.getForumpost()) || (this
						.getForumpost() != null
						&& castOther.getForumpost() != null && this
						.getForumpost().equals(castOther.getForumpost())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		result = 37 * result
				+ (getForumpost() == null ? 0 : this.getForumpost().hashCode());
		return result;
	}

}