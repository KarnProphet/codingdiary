package com.huihuan.gmp.entity;

/**
 * BlogCollectId entity. @author MyEclipse Persistence Tools
 */

public class BlogCollectId implements java.io.Serializable {

	// Fields

	private Blog blog;
	private Userinfo userinfo;

	// Constructors

	/** default constructor */
	public BlogCollectId() {
	}

	/** full constructor */
	public BlogCollectId(Blog blog, Userinfo userinfo) {
		this.blog = blog;
		this.userinfo = userinfo;
	}

	// Property accessors

	public Blog getBlog() {
		return this.blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BlogCollectId))
			return false;
		BlogCollectId castOther = (BlogCollectId) other;

		return ((this.getBlog() == castOther.getBlog()) || (this.getBlog() != null
				&& castOther.getBlog() != null && this.getBlog().equals(
				castOther.getBlog())))
				&& ((this.getUserinfo() == castOther.getUserinfo()) || (this
						.getUserinfo() != null
						&& castOther.getUserinfo() != null && this
						.getUserinfo().equals(castOther.getUserinfo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getBlog() == null ? 0 : this.getBlog().hashCode());
		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		return result;
	}

}