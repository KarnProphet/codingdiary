package com.huihuan.gmp.entity;

/**
 * BlogComment entity. @author MyEclipse Persistence Tools
 */

public class BlogComment implements java.io.Serializable {

	// Fields

	private BlogCommentId id;

	// Constructors

	/** default constructor */
	public BlogComment() {
	}

	/** full constructor */
	public BlogComment(BlogCommentId id) {
		this.id = id;
	}

	// Property accessors

	public BlogCommentId getId() {
		return this.id;
	}

	public void setId(BlogCommentId id) {
		this.id = id;
	}

}