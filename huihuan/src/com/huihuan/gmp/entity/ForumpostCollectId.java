package com.huihuan.gmp.entity;

/**
 * ForumpostCollectId entity. @author MyEclipse Persistence Tools
 */

public class ForumpostCollectId implements java.io.Serializable {

	// Fields

	private Forumpost forumpost;
	private Userinfo userinfo;

	// Constructors

	/** default constructor */
	public ForumpostCollectId() {
	}

	/** full constructor */
	public ForumpostCollectId(Forumpost forumpost, Userinfo userinfo) {
		this.forumpost = forumpost;
		this.userinfo = userinfo;
	}

	// Property accessors

	public Forumpost getForumpost() {
		return this.forumpost;
	}

	public void setForumpost(Forumpost forumpost) {
		this.forumpost = forumpost;
	}

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ForumpostCollectId))
			return false;
		ForumpostCollectId castOther = (ForumpostCollectId) other;

		return ((this.getForumpost() == castOther.getForumpost()) || (this
				.getForumpost() != null && castOther.getForumpost() != null && this
				.getForumpost().equals(castOther.getForumpost())))
				&& ((this.getUserinfo() == castOther.getUserinfo()) || (this
						.getUserinfo() != null
						&& castOther.getUserinfo() != null && this
						.getUserinfo().equals(castOther.getUserinfo())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getForumpost() == null ? 0 : this.getForumpost().hashCode());
		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		return result;
	}

}