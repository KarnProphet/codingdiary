package com.huihuan.gmp.entity;

/**
 * BlogCommentId entity. @author MyEclipse Persistence Tools
 */

public class BlogCommentId implements java.io.Serializable {

	// Fields

	private Blog blog;
	private Usercomment usercomment;

	// Constructors

	/** default constructor */
	public BlogCommentId() {
	}

	/** full constructor */
	public BlogCommentId(Blog blog, Usercomment usercomment) {
		this.blog = blog;
		this.usercomment = usercomment;
	}

	// Property accessors

	public Blog getBlog() {
		return this.blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public Usercomment getUsercomment() {
		return this.usercomment;
	}

	public void setUsercomment(Usercomment usercomment) {
		this.usercomment = usercomment;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BlogCommentId))
			return false;
		BlogCommentId castOther = (BlogCommentId) other;

		return ((this.getBlog() == castOther.getBlog()) || (this.getBlog() != null
				&& castOther.getBlog() != null && this.getBlog().equals(
				castOther.getBlog())))
				&& ((this.getUsercomment() == castOther.getUsercomment()) || (this
						.getUsercomment() != null
						&& castOther.getUsercomment() != null && this
						.getUsercomment().equals(castOther.getUsercomment())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getBlog() == null ? 0 : this.getBlog().hashCode());
		result = 37
				* result
				+ (getUsercomment() == null ? 0 : this.getUsercomment()
						.hashCode());
		return result;
	}

}