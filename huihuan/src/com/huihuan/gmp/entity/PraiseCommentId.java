package com.huihuan.gmp.entity;

/**
 * PraiseCommentId entity. @author MyEclipse Persistence Tools
 */

public class PraiseCommentId implements java.io.Serializable {

	// Fields

	private Userinfo userinfo;
	private Usercomment usercomment;

	// Constructors

	/** default constructor */
	public PraiseCommentId() {
	}

	/** full constructor */
	public PraiseCommentId(Userinfo userinfo, Usercomment usercomment) {
		this.userinfo = userinfo;
		this.usercomment = usercomment;
	}

	// Property accessors

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public Usercomment getUsercomment() {
		return this.usercomment;
	}

	public void setUsercomment(Usercomment usercomment) {
		this.usercomment = usercomment;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PraiseCommentId))
			return false;
		PraiseCommentId castOther = (PraiseCommentId) other;

		return ((this.getUserinfo() == castOther.getUserinfo()) || (this
				.getUserinfo() != null && castOther.getUserinfo() != null && this
				.getUserinfo().equals(castOther.getUserinfo())))
				&& ((this.getUsercomment() == castOther.getUsercomment()) || (this
						.getUsercomment() != null
						&& castOther.getUsercomment() != null && this
						.getUsercomment().equals(castOther.getUsercomment())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		result = 37
				* result
				+ (getUsercomment() == null ? 0 : this.getUsercomment()
						.hashCode());
		return result;
	}

}