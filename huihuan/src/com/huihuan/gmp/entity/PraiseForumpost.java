package com.huihuan.gmp.entity;

/**
 * PraiseForumpost entity. @author MyEclipse Persistence Tools
 */

public class PraiseForumpost implements java.io.Serializable {

	// Fields

	private PraiseForumpostId id;

	// Constructors

	/** default constructor */
	public PraiseForumpost() {
	}

	/** full constructor */
	public PraiseForumpost(PraiseForumpostId id) {
		this.id = id;
	}

	// Property accessors

	public PraiseForumpostId getId() {
		return this.id;
	}

	public void setId(PraiseForumpostId id) {
		this.id = id;
	}

}