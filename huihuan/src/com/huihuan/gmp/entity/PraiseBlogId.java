package com.huihuan.gmp.entity;

/**
 * PraiseBlogId entity. @author MyEclipse Persistence Tools
 */

public class PraiseBlogId implements java.io.Serializable {

	// Fields

	private Userinfo userinfo;
	private Blog blog;

	// Constructors

	/** default constructor */
	public PraiseBlogId() {
	}

	/** full constructor */
	public PraiseBlogId(Userinfo userinfo, Blog blog) {
		this.userinfo = userinfo;
		this.blog = blog;
	}

	// Property accessors

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public Blog getBlog() {
		return this.blog;
	}

	public void setBlog(Blog blog) {
		this.blog = blog;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof PraiseBlogId))
			return false;
		PraiseBlogId castOther = (PraiseBlogId) other;

		return ((this.getUserinfo() == castOther.getUserinfo()) || (this
				.getUserinfo() != null && castOther.getUserinfo() != null && this
				.getUserinfo().equals(castOther.getUserinfo())))
				&& ((this.getBlog() == castOther.getBlog()) || (this.getBlog() != null
						&& castOther.getBlog() != null && this.getBlog()
						.equals(castOther.getBlog())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		result = 37 * result
				+ (getBlog() == null ? 0 : this.getBlog().hashCode());
		return result;
	}

}