package com.huihuan.gmp.entity;

/**
 * PraiseDocument entity. @author MyEclipse Persistence Tools
 */

public class PraiseDocument implements java.io.Serializable {

	// Fields

	private PraiseDocumentId id;

	// Constructors

	/** default constructor */
	public PraiseDocument() {
	}

	/** full constructor */
	public PraiseDocument(PraiseDocumentId id) {
		this.id = id;
	}

	// Property accessors

	public PraiseDocumentId getId() {
		return this.id;
	}

	public void setId(PraiseDocumentId id) {
		this.id = id;
	}

}