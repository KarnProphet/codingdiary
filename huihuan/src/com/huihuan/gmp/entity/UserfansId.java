package com.huihuan.gmp.entity;

/**
 * UserfansId entity. @author MyEclipse Persistence Tools
 */

public class UserfansId implements java.io.Serializable {

	// Fields

	private Userinfo userinfo;
	private Userinfo userinfo_1;

	// Constructors

	/** default constructor */
	public UserfansId() {
	}

	/** full constructor */
	public UserfansId(Userinfo userinfo, Userinfo userinfo_1) {
		this.userinfo = userinfo;
		this.userinfo_1 = userinfo_1;
	}

	// Property accessors

	public Userinfo getUserinfo() {
		return this.userinfo;
	}

	public void setUserinfo(Userinfo userinfo) {
		this.userinfo = userinfo;
	}

	public Userinfo getUserinfo_1() {
		return this.userinfo_1;
	}

	public void setUserinfo_1(Userinfo userinfo_1) {
		this.userinfo_1 = userinfo_1;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UserfansId))
			return false;
		UserfansId castOther = (UserfansId) other;

		return ((this.getUserinfo() == castOther.getUserinfo()) || (this
				.getUserinfo() != null && castOther.getUserinfo() != null && this
				.getUserinfo().equals(castOther.getUserinfo())))
				&& ((this.getUserinfo_1() == castOther.getUserinfo_1()) || (this
						.getUserinfo_1() != null
						&& castOther.getUserinfo_1() != null && this
						.getUserinfo_1().equals(castOther.getUserinfo_1())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getUserinfo() == null ? 0 : this.getUserinfo().hashCode());
		result = 37
				* result
				+ (getUserinfo_1() == null ? 0 : this.getUserinfo_1()
						.hashCode());
		return result;
	}

}