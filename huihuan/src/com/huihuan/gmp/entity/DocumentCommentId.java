package com.huihuan.gmp.entity;

/**
 * DocumentCommentId entity. @author MyEclipse Persistence Tools
 */

public class DocumentCommentId implements java.io.Serializable {

	// Fields

	private Document document;
	private Usercomment usercomment;

	// Constructors

	/** default constructor */
	public DocumentCommentId() {
	}

	/** full constructor */
	public DocumentCommentId(Document document, Usercomment usercomment) {
		this.document = document;
		this.usercomment = usercomment;
	}

	// Property accessors

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Usercomment getUsercomment() {
		return this.usercomment;
	}

	public void setUsercomment(Usercomment usercomment) {
		this.usercomment = usercomment;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DocumentCommentId))
			return false;
		DocumentCommentId castOther = (DocumentCommentId) other;

		return ((this.getDocument() == castOther.getDocument()) || (this
				.getDocument() != null && castOther.getDocument() != null && this
				.getDocument().equals(castOther.getDocument())))
				&& ((this.getUsercomment() == castOther.getUsercomment()) || (this
						.getUsercomment() != null
						&& castOther.getUsercomment() != null && this
						.getUsercomment().equals(castOther.getUsercomment())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getDocument() == null ? 0 : this.getDocument().hashCode());
		result = 37
				* result
				+ (getUsercomment() == null ? 0 : this.getUsercomment()
						.hashCode());
		return result;
	}

}