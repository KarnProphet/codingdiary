package com.huihuan.gmp.entity;

/**
 * PraiseComment entity. @author MyEclipse Persistence Tools
 */

public class PraiseComment implements java.io.Serializable {

	// Fields

	private PraiseCommentId id;

	// Constructors

	/** default constructor */
	public PraiseComment() {
	}

	/** full constructor */
	public PraiseComment(PraiseCommentId id) {
		this.id = id;
	}

	// Property accessors

	public PraiseCommentId getId() {
		return this.id;
	}

	public void setId(PraiseCommentId id) {
		this.id = id;
	}

}