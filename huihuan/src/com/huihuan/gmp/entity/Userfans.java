package com.huihuan.gmp.entity;

/**
 * Userfans entity. @author MyEclipse Persistence Tools
 */

public class Userfans implements java.io.Serializable {

	// Fields

	private UserfansId id;

	// Constructors

	/** default constructor */
	public Userfans() {
	}

	/** full constructor */
	public Userfans(UserfansId id) {
		this.id = id;
	}

	// Property accessors

	public UserfansId getId() {
		return this.id;
	}

	public void setId(UserfansId id) {
		this.id = id;
	}

}