package com.huihuan.common.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* 
 * @title JsonUtil.java 
 * @description 提供用于Json数据转换的静态方法 
 * @version V1.0 
 */
public class JsonUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(JsonUtil.class);

	/**
	 * 性能方面 JackSon > Gson > Json 故这里决定使用JackSon 将Collection转换为Json
	 * 参数是Collection
	 * 
	 * @param collection
	 */
	public static String toJson(Collection<Object> collection) {
		/*
		 * 使用Gson的方式 Gson gson = new Gson(); String json = gson.toJson(map);
		 */

		/*
		 * 使用Jackson的方式
		 */
		String json = "[]";
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			JsonGenerator gen = new JsonFactory().createJsonGenerator(sw);
			mapper.writeValue(gen, collection);
			gen.close();
			json = sw.toString();
		} catch (JsonGenerationException e) {
			json = "[]";
			logger.error(
					"------1------ toJson JsonGenerationException error:\n", e);

		} catch (JsonMappingException e) {
			json = "[]";
			logger.error("------2------ toJson JsonMappingException error:\n",
					e);

		} catch (IOException e) {
			json = "[]";
			logger.error("------3------ toJson IOException error:\n", e);
		}

		return json;
	}

	/**
	 * @Title: toJson
	 * @Description: 将对象转换为Json数据
	 * @param @param obj
	 * @param @return 设定文件
	 * @return String 返回类型
	 * @throws
	 */
	public static String toJson(Object obj) {
		String json = "[]";
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			JsonGenerator gen = new JsonFactory().createJsonGenerator(sw);
			mapper.writeValue(gen, obj);
			gen.close();

			json = sw.toString();
		} catch (JsonGenerationException e) {
			json = "[]";
			logger.error("------1------ toJson IOException error:\n", e);

		} catch (JsonMappingException e) {
			json = "[]";
			logger.error("------2------ toJson IOException error:\n", e);

		} catch (IOException e) {
			json = "[]";
			logger.error("------3------ toJson IOException error:\n", e);
		}

		return json;
	}

	/**
	 * 将MAP转为Json Map参数
	 * 
	 * @param map
	 * @return
	 * @throws IOException
	 */
	public static String toJson(Map<String, Object> map) {
		String json = "[]";
		ObjectMapper mapper = new ObjectMapper();
		StringWriter sw = new StringWriter();
		try {
			JsonGenerator gen = new JsonFactory().createJsonGenerator(sw);
			mapper.writeValue(gen, map);
			gen.close();

			json = sw.toString();
		} catch (JsonGenerationException e) {
			json = "[]";
			logger.error("----1------toJson JsonGenerationException:\n" + e);
		} catch (JsonMappingException e) {
			json = "[]";
			logger.error("----2------toJson JsonMappingException:\n" + e);
		} catch (IOException e) {
			json = "[]";
			logger.error("----3------toJson IOException:\n" + e);
		}

		return json;
	}

	/**
	 * 将JSON字符串 转换为对象
	 * 
	 * @author weiyuanhua
	 * @date 2010-11-18 下午02:52:13
	 * @param jsonStr
	 *            JSON字符串
	 * @param beanClass
	 *            泛型对象
	 * @param field
	 *            对象中需要忽略的属性
	 * @return
	 */
	public static Object jsonToObject(String jsonStr, Class<?> beanClass)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(jsonStr.getBytes(), beanClass);
	}

	public static Object jsonToObject(String jsonStr, String encoding,
			Class<?> beanClass) throws JsonParseException,
			JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(jsonStr.getBytes(encoding), beanClass);
	}

	public static Map<String, Object> parseJSON2Map(String jsonStr)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(jsonStr, Map.class);
	}

	// 不需要记性属性的忽略
	public static String toJson(Object object, String filterName) {
		ObjectMapper mapper = new ObjectMapper();
		String json = "[]";
		try {
			FilterProvider filterProvider = new SimpleFilterProvider()
					.addFilter(filterName,
							SimpleBeanPropertyFilter.serializeAllExcept());
			json = mapper.writer(filterProvider).writeValueAsString(object);
		} catch (Exception e) {
			json = "[]";
			logger.error("----1------toJson Exception:\n" + e);
		}

		return json;
	}

	/*
	 * 
	 * @param object 需要进行json序列化的类 可以是任何复杂的类
	 * 
	 * @param args 需要进行过滤的属性名称 类对象名称或者一些属性名称都可以进行过滤
	 */
	public static String toJson(Object object, String[] args, String filterName) {
		String json = "[]";
		ObjectMapper mapper = new ObjectMapper();
		try {
			FilterProvider filterProvider = new SimpleFilterProvider()
					.addFilter(filterName,
							SimpleBeanPropertyFilter.serializeAllExcept(args));
			json = mapper.writer(filterProvider).writeValueAsString(object);
		} catch (Exception e) {
			json = "[]";
			logger.error("----1------toJson Exception:\n" + e);
		}
		return json;
	}
}
